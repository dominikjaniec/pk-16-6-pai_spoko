#!/bin/bash

_NAME='spoko'
_WITH_PHP_DEBUG=true



_APP_URL="http://localhost/$_NAME"

_OWNER=$(ls -ld $_PATH_GLOBAL | awk '{print $3}')
_OWNER_MOD=644

_INSTALLER_DIR=$(dirname $(readlink -f $0))
_PATH_GLOBAL="$_INSTALLER_DIR/../application/ignored.file"
_PATH_GLOBAL=$(dirname $(readlink -f $_PATH_GLOBAL))
_PATH_APP="$_PATH_GLOBAL/public_html"

echo "Installation of 'spoko' in apache - needs root access"
echo "Application 'spoko' will be installed under:"
echo "  * $_APP_URL"
echo "Path to application will be set to:"
echo "  * $_PATH_APP"
if $_WITH_PHP_DEBUG; then
    echo "And with showing PHP errors and statistics..."
fi

_SITE_CONF_NAME="$_NAME.conf"
_SITE_CONF="/etc/apache2/sites-available/$_SITE_CONF_NAME"
echo "<VirtualHost *:80>" > $_SITE_CONF
echo "    ServerAdmin root@localhost" >> $_SITE_CONF
echo "" >> $_SITE_CONF
echo "    ServerName localhost" >> $_SITE_CONF
echo "    ServerAlias $(hostname)" >> $_SITE_CONF
echo "    ServerPath /$_NAME" >> $_SITE_CONF
echo "    DocumentRoot $_PATH_APP" >> $_SITE_CONF
echo "    ErrorDocument 404 /_show404.php" >> $_SITE_CONF
echo "" >> $_SITE_CONF
echo "    ErrorLog \${APACHE_LOG_DIR}/error.log" >> $_SITE_CONF
echo "    CustomLog \${APACHE_LOG_DIR}/access.log combined" >> $_SITE_CONF

if $_WITH_PHP_DEBUG; then
    echo "" >> $_SITE_CONF
    echo "    php_flag log_errors on" >> $_SITE_CONF
    echo "    php_flag display_errors on" >> $_SITE_CONF
    echo "    php_flag xdebug.remote_enable on" >> $_SITE_CONF
fi

echo "" >> $_SITE_CONF
echo "    <Directory $_PATH_APP>" >> $_SITE_CONF
echo "        Require all granted" >> $_SITE_CONF
echo "    </Directory>" >> $_SITE_CONF
echo "</VirtualHost>" >> $_SITE_CONF
echo ""
echo "Created VH file: $_SITE_CONF"

echo ""
echo "Executing: 'a2ensite $_SITE_CONF_NAME'"
a2ensite $_SITE_CONF_NAME

echo ""
echo "Executing: 'service apache2 reload'"
service apache2 reload

echo ""
echo "Restarted apache and site is enabled."
echo "Apache installation has been done."

_APP_CONFIG="spoko_config.ini"
_APP_CONFIG_PATH="$_PATH_GLOBAL/$_APP_CONFIG"
cp "$_INSTALLER_DIR/$_APP_CONFIG" $_APP_CONFIG_PATH
chown $_OWNER $_APP_CONFIG_PATH
chmod $_OWNER_MOD $_APP_CONFIG_PATH

_WEB_INSTALL="spoko_install.php"
_WEB_INSTALL_PATH="$_PATH_APP/$_WEB_INSTALL"

cp "$_INSTALLER_DIR/$_WEB_INSTALL" $_WEB_INSTALL_PATH
chown $_OWNER $_WEB_INSTALL_PATH
chmod $_OWNER_MOD $_WEB_INSTALL_PATH

echo ""
echo "Pleas corrent configuration in:"
echo "  * $_APP_CONFIG_PATH"
echo "Then open in browser a web installation page:"
echo "  * $_APP_URL/$_WEB_INSTALL"
echo "And finnaly, after a web installation please remove a file:"
echo "  * $_WEB_INSTALL_PATH"

