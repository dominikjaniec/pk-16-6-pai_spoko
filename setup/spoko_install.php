<?php
require_once '../common/Config.php';
require_once '../common/Helpers.php';
require_once '../common/Templater.php';
require_once '../db_scripts/Installer.php';
require_once '../model/UserSessionModel.php';

const INSTALL_ACTION = "install";
const WIPEOUT_SCHEMA = "cleanup";
const SEED_SAMPLES = "samples";

try {
    DataBase::EnsureSchemaExists();
    UserSessionModel::remove();
} catch (Exception $e) {
    // No problem here ;)
}

function wipeoutSchema() {
    return isset($_POST[WIPEOUT_SCHEMA]);
}

function seedSample() {
    return isset($_POST[SEED_SAMPLES]);
}

function template_render() {
    $shouldInstall = isset($_GET[INSTALL_ACTION]);
    ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>Installer</h2>
        </div>
        <div class="panel-body">
            <h3>Odczytana konfiguracja z pliku:</h3>
            <samp><?= Config::configurationFile(); ?></samp>
        </div>
        <table class="table table-striped">
            <tr>
                <td class="text-right">Server:</td>
                <td><?= Config::dbServer(); ?></td>
            </tr>
            <tr>
                <td class="text-right">Schema:</td>
                <td><?= Config::dbSchema(); ?></td>
            </tr>
            <tr>
                <td class="text-right">User:</td>
                <td><?= Config::dbUser(); ?></td>
            </tr>
            <tr>
                <td class="text-right">Pass:</td>
                <td><?= Config::dbPass(); ?></td>
            </tr>
        </table>
    </div>

    <?php
    if ($shouldInstall) {
        $installer = new Installer(wipeoutSchema(), seedSample());
        if ($installer->installSpoko()) {
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Zainstalowano poprawnie :)</h3>
                </div>
                <table class="table table-striped">
                    <tr>
                        <td>Trwało to: <?= $installer->getInstalationTime() ?> sekund.</td>
                    </tr>
                    <?php if ($installer->wipeoutSchema()) { ?>
                        <tr>
                            <td>Schemat bazy danych zostało wyczyszczony.</td>
                        </tr>
                    <?php } ?>
                        <tr>
                            <td>
                                Stworzono domyślnego administratora:
                                <ul>
                                    <li>Login: <strong><?= Installer::ADMINISTRATOR_LOGIN ?></strong></li>
                                    <li>Hasło: <strong><?= Installer::ADMINISTRATOR_PASSWORD ?></strong></li>
                                </ul>
                            </td>
                        </tr>
                    <?php if ($installer->seedSamples()) { ?>
                        <tr>
                            <td>Wypełniono przykładowymi danymi.</td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td>
                            By zacząć normalną pracę, usuń plik:<br />
                            <samp><?= realpath(__FILE__); ?></samp>
                        </td>
                    </tr>
                </table>
                <div class="panel-footer">
                    <h3>
                        <a href="index.php">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            Przejdź do na stronę główną
                        </a>
                    </h3>
                </div>
            </div>
            <?php
        } else {
            $occured_error = $installer->getError();
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Wystąpił problem :(</h3>
                </div>
                <div class="panel-body">
                    <strong>W trakcie:</strong>
                    <samp><?= $occured_error["last_action"]; ?></samp>
                    <?php DumpIt($occured_error["last_error"]); ?>
                </div>
            </div>
            <?php
        }
    } else {
        ?>
        <form action="<?= basename(__FILE__) . "?" . INSTALL_ACTION; ?>" method="post">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>Dodatkowa konfiguracja:</h3>
                </div>
                <div class="panel-body">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="<?= WIPEOUT_SCHEMA ?>" />
                            Wyczyścić Schemat bazy danych?
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="<?= SEED_SAMPLES ?>" />
                            Wypełnić przykładowymi danymi?
                        </label>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg pull-right ">
                        Instaluj
                        <span class="glyphicon glyphicon-circle-arrow-right"></span>
                    </button>
                </div>
            </div>
        </form>
        <?php
    }
}

Templater::applyForInstaller(__FILE__);
?>
