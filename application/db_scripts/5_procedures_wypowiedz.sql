DELIMITER $$


DROP PROCEDURE IF EXISTS _WypowiedzStatystyki_Update_Oceny $$
CREATE PROCEDURE _WypowiedzStatystyki_Update_Oceny (
    IN wypowiedzID int(11)
)
BEGIN
    DECLARE _ocenyIlosc int(11);
    DECLARE _ocenySumwaWartosci int(11);

    SELECT
        count(*),
        sum(IF(wo.`Wartosc` = N'+', 1, -1))
    INTO
        _ocenyIlosc,
        _ocenySumwaWartosci
    FROM `WypowiedzOcena` AS wo
        WHERE wo.`Wypowiedz_ID` = wypowiedzID;


    UPDATE `WypowiedzStatystyki`
        SET `Oceny_Ilosc` = ifnull(_ocenyIlosc, 0),
            `Oceny_SumaWartosc` = ifnull(_ocenySumwaWartosci, 0)
        WHERE `Wypowiedz_ID` = wypowiedzID;
END $$

DROP PROCEDURE IF EXISTS _WypowiedzStatystyki_Update_OstatniaWersja $$
CREATE PROCEDURE _WypowiedzStatystyki_Update_OstatniaWersja (
    IN wypowiedzID int(11)
)
BEGIN
    DECLARE _wypowiedzTrescID int(11);
    DECLARE _autorUzytkownikID int(11);
    DECLARE _dataUtworzenia timestamp;

    SELECT
        wt.`WypowiedzTresc_ID`,
        wt.`AutorUzytkownik_ID`,
        wt.`DataUtworzenia`
    INTO
        _wypowiedzTrescID,
        _autorUzytkownikID,
        _dataUtworzenia
    FROM `WypowiedzTresc` AS wt
        WHERE wt.`Wypowiedz_ID` = wypowiedzID
        ORDER BY wt.`DataUtworzenia` DESC
        LIMIT 1;


    UPDATE `WypowiedzStatystyki`
        SET `OstatniaWersja_WypowiedzTresc_ID` = _wypowiedzTrescID,
            `OstatniaWersja_AutorUzytkownik_ID` = _autorUzytkownikID,
            `OstatniaWersja_DataUtworzenia` = _dataUtworzenia
        WHERE `Wypowiedz_ID` = wypowiedzID;
END $$

DROP PROCEDURE IF EXISTS _WypowiedzStatystyki_Update_OstatniKomentarz $$
CREATE PROCEDURE _WypowiedzStatystyki_Update_OstatniKomentarz (
    IN wypowiedzID int(11)
)
BEGIN
    DECLARE _wypowiedzKomentarzID int(11);
    DECLARE _autorUzytkownikID int(11);
    DECLARE _dataUtworzenia timestamp;

    SELECT
        wk.`WypowiedzKomentarz_ID`,
        wk.`AutorUzytkownik_ID`,
        wk.`DataUtworzenia`
    INTO
        _wypowiedzKomentarzID,
        _autorUzytkownikID,
        _dataUtworzenia
    FROM `WypowiedzKomentarz` AS wk
        WHERE wk.`Wypowiedz_ID` = wypowiedzID
        ORDER BY wk.`DataUtworzenia` DESC
        LIMIT 1;


    UPDATE `WypowiedzStatystyki`
        SET `OstatniKomentarz_WypowiedzKomentarz_ID` = _wypowiedzKomentarzID,
            `OstatniKomentarz_AutorUzytkownik_ID` = _autorUzytkownikID,
            `OstatniKomentarz_DataUtworzenia` = _dataUtworzenia
        WHERE `Wypowiedz_ID` = wypowiedzID;
END $$


DROP PROCEDURE IF EXISTS _WypowiedzStatystykiPytanie_Update $$
CREATE PROCEDURE _WypowiedzStatystykiPytanie_Update (
    IN pytanieWypowiedzID int(11)
)
BEGIN
    DECLARE _uczestnicyIlosc int(11);
    DECLARE _uczestnicyWartosc_sum int(11);
    DECLARE _lastPytanie_WypowiedzOcena timestamp;
    DECLARE _lastNew_WypowiedzOdpowiedz timestamp;
    DECLARE _lastVersion_WypowiedzTresc timestamp;

    SELECT count(users.`AutorUzytkownik_ID`) INTO _uczestnicyIlosc
        FROM (
            SELECT wt.`AutorUzytkownik_ID`
                FROM `WypowiedzTresc` AS wt
                WHERE wt.`Wypowiedz_ID` = pytanieWypowiedzID
            UNION
            SELECT wk.`AutorUzytkownik_ID`
                FROM `WypowiedzKomentarz` AS wk
                WHERE wk.`Wypowiedz_ID` = pytanieWypowiedzID
        ) AS users;

    SELECT sum(us.`WypowiedzOcena_SumaWartosci`) INTO _uczestnicyWartosc_sum
        FROM (
            SELECT wt.`AutorUzytkownik_ID`
                FROM `WypowiedzTresc` AS wt
                WHERE wt.`Wypowiedz_ID` = pytanieWypowiedzID
            UNION
            SELECT wk.`AutorUzytkownik_ID`
                FROM `WypowiedzKomentarz` AS wk
                WHERE wk.`Wypowiedz_ID` = pytanieWypowiedzID
        ) AS users
        INNER JOIN `UzytkownikStatystyki` AS us
            ON users.`AutorUzytkownik_ID` = us.`Uzytkownik_ID`;

    SELECT wo.`DataOceny` INTO _lastPytanie_WypowiedzOcena
        FROM `WypowiedzOcena` AS wo
        WHERE wo.`Wypowiedz_ID` = pytanieWypowiedzID
        ORDER BY wo.`DataOceny` DESC
        LIMIT 1;

    SELECT w.`DataUtworzenia` INTO _lastNew_WypowiedzOdpowiedz
        FROM `Wypowiedz` AS w
            NATURAL JOIN `WypowiedzPartitionData` AS wp
        WHERE wp.`PytanieWypowiedz_ID` = pytanieWypowiedzID
        ORDER BY w.`DataUtworzenia` DESC
        LIMIT 1;

    SELECT w.`DataUtworzenia` INTO _lastVersion_WypowiedzTresc
        FROM `Wypowiedz` AS w
            NATURAL JOIN `WypowiedzTresc` AS wt
            NATURAL JOIN `WypowiedzPartitionData` AS wp
        WHERE wp.`PytanieWypowiedz_ID` = pytanieWypowiedzID
        ORDER BY wt.`DataUtworzenia` DESC
        LIMIT 1;


    UPDATE `WypowiedzStatystykiPytanie`
        SET `Uczestnicy_Ilosc` = ifnull(_uczestnicyIlosc, 0),
            `Uczestnicy_Wartosc` = ifnull(_uczestnicyWartosc_sum, 0),
            `OstatniaOcenaPytania` = _lastPytanie_WypowiedzOcena,
            `OstatniaNowaOdpowiedz` = _lastNew_WypowiedzOdpowiedz,
            `OstatniaWypowiedzTresc` = _lastVersion_WypowiedzTresc
        WHERE `Wypowiedz_ID` = pytanieWypowiedzID;
END $$


DROP PROCEDURE IF EXISTS _WypowiedzTresc_CreateVersion $$
CREATE PROCEDURE _WypowiedzTresc_CreateVersion (
    IN tytul nvarchar(200),
    IN tresc text,
    IN autorID int(11),
    IN komentarz nvarchar(50),
    IN forWypowiedzID int(11),
    IN underPytanieWypowiedzID int(11),
    IN creationTime timestamp,
    OUT wypowiedzTrescID int(11)
)
BEGIN
    IF creationTime IS NULL THEN
        SET creationTime = CURRENT_TIMESTAMP;
    END IF;

    INSERT INTO `WypowiedzTresc`
        (`Wypowiedz_ID`, `AutorUzytkownik_ID`, `DataUtworzenia`, `KomentarzWersji`, `Tytul`, `Tresc`)
        VALUES (forWypowiedzID, autorID, creationTime, komentarz, tytul, tresc);

    SELECT wt.`WypowiedzTresc_ID` INTO wypowiedzTrescID
        FROM `WypowiedzTresc` AS wt
        WHERE wt.`Wypowiedz_ID` = forWypowiedzID
            AND wt.`AutorUzytkownik_ID` = autorID
            AND wt.`DataUtworzenia` = creationTime;

    CALL _WypowiedzStatystyki_Update_OstatniaWersja(forWypowiedzID);
    CALL _WypowiedzStatystykiPytanie_Update(underPytanieWypowiedzID);
    CALL _UzytkownikStatystyki_Inc_WypowiedzTresc(autorID, creationTime);
END $$

DROP PROCEDURE IF EXISTS WypowiedzTresc_CreateVersionForPytanie $$
CREATE PROCEDURE WypowiedzTresc_CreateVersionForPytanie (
    IN tytul nvarchar(200),
    IN tresc text,
    IN autorID int(11),
    IN komentarz nvarchar(50),
    IN pytanieWypowiedzID int(11)
)
this_procedure:BEGIN
    DECLARE _ignored_wypowiedzTrescID int(11);

    IF tytul IS NULL THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = N'Tytuł jest wymagany dla Pytania.';
        LEAVE this_procedure;
    END IF;

    CALL _WypowiedzTresc_CreateVersion(tytul, tresc, autorID, komentarz, pytanieWypowiedzID, pytanieWypowiedzID, CURRENT_TIMESTAMP, _ignored_wypowiedzTrescID);
END $$

DROP PROCEDURE IF EXISTS WypowiedzTresc_CreateVersionForOdpowiedz $$
CREATE PROCEDURE WypowiedzTresc_CreateVersionForOdpowiedz (
    IN tresc text,
    IN autorID int(11),
    IN komentarz nvarchar(50),
    IN wypowiedzID int(11)
)
this_procedure:BEGIN
    DECLARE _pytanieWypowiedzID int(11);
    DECLARE _ignored_wypowiedzTrescID int(11);

    START TRANSACTION;

    SELECT wp.`PytanieWypowiedz_ID` INTO _pytanieWypowiedzID
        FROM `WypowiedzPartitionData` AS wp
        WHERE wp.`Wypowiedz_ID` = wypowiedzID;

    CALL _WypowiedzTresc_CreateVersion(NULL, tresc, autorID, komentarz, wypowiedzID, _pytanieWypowiedzID, CURRENT_TIMESTAMP, _ignored_wypowiedzTrescID);

    COMMIT;
END $$


DROP PROCEDURE IF EXISTS _Wypowiedz_Create $$
CREATE PROCEDURE _Wypowiedz_Create (
    IN tytul nvarchar(200),
    IN tresc text,
    IN autorID int(11),
    IN pytanieWypowiedzID int(11),
    OUT wypowiedzID int(11),
    OUT creationTime timestamp
)
BEGIN
    DECLARE _wypowiedzTrescID int(11);
    SET creationTime = CURRENT_TIMESTAMP;

    INSERT INTO `Wypowiedz`
        (`AutorUzytkownik_ID`, `DataUtworzenia`)
        VALUES (autorID, creationTime);

    SELECT w.`Wypowiedz_ID` INTO wypowiedzID
        FROM `Wypowiedz` AS w
        WHERE w.`AutorUzytkownik_ID` = autorID
            AND w.`DataUtworzenia` = creationTime;

    INSERT INTO `WypowiedzPartitionData`
        (`Wypowiedz_ID`, `PytanieWypowiedz_ID`)
        VALUES (wypowiedzID, pytanieWypowiedzID);

    CALL _WypowiedzTresc_CreateVersion(tytul, tresc, autorID, NULL, wypowiedzID, pytanieWypowiedzID, creationTime, _wypowiedzTrescID);

    INSERT INTO `WypowiedzStatystyki`
        (`Wypowiedz_ID`, `OstatniaWersja_WypowiedzTresc_ID`, `OstatniaWersja_AutorUzytkownik_ID`)
        VALUES (wypowiedzID, _wypowiedzTrescID, autorID);

    IF pytanieWypowiedzID IS NULL THEN
        INSERT INTO `WypowiedzStatystykiPytanie`
            (`Wypowiedz_ID`)
            VALUES (wypowiedzID);

        SET pytanieWypowiedzID = wypowiedzID;
    END IF;

    CALL _UzytkownikStatystyki_Inc_Wypowiedz(autorID, creationTime);
END $$

DROP FUNCTION IF EXISTS Wypowiedz_CreatePytanie $$
CREATE FUNCTION Wypowiedz_CreatePytanie (
    tytul nvarchar(200),
    tresc text,
    autorID int(11)
)
RETURNS int(11)
this_procedure:BEGIN
    DECLARE _pytanieWypowiedzID int(11);
    DECLARE _creationTime timestamp;

    IF tytul IS NULL THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = N'Tytuł jest wymagany dla Pytania.';
        LEAVE this_procedure;
    END IF;

    CALL _Wypowiedz_Create(tytul, tresc, autorID, NULL, _pytanieWypowiedzID, _creationTime);
    CALL _UzytkownikStatystyki_Inc_WypowiedzPytanie(autorID, _creationTime);

    RETURN _pytanieWypowiedzID;
END $$

DROP FUNCTION IF EXISTS Wypowiedz_CreateOdpowiedz $$
CREATE FUNCTION Wypowiedz_CreateOdpowiedz (
    tresc text,
    autorID int(11),
    pytanieWypowiedzID int(11)
)
RETURNS int(11)
BEGIN
    DECLARE _odpowiedzWypowiedzID int(11);
    DECLARE _creationTime timestamp;

    CALL _Wypowiedz_Create(NULL, tresc, autorID, pytanieWypowiedzID, _odpowiedzWypowiedzID, _creationTime);

    RETURN _odpowiedzWypowiedzID;
END $$


DROP PROCEDURE IF EXISTS _WypowiedzKomentarz_Create $$
CREATE PROCEDURE _WypowiedzKomentarz_Create (
    IN tresc text,
    IN autorID int(11),
    IN wypowiedzID int(11),
    OUT wypowiedzKomentarzID int(11)
)
BEGIN
    DECLARE _creationTime timestamp
        DEFAULT CURRENT_TIMESTAMP;

    INSERT INTO `WypowiedzKomentarz`
        (`Wypowiedz_ID`, `AutorUzytkownik_ID`, `DataUtworzenia`, `Tresc`)
        VALUES (wypowiedzID, autorID, _creationTime, tresc);

    SELECT wk.`WypowiedzKomentarz_ID` INTO wypowiedzKomentarzID
        FROM `WypowiedzKomentarz` AS wk
        WHERE wk.`Wypowiedz_ID` = wypowiedzID
            AND wk.`AutorUzytkownik_ID` = autorID
            AND wk.`DataUtworzenia` = _creationTime;

    CALL _WypowiedzStatystyki_Update_OstatniKomentarz(wypowiedzID);
END $$

DROP FUNCTION IF EXISTS WypowiedzKomentarz_Create $$
CREATE FUNCTION WypowiedzKomentarz_Create (
    tresc text,
    autorID int(11),
    wypowiedzID int(11)
)
RETURNS int(11)
BEGIN
    DECLARE _wypowiedzKomentarzID int(11);

    CALL _WypowiedzKomentarz_Create(tresc, autorID, wypowiedzID, _wypowiedzKomentarzID);

    RETURN _wypowiedzKomentarzID;
END $$


DROP PROCEDURE IF EXISTS _Wypowiedz_Ocen $$
CREATE PROCEDURE _Wypowiedz_Ocen (
    IN wypowiedzID int(11),
    IN uzytkownikID int(11),
    IN naWartosc nvarchar(1)
)
BEGIN
    DECLARE _wypowiedzAutorID int(11);

    START TRANSACTION;

    IF EXISTS (
        SELECT wo.`Wypowiedz_ID`
            FROM `WypowiedzOcena` AS wo
            WHERE wo.`Wypowiedz_ID` = wypowiedzID
                AND wo.`Uzytkownik_ID` = uzytkownikID
    ) THEN
        UPDATE `WypowiedzOcena`
            SET `DataOceny` = CURRENT_TIMESTAMP,
                `Wartosc` = naWartosc
            WHERE `Wypowiedz_ID` = wypowiedzID
                AND `Uzytkownik_ID` = uzytkownikID;
    ELSE
        INSERT INTO `WypowiedzOcena`
            (`Wypowiedz_ID`, `Uzytkownik_ID`, `Wartosc`)
            VALUES (wypowiedzID, uzytkownikID, naWartosc);
    END IF;

    CALL _WypowiedzStatystyki_Update_Oceny(wypowiedzID);
    CALL _TagStatystyki_UpdateTags_ByWypowiedz(wypowiedzID);

    SELECT w.`AutorUzytkownik_ID` INTO _wypowiedzAutorID
        FROM `Wypowiedz` AS w
        WHERE w.`Wypowiedz_ID` = wypowiedzID;

    CALL _UzytkownikStatystyki_Update_Oceny(_wypowiedzAutorID);

    COMMIT;
END $$

DROP PROCEDURE IF EXISTS Wypowiedz_OcenNaPlus $$
CREATE PROCEDURE Wypowiedz_OcenNaPlus (
    IN wypowiedzID int(11),
    IN uzytkownikID int(11)
)
BEGIN
    CALL _Wypowiedz_Ocen(wypowiedzID, uzytkownikID, N'+');
END $$

DROP PROCEDURE IF EXISTS Wypowiedz_OcenNaMinus $$
CREATE PROCEDURE Wypowiedz_OcenNaMinus (
    IN wypowiedzID int(11),
    IN uzytkownikID int(11)
)
BEGIN
    CALL _Wypowiedz_Ocen(wypowiedzID, uzytkownikID, N'-');
END $$


DROP PROCEDURE IF EXISTS Wypowiedz_SetAs_Uznana $$
CREATE PROCEDURE Wypowiedz_SetAs_Uznana (
    IN wypowiedzID int(11)
)
this_procedure:BEGIN
    DECLARE _pytanieWypowiedzID int(11);

    START TRANSACTION;

    SELECT wp.`PytanieWypowiedz_ID` INTO _pytanieWypowiedzID
        FROM `WypowiedzPartitionData` AS wp
        WHERE wp.`Wypowiedz_ID` = wypowiedzID;

    IF _pytanieWypowiedzID IS NULL THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = N'Nie można uznać Pytania za odpowiedź.';
        LEAVE this_procedure;
    END IF;

    UPDATE `WypowiedzPartitionData`
        SET `CzyUznana` = FALSE
    WHERE `PytanieWypowiedz_ID` = _pytanieWypowiedzID;

    UPDATE `WypowiedzPartitionData`
        SET `CzyUznana` = TRUE
    WHERE `PytanieWypowiedz_ID` = _pytanieWypowiedzID
        AND `Wypowiedz_ID` = wypowiedzID;

    COMMIT;
END $$


DROP PROCEDURE IF EXISTS _Wypowiedz_DeleteOdpowiedzi $$
CREATE PROCEDURE _Wypowiedz_DeleteOdpowiedzi (
    IN pytanieWypowiedzID int(11)
)
BEGIN
    DECLARE _currentWypowiedzId int(11);
    DECLARE _currentWypowiedzAutorId int(11);
    DECLARE _hasMoreOdpwiedzi int(1) DEFAULT true;

    DECLARE _odpowiedziCursor CURSOR FOR
        SELECT w.`Wypowiedz_ID`, w.`AutorUzytkownik_ID`
            FROM `Wypowiedz` AS w
                NATURAL JOIN `WypowiedzPartitionData` AS wp
            WHERE wp.`PytanieWypowiedz_ID` = pytanieWypowiedzID;

    DECLARE CONTINUE HANDLER
        FOR NOT FOUND
        SET _hasMoreOdpwiedzi = false;

    START TRANSACTION;

    OPEN _odpowiedziCursor;

    this_loop: LOOP
        FETCH _odpowiedziCursor
            INTO _currentWypowiedzId, _currentWypowiedzAutorId;

        IF NOT _hasMoreOdpwiedzi THEN
            LEAVE this_loop;
        END IF;

        UPDATE `WypowiedzPartitionData`
            SET `CzyUsunieta` = TRUE
        WHERE `Wypowiedz_ID` = _currentWypowiedzId;

        CALL _UzytkownikStatystyki_Update(_currentWypowiedzAutorId);
    END LOOP;

    CLOSE _odpowiedziCursor;

    COMMIT;
END $$



DROP PROCEDURE IF EXISTS Wypowiedz_Delete $$
CREATE PROCEDURE Wypowiedz_Delete (
    IN wypowiedzID int(11)
)
BEGIN
    DECLARE _pytanieWypowiedzId int(11);
    DECLARE _wypowiedzAutorID int(11);

    START TRANSACTION;

    SELECT
        wp.`PytanieWypowiedz_ID`,
        w.`AutorUzytkownik_ID`
    INTO
        _pytanieWypowiedzID,
        _wypowiedzAutorID
    FROM `Wypowiedz` AS w
        NATURAL JOIN `WypowiedzPartitionData` AS wp
        WHERE w.`Wypowiedz_ID` = wypowiedzID;

    IF _pytanieWypowiedzID IS NULL THEN
        SET _pytanieWypowiedzID = wypowiedzID;
        CALL _Wypowiedz_DeleteOdpowiedzi(_pytanieWypowiedzID);
    END IF;

    UPDATE `WypowiedzPartitionData`
        SET `CzyUznana` = FALSE,
            `CzyUsunieta` = TRUE
    WHERE `Wypowiedz_ID` = wypowiedzID;

    CALL _UzytkownikStatystyki_Update(_wypowiedzAutorID);
    CALL _TagStatystyki_UpdateTags_ByWypowiedz(wypowiedzID);

    COMMIT;
END $$


DELIMITER ;
