DROP VIEW IF EXISTS `Pytania_ListView`;
CREATE VIEW `Pytania_ListView`
AS SELECT
    w.`Wypowiedz_ID`,
    w.`DataUtworzenia`,
    wt.`Tytul`,
    ws.`Oceny_Ilosc`,
    ws.`Oceny_SumaWartosc`,
    w.`AutorUzytkownik_ID`
FROM `Wypowiedz` AS w
    NATURAL JOIN `WypowiedzPartitionData` AS wp
    NATURAL JOIN `WypowiedzStatystyki` AS ws
JOIN `WypowiedzTresc` AS wt
    ON wt.`WypowiedzTresc_ID` = ws.`OstatniaWersja_WypowiedzTresc_ID`
WHERE wp.`PytanieWypowiedz_ID` IS NULL
    AND wp.`CzyUsunieta` = false;


DROP VIEW IF EXISTS `Pytanie_DataView`;
CREATE VIEW `Pytanie_DataView`
AS SELECT
    w.`Wypowiedz_ID`,
    w.`AutorUzytkownik_ID`,
    ws.`Oceny_Ilosc`,
    ws.`Oceny_SumaWartosc`,
    wsp.`Uczestnicy_Ilosc`,
    wt.`DataUtworzenia`,
    wt.`Tytul`,
    wt.`Tresc`
FROM `Wypowiedz` AS w
    NATURAL JOIN `WypowiedzPartitionData` AS wp
    NATURAL JOIN `WypowiedzStatystyki` AS ws
    NATURAL JOIN `WypowiedzStatystykiPytanie` AS wsp
JOIN `WypowiedzTresc` AS wt
    ON wt.`WypowiedzTresc_ID` = ws.`OstatniaWersja_WypowiedzTresc_ID`
WHERE wp.`PytanieWypowiedz_ID` IS NULL
    AND wp.`CzyUsunieta` = false;


DROP VIEW IF EXISTS `Odpowiedz_DataView`;
CREATE VIEW `Odpowiedz_DataView`
AS SELECT
    w.`Wypowiedz_ID`,
    w.`AutorUzytkownik_ID`,
    wp.`PytanieWypowiedz_ID`,
    wp.`CzyUznana`,
    ws.`Oceny_Ilosc`,
    ws.`Oceny_SumaWartosc`,
    wt.`DataUtworzenia`,
    wt.`Tresc`
FROM `Wypowiedz` AS w
    NATURAL JOIN `WypowiedzPartitionData` AS wp
    NATURAL JOIN `WypowiedzStatystyki` AS ws
JOIN `WypowiedzTresc` AS wt
    ON wt.`WypowiedzTresc_ID` = ws.`OstatniaWersja_WypowiedzTresc_ID`
WHERE wp.`PytanieWypowiedz_ID` IS NOT NULL
    AND wp.`CzyUsunieta` = false;


DROP VIEW IF EXISTS `Wypowiedz_Tag_View`;
CREATE VIEW `Wypowiedz_Tag_View`
AS SELECT
    t.`Tag_ID`,
    t.`Nazwa`,
    wt.`Wypowiedz_ID`,
    ts.`Wypowiedz_Ilosc`,
    ts.`Wypowiedz_SumaWartosci`,
    ts.`Uzytkownicy_Ulubiony`
FROM `Wypowiedz_Tag` AS wt
    NATURAL JOIN `Tag`AS t
    NATURAL JOIN `TagStatystyki` AS ts;


DROP VIEW IF EXISTS `Uzytkownik_LinkView`;
CREATE VIEW `Uzytkownik_LinkView`
AS SELECT
    u.`Uzytkownik_ID`,
    u.`Login`,
    u.`PelnaNazwa`,
    u.`Email`,
    u.`DataUtworzenia`,
    us.`Wypowiedz_Ilosc`,
    us.`Wypowiedz_Ostatnia`,
    us.`WypowiedzPytanie_Ilosc`,
    us.`WypowiedzPytanie_Ostatnie`,
    us.`WypowiedzOcena_SumaWartosci`
FROM `Uzytkownik` AS u
    NATURAL JOIN `UzytkownikStatystyki` AS us;

