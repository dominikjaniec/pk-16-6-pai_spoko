DELIMITER $$


DROP FUNCTION IF EXISTS _Password_GetHash $$
CREATE FUNCTION _Password_GetHash (
    haslo nvarchar(255),
    salt nvarchar(50)
)
RETURNS nvarchar(50)
BEGIN
    RETURN password(concat(haslo, salt));
END $$

DROP FUNCTION IF EXISTS _Password_GetSalt $$
CREATE FUNCTION _Password_GetSalt ()
RETURNS nvarchar(50)
BEGIN
    RETURN password(concat(sysdate(), ' ', rand()));
END $$


DROP PROCEDURE IF EXISTS Uzytkownik_SetPassword $$
CREATE PROCEDURE Uzytkownik_SetPassword (
    IN uzytkownikID int(11),
    IN haslo nvarchar(255)
)
BEGIN
    DECLARE _hasloSalt nvarchar(50)
        DEFAULT _Password_GetSalt();
    DECLARE _hasloHash nvarchar(50)
        DEFAULT _Password_GetHash(haslo, _hasloSalt);

    UPDATE `Uzytkownik`
        SET `Haslo_Hash` = _hasloHash,
            `Haslo_Salt` = _hasloSalt
        WHERE `Uzytkownik_ID` = uzytkownikID;
END $$


DROP PROCEDURE IF EXISTS _Uzytkownik_Create $$
CREATE PROCEDURE _Uzytkownik_Create (
    IN login nvarchar(50),
    IN email nvarchar(50),
    IN haslo nvarchar(255),
    IN rolaKlucz nvarchar(25),
    OUT uzytkownikID int(11)
)
this_procedure:BEGIN
    DECLARE _errorMessage nvarchar(255);
    DECLARE _uzytkownik int(11);
    DECLARE _rolaID int(11);

    SELECT Rola_ID INTO _rolaID
        FROM `Rola` AS r
        WHERE r.`Klucz` = rolaKlucz;

    IF _rolaID IS NULL THEN
        SET _errorMessage = concat(N'Rola: "', rolaKlucz, N'" nie istnieje...');
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = _errorMessage;
        LEAVE this_procedure;
    END IF;


    IF EXISTS (
        SELECT u.`Uzytkownik_ID`
            FROM `Uzytkownik` AS u
            WHERE u.`Login` = login
    ) THEN
        SET _errorMessage = concat(N'Użytkownik: "', login, N'" już istnieje...');
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = _errorMessage;
        LEAVE this_procedure;
    END IF;

    IF EXISTS (
        SELECT u.`Uzytkownik_ID`
            FROM `Uzytkownik` AS u
            WHERE u.`Email` = email
    ) THEN
        SET _errorMessage = concat(N'Użytkownik o adresie email: "', email, N'" już istnieje...');
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = _errorMessage;
        LEAVE this_procedure;
    END IF;


    INSERT INTO `Uzytkownik`
        (`Login`, `Haslo_Hash`, `Haslo_Salt`, `Email`, `Rola_ID`)
        VALUES (login, N'_', N'_', email, _rolaID);

    SELECT u.`Uzytkownik_ID` INTO uzytkownikID
        FROM `Uzytkownik` AS u
        WHERE u.`Login` = login;

    INSERT INTO `UzytkownikStatystyki` (`Uzytkownik_ID`)
        VALUES (uzytkownikID);

    CALL Uzytkownik_SetPassword(uzytkownikID, haslo);
END $$

DROP FUNCTION IF EXISTS Uzytkownik_Create $$
CREATE FUNCTION Uzytkownik_Create (
    login nvarchar(50),
    email nvarchar(50),
    haslo nvarchar(255),
    rolaKlucz nvarchar(25)
)
RETURNS int(11)
BEGIN
    DECLARE uzytkownikID int(11);

    CALL _Uzytkownik_Create(login, email, haslo, rolaKlucz, uzytkownikID);

    RETURN uzytkownikID;
END $$


DROP FUNCTION IF EXISTS Uzytkownik_Authenticate $$
CREATE FUNCTION Uzytkownik_Authenticate (
    login nvarchar(50),
    haslo nvarchar(255)
)
RETURNS int(11)
BEGIN
    DECLARE _uzytkownikID int(11);
    DECLARE _uzytkownikHasloHash nvarchar(50);
    DECLARE _uzytkownikHasloSalt nvarchar(50);
    DECLARE _calculatedHash nvarchar(50);

    SELECT u.`Uzytkownik_ID`, u.`Haslo_Hash`, u.`Haslo_Salt`
    INTO _uzytkownikID, _uzytkownikHasloHash, _uzytkownikHasloSalt
        FROM `Uzytkownik` AS u
        WHERE u.`Login` = login;

    SET _calculatedHash = _Password_GetHash(haslo, _uzytkownikHasloSalt);

    IF _uzytkownikID IS NULL OR _calculatedHash != _uzytkownikHasloHash THEN
        SET _uzytkownikID = null;
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = N'Nieprawidłowe dane logowania';
    END IF;

    RETURN _uzytkownikID;
END $$



DROP PROCEDURE IF EXISTS _UzytkownikStatystyki_Inc_Wypowiedz $$
CREATE PROCEDURE _UzytkownikStatystyki_Inc_Wypowiedz (
    IN uzytkownikID int(11),
    IN creationTime timestamp
)
BEGIN
    DECLARE _ilosc int(11);

    SELECT us.`Wypowiedz_Ilosc` INTO _ilosc
        FROM `UzytkownikStatystyki` AS us
        WHERE us.`Uzytkownik_ID` = uzytkownikID;

    UPDATE `UzytkownikStatystyki`
        SET `Wypowiedz_Ilosc` = _ilosc + 1,
            `Wypowiedz_Ostatnia` = creationTime
        WHERE `Uzytkownik_ID` = uzytkownikID;
END $$

DROP PROCEDURE IF EXISTS _UzytkownikStatystyki_Inc_WypowiedzTresc $$
CREATE PROCEDURE _UzytkownikStatystyki_Inc_WypowiedzTresc (
    IN uzytkownikID int(11),
    IN creationTime timestamp
)
BEGIN
    DECLARE _trescIlosc int(11);

    SELECT us.`WypowiedzTresc_Ilosc` INTO _trescIlosc
        FROM `UzytkownikStatystyki` AS us
        WHERE us.`Uzytkownik_ID` = uzytkownikID;

    UPDATE `UzytkownikStatystyki`
        SET `WypowiedzTresc_Ilosc` = _trescIlosc + 1,
            `WypowiedzTresc_Ostatnia` = creationTime
        WHERE `Uzytkownik_ID` = uzytkownikID;
END $$

DROP PROCEDURE IF EXISTS _UzytkownikStatystyki_Inc_WypowiedzPytanie $$
CREATE PROCEDURE _UzytkownikStatystyki_Inc_WypowiedzPytanie (
    IN uzytkownikID int(11),
    IN creationTime timestamp
)
BEGIN
    DECLARE _pytanieIlosc int(11);

    SELECT us.`WypowiedzPytanie_Ilosc` INTO _pytanieIlosc
        FROM `UzytkownikStatystyki` AS us
        WHERE us.`Uzytkownik_ID` = uzytkownikID;

    UPDATE `UzytkownikStatystyki`
        SET `WypowiedzPytanie_Ilosc` = _pytanieIlosc + 1,
            `WypowiedzPytanie_Ostatnie` = creationTime
        WHERE `Uzytkownik_ID` = uzytkownikID;
END $$

DROP FUNCTION IF EXISTS _UzytkownikStatystyki_CalculateOceny $$
CREATE FUNCTION _UzytkownikStatystyki_CalculateOceny (
    uzytkownikID int(11)
)
RETURNS int(11)
BEGIN
    DECLARE _ocenySumaWartosci int(11);

    SELECT
        sum(IF(wo.`Wartosc` = N'+', 1, -1))
    INTO
        _ocenySumaWartosci
    FROM `Wypowiedz` AS w
        NATURAL JOIN `WypowiedzOcena` AS wo
	    NATURAL JOIN `WypowiedzPartitionData` AS wp
    WHERE w.`AutorUzytkownik_ID` = uzytkownikID
        AND wo.`Uzytkownik_ID` != uzytkownikID
        AND wp.`CzyUsunieta` = FALSE;

    RETURN ifnull(_ocenySumaWartosci, 0);
END $$

DROP PROCEDURE IF EXISTS _UzytkownikStatystyki_Update_Oceny $$
CREATE PROCEDURE _UzytkownikStatystyki_Update_Oceny (
    IN uzytkownikID int(11)
)
BEGIN
    DECLARE _currentSumaOcen int(11);

    SET _currentSumaOcen = _UzytkownikStatystyki_CalculateOceny(uzytkownikID);

    UPDATE `UzytkownikStatystyki`
        SET `WypowiedzOcena_SumaWartosci` = _currentSumaOcen
        WHERE `Uzytkownik_ID` = uzytkownikID;
END $$

DROP PROCEDURE IF EXISTS _UzytkownikStatystyki_Update $$
CREATE PROCEDURE _UzytkownikStatystyki_Update (
    IN uzytkownikID int(11)
)
BEGIN
    DECLARE _wypowiedziIlosc int(11);
    DECLARE _wypowiedzOstatnia timestamp;
    DECLARE _wypowiedzTresciIlosc int(11);
    DECLARE _wypowiedzTrescOstatnia timestamp;
    DECLARE _wypowiedzPytaniaIlosc int(11);
    DECLARE _wypowiedzPytanieOstatnie timestamp;
    DECLARE _wypowiedzOcenaSumaWartosci int(11);

    SELECT count(w.`Wypowiedz_ID`) INTO _wypowiedziIlosc
        FROM `Wypowiedz` AS w
            NATURAL JOIN `WypowiedzPartitionData` AS wp
        WHERE w.`AutorUzytkownik_ID` = uzytkownikID
            AND wp.`CzyUsunieta` = FALSE;

    SELECT w.`DataUtworzenia` INTO _wypowiedzOstatnia
        FROM `Wypowiedz` AS w
            NATURAL JOIN `WypowiedzPartitionData` AS wp
        WHERE w.`AutorUzytkownik_ID` = uzytkownikID
            AND wp.`CzyUsunieta` = FALSE
        ORDER BY w.`DataUtworzenia` DESC
        LIMIT 1;


    SELECT count(wt.`WypowiedzTresc_ID`) INTO _wypowiedzTresciIlosc
        FROM `WypowiedzTresc` wt
            NATURAL JOIN `WypowiedzPartitionData` AS wp
        WHERE wt.`AutorUzytkownik_ID` = uzytkownikID
            AND wp.`CzyUsunieta` = FALSE;

    SELECT wt.`DataUtworzenia` INTO _wypowiedzTrescOstatnia
        FROM `WypowiedzTresc` AS wt
            NATURAL JOIN `WypowiedzPartitionData` AS wp
        WHERE wt.`AutorUzytkownik_ID` = uzytkownikID
            AND wp.`CzyUsunieta` = FALSE
        ORDER BY wt.`DataUtworzenia` DESC
        LIMIT 1;


    SELECT count(w.`Wypowiedz_ID`) INTO _wypowiedzPytaniaIlosc
        FROM `Wypowiedz` AS w
            NATURAL JOIN `WypowiedzPartitionData` AS wp
        WHERE w.`AutorUzytkownik_ID` = uzytkownikID
            AND wp.`PytanieWypowiedz_ID` IS NULL
            AND wp.`CzyUsunieta` = FALSE;

    SELECT w.`DataUtworzenia` INTO _wypowiedzPytanieOstatnie
        FROM `Wypowiedz` AS w
            NATURAL JOIN `WypowiedzPartitionData` AS wp
        WHERE w.`AutorUzytkownik_ID` = uzytkownikID
            AND wp.`PytanieWypowiedz_ID` IS NULL
            AND wp.`CzyUsunieta` = FALSE
        ORDER BY w.`DataUtworzenia` DESC
        LIMIT 1;

    SET _wypowiedzOcenaSumaWartosci = _UzytkownikStatystyki_CalculateOceny(uzytkownikID);

    UPDATE `UzytkownikStatystyki`
        SET `Wypowiedz_Ilosc` = ifnull(_wypowiedziIlosc, 0),
            `Wypowiedz_Ostatnia` = _wypowiedzOstatnia,
            `WypowiedzTresc_Ilosc` = ifnull(_wypowiedzTresciIlosc, 0),
            `WypowiedzTresc_Ostatnia` = _wypowiedzTrescOstatnia,
            `WypowiedzPytanie_Ilosc` = ifnull(_wypowiedzPytaniaIlosc, 0),
            `WypowiedzPytanie_Ostatnie` = _wypowiedzPytanieOstatnie,
            `WypowiedzOcena_SumaWartosci` = ifnull(_wypowiedzOcenaSumaWartosci, 0)
        WHERE `Uzytkownik_ID` = uzytkownikID;
END $$


DELIMITER ;
