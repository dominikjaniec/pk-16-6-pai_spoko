DELIMITER $$


DROP PROCEDURE IF EXISTS _Tag_CreateAsSubtag $$
CREATE PROCEDURE _Tag_CreateAsSubtag (
    IN tagNazwa nvarchar(20),
    IN nadrzednyTagID int(11),
    OUT tagID int(11)
)
this_procedure:BEGIN
    DECLARE _errorMessage nvarchar(255);

    IF EXISTS (
        SELECT t.`Tag_ID`
            FROM `Tag` AS t
            WHERE lower(t.`Nazwa`) = lower(tagNazwa)
    ) THEN
        SET _errorMessage = concat(N'Istnieje już Tag o nazwie: "', tagNazwa, N'".');
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = _errorMessage;
        LEAVE this_procedure;
    END IF;

    INSERT INTO `Tag`
        (`Nazwa`, `Soundex`, `NadrzednyTag_ID`)
        VALUES (tagNazwa, soundex(tagNazwa), nadrzednyTagID);

    SELECT t.`Tag_ID` INTO tagID
        FROM `Tag` AS t
        WHERE t.`Nazwa` = tagNazwa;

    INSERT INTO `TagStatystyki` (`Tag_ID`)
        VALUES (tagID);
END $$

DROP FUNCTION IF EXISTS Tag_CreateAsSubtag $$
CREATE FUNCTION Tag_CreateAsSubtag (
    tagNazwa nvarchar(20),
    nadrzednyTagID int(11)
)
RETURNS int(11)
BEGIN
    DECLARE _tagID int(11);

    CALL _Tag_CreateAsSubtag(tagNazwa, nadrzednyTagID, _tagID);

    RETURN _tagID;
END $$

DROP FUNCTION IF EXISTS Tag_Create $$
CREATE FUNCTION Tag_Create (
    tagNazwa nvarchar(20)
)
RETURNS int(11)
BEGIN
    DECLARE _tagID int(11);

    CALL _Tag_CreateAsSubtag(tagNazwa, NULL, _tagID);

    RETURN _tagID;
END $$


DROP PROCEDURE IF EXISTS _TagStatystyki_Update_Wypowiedz $$
CREATE PROCEDURE _TagStatystyki_Update_Wypowiedz (
    IN tagID int(11)
)
BEGIN
    DECLARE _wypowiedzIlosc int(11);
    DECLARE _wypowiedzSumaWartosci int(11);

    START TRANSACTION;

    SELECT count(wt.`Wypowiedz_ID`) INTO _wypowiedzIlosc
        FROM `Wypowiedz_Tag` AS wt
            NATURAL JOIN `WypowiedzPartitionData` AS wp
        WHERE wt.`Tag_ID` = tagID
            AND wp.`CzyUsunieta` = FALSE;

    SELECT sum(ws.`Oceny_SumaWartosc`) INTO _wypowiedzSumaWartosci
        FROM `Wypowiedz_Tag` AS wt
            NATURAL JOIN `WypowiedzPartitionData` AS wp
            NATURAL JOIN `WypowiedzStatystyki` AS ws
        WHERE wt.`Tag_ID` = tagID
            AND wp.`CzyUsunieta` = FALSE;

    UPDATE `TagStatystyki`
        SET `Wypowiedz_Ilosc` = ifnull(_wypowiedzIlosc, 0),
            `Wypowiedz_SumaWartosci` = ifnull(_wypowiedzSumaWartosci, 0)
        WHERE `Tag_ID` = tagID;

    COMMIT;
END $$


DROP PROCEDURE IF EXISTS _TagStatystyki_Update_Uzytkownicy $$
CREATE PROCEDURE _TagStatystyki_Update_Uzytkownicy (
    IN tagID int(11)
)
BEGIN
    DECLARE _uzytkownicyCountUlubiony int(11);
    DECLARE _uzytkownicyCountIgnorowany int(11);

    START TRANSACTION;

    SELECT count(ut.`Uzytkownik_ID`)
    INTO _uzytkownicyCountUlubiony
        FROM `Uzytkownik_Tag` AS ut
            NATURAL JOIN `Uzytkownik` AS u
        WHERE ut.`TypUzycia` = N'ulubiony'
            AND ut.`Tag_ID` = tagID;

    SELECT count(ut.`Uzytkownik_ID`)
    INTO _uzytkownicyCountIgnorowany
        FROM `Uzytkownik_Tag` AS ut
            NATURAL JOIN `Uzytkownik` AS u
        WHERE ut.`TypUzycia` = N'ignorowany'
            AND ut.`Tag_ID` = tagID;

    UPDATE `TagStatystyki` AS ts
        SET ts.`Uzytkownicy_Ulubiony` = ifnull(_uzytkownicyCountUlubiony, 0),
            ts.`Uzytkownicy_Ignorowany` = ifnull(_uzytkownicyCountIgnorowany, 0)
        WHERE ts.`Tag_ID` = tagID;

    COMMIT;
END $$


DROP PROCEDURE IF EXISTS _TagStatystyki_UpdateTags_ByWypowiedz $$
CREATE PROCEDURE _TagStatystyki_UpdateTags_ByWypowiedz (
    IN wypowiedzID int(11)
)
BEGIN
    DECLARE _currentTagID int(11);
    DECLARE _hasMore int(1) DEFAULT true;

    DECLARE _tagCursor CURSOR FOR
        SELECT wt.`Tag_ID`
            FROM `Wypowiedz_Tag` AS wt
            WHERE wt.`Wypowiedz_ID` = wypowiedzID;

    DECLARE CONTINUE HANDLER
        FOR NOT FOUND
        SET _hasMore = false;

    START TRANSACTION;

    OPEN _tagCursor;

    this_loop: LOOP
        FETCH _tagCursor
            INTO _currentTagID;

        IF NOT _hasMore THEN
            LEAVE this_loop;
        END IF;

        CALL _TagStatystyki_Update_Wypowiedz(_currentTagID);
    END LOOP;

    CLOSE _tagCursor;

    COMMIT;
END $$


DELIMITER ;
