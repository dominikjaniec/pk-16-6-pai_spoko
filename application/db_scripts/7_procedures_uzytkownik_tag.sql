DELIMITER $$


DROP PROCEDURE IF EXISTS _Uzytkownik_Tag_Set $$
CREATE PROCEDURE _Uzytkownik_Tag_Set (
    IN tagID int(11),
    IN uzytkownikID int(11),
    IN typUzycia NVARCHAR(10)
)
BEGIN
    START TRANSACTION;

    IF EXISTS (
        SELECT ut.`TypUzycia`
            FROM `Uzytkownik_Tag` AS ut
            WHERE ut.`Uzytkownik_ID` = uzytkownikID
                AND ut.`Tag_ID` = tagID
    ) THEN
        UPDATE `Uzytkownik_Tag` AS ut
            SET ut.`TypUzycia` = typUzycia
            WHERE ut.`Uzytkownik_ID` = uzytkownikID
                AND ut.`Tag_ID` = tagID;
    ELSE
        INSERT INTO `Uzytkownik_Tag`
            (`Uzytkownik_ID`, `Tag_ID`, `TypUzycia`)
            VALUES (uzytkownikID, tagID, typUzycia);
    END IF;

    CALL _TagStatystyki_Update_Uzytkownicy(tagID);

    COMMIT;
END $$


DROP PROCEDURE IF EXISTS Uzytkownik_Tag_SetAsUlubiony $$
CREATE PROCEDURE Uzytkownik_Tag_SetAsUlubiony (
    IN tagID int(11),
    IN uzytkownikID int(11)
)
BEGIN
    CALL _Uzytkownik_Tag_Set(tagID, uzytkownikID, N'ulubiony');
END $$


DROP PROCEDURE IF EXISTS Uzytkownik_Tag_SetAsIgnorowany $$
CREATE PROCEDURE Uzytkownik_Tag_SetAsIgnorowany (
    IN tagID int(11),
    IN uzytkownikID int(11)
)
BEGIN
    CALL _Uzytkownik_Tag_Set(tagID, uzytkownikID, N'ignorowany');
END $$


DROP PROCEDURE IF EXISTS Uzytkownik_Tag_Unset $$
CREATE PROCEDURE Uzytkownik_Tag_Unset (
    IN tagID int(11),
    IN uzytkownikID int(11)
)
BEGIN
    START TRANSACTION;

    DELETE
        FROM `Uzytkownik_Tag`
        WHERE `Uzytkownik_ID` = uzytkownikID
            AND `Tag_ID` = tagID;

    CALL _TagStatystyki_Update_Uzytkownicy(tagID);

    COMMIT;
END $$


DELIMITER ;
