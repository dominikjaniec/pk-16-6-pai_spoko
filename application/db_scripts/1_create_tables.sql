CREATE TABLE `Rola` (
  `Rola_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nazwa` nvarchar(50) NOT NULL,
  `Klucz` nvarchar(25) NOT NULL,
  PRIMARY KEY (`Rola_ID`),
  UNIQUE KEY `UQ_Klucz` (`Klucz`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `Tag` (
  `Tag_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nazwa` nvarchar(20) NOT NULL,
  `Soundex` nvarchar(20) NOT NULL,
  `NadrzednyTag_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Tag_ID`),
  UNIQUE KEY `UQ_Nazwa` (`Nazwa`),
  KEY `FK_NadrzednyTag_ID` (`NadrzednyTag_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `TagStatystyki` (
  `Tag_ID` int(11) NOT NULL,
  `Wypowiedz_Ilosc` int(11) NOT NULL DEFAULT '0',
  `Wypowiedz_SumaWartosci` int(11) NOT NULL DEFAULT '0',
  `Uzytkownicy_Ulubiony` int(11) NOT NULL DEFAULT '0',
  `Uzytkownicy_Ignorowany` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Tag_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `Uzytkownik` (
  `Uzytkownik_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DataUtworzenia` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Login` nvarchar(50) NOT NULL,
  `Haslo_Hash` nvarchar(50) NOT NULL,
  `Haslo_Salt` nvarchar(50) NOT NULL,
  `PelnaNazwa` nvarchar(100) DEFAULT NULL,
  `Email` nvarchar(50) NOT NULL,
  `Rola_ID` int(11) NOT NULL,
  PRIMARY KEY (`Uzytkownik_ID`),
  UNIQUE KEY `UQ_Login` (`Login`),
  UNIQUE KEY `UQ_Email` (`Email`),
  KEY `FK_Rola_ID` (`Rola_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `UzytkownikLogowania` (
  `Uzytkownik_ID` int(11) NOT NULL,
  `DataLogowania` datetime NOT NULL,
  `SourceHost` nvarchar(200) NOT NULL,
  `UserAgent` nvarchar(400) NOT NULL,
  UNIQUE KEY `UQ_UzytkownikLogowanie` (`Uzytkownik_ID`, `DataLogowania`),
  KEY `FK_Uzytkownik_ID` (`Uzytkownik_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1
  PARTITION BY HASH ( YEAR(`DataLogowania`) )
    PARTITIONS 7;


CREATE TABLE `UzytkownikSesja` (
  `Session_KEY` nvarchar(50) NOT NULL,
  `Session_VALUE` text NOT NULL,
  `ExpirationTime` timestamp NOT NULL,
  PRIMARY KEY (`Session_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `UzytkownikStatystyki` (
  `Uzytkownik_ID` int(11) NOT NULL,
  `Wypowiedz_Ilosc` int(11) NOT NULL DEFAULT '0',
  `Wypowiedz_Ostatnia` timestamp NULL DEFAULT NULL,
  `WypowiedzTresc_Ilosc` int(11) NOT NULL DEFAULT '0',
  `WypowiedzTresc_Ostatnia` timestamp NULL DEFAULT NULL,
  `WypowiedzPytanie_Ilosc` int(11) NOT NULL DEFAULT '0',
  `WypowiedzPytanie_Ostatnie` timestamp NULL DEFAULT NULL,
  `ProfilOdslona_Ilosc` int(11) NOT NULL DEFAULT '0',
  `ProfilOdslona_Ostatnia` timestamp NULL DEFAULT NULL,
  `WypowiedzOcena_SumaWartosci` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uzytkownik_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `Uzytkownik_Tag` (
  `Uzytkownik_ID` int(11) NOT NULL,
  `Tag_ID` int(11) NOT NULL,
  `TypUzycia` enum('ulubiony','ignorowany') NOT NULL,
  PRIMARY KEY (`Uzytkownik_ID`,`Tag_ID`),
  KEY `FK_Uzytkownik_ID` (`Uzytkownik_ID`),
  KEY `FK_Tag_ID` (`Tag_ID`),
  KEY `FK_TypUzycia` (`TypUzycia`),
  INDEX (`Uzytkownik_ID`, `TypUzycia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `Wypowiedz` (
  `Wypowiedz_ID` int(11) NOT NULL AUTO_INCREMENT,
  `AutorUzytkownik_ID` int(11) NOT NULL,
  `DataUtworzenia` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Wypowiedz_ID`),
  UNIQUE KEY `UQ_AutorUtworzenie` (`AutorUzytkownik_ID`, `DataUtworzenia`),
  KEY `FK_AutorUzytkownik_ID` (`AutorUzytkownik_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `WypowiedzPartitionData` (
  `Wypowiedz_ID` int(11) NOT NULL,
  `CzyUznana` tinyint(1) NOT NULL DEFAULT '0',
  `CzyUsunieta` tinyint(1) NOT NULL DEFAULT '0',
  `PytanieWypowiedz_ID` int(11) DEFAULT NULL,
  KEY `FK_PytanieWypowiedz_ID` (`PytanieWypowiedz_ID`),
  INDEX (`CzyUsunieta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1
  PARTITION BY RANGE ( `PytanieWypowiedz_ID` ) (
    PARTITION part_pytania VALUES LESS THAN (1),
    PARTITION part_odpowiedzi VALUES LESS THAN MAXVALUE
);


CREATE TABLE `WypowiedzKomentarz` (
  `WypowiedzKomentarz_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Wypowiedz_ID` int(11) NOT NULL,
  `AutorUzytkownik_ID` int(11) NOT NULL,
  `DataUtworzenia` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Tresc` text NOT NULL,
  PRIMARY KEY (`WypowiedzKomentarz_ID`),
  UNIQUE KEY `UQ_WypowiedzAutorUtworzenie` (`Wypowiedz_ID`, `AutorUzytkownik_ID`, `DataUtworzenia`),
  KEY `FK_Wypowiedz_ID` (`Wypowiedz_ID`),
  KEY `FK_AutorUzytkownik_ID` (`AutorUzytkownik_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `WypowiedzOcena` (
  `Wypowiedz_ID` int(11) NOT NULL,
  `Uzytkownik_ID` int(11) NOT NULL,
  `DataOceny` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Wartosc` enum('+','-') NOT NULL,
  PRIMARY KEY (`Wypowiedz_ID`,`Uzytkownik_ID`),
  KEY `FK_Wypowiedz_ID` (`Wypowiedz_ID`),
  KEY `FK_Uzytkownik_ID` (`Uzytkownik_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `WypowiedzOdslona` (
  `Wypowiedz_ID` int(11) NOT NULL,
  `Uzytkownik_ID` int(11) NOT NULL,
  `DataOdslony` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Wypowiedz_ID`,`Uzytkownik_ID`),
  KEY `FK_Wypowiedz_ID` (`Wypowiedz_ID`),
  KEY `FK_Uzytkownik_ID` (`Uzytkownik_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `WypowiedzStatystyki` (
  `Wypowiedz_ID` int(11) NOT NULL,
  `Oceny_Ilosc` int(11) NOT NULL DEFAULT '0',
  `Oceny_SumaWartosc` int(11) NOT NULL DEFAULT '0',
  `OstatniaWersja_DataUtworzenia` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `OstatniaWersja_WypowiedzTresc_ID` int(11) NOT NULL,
  `OstatniaWersja_AutorUzytkownik_ID` int(11) NOT NULL,
  `OstatniKomentarz_DataUtworzenia` TIMESTAMP NULL,
  `OstatniKomentarz_WypowiedzKomentarz_ID` INT(11) NULL,
  `OstatniKomentarz_AutorUzytkownik_ID` INT(11) NULL,
  PRIMARY KEY (`Wypowiedz_ID`),
  KEY `FK_OstatniaWersja_WypowiedzTresc_ID` (`OstatniaWersja_WypowiedzTresc_ID`),
  KEY `FK_OstatniaWersja_AutorUzytkownik_ID` (`OstatniaWersja_AutorUzytkownik_ID`),
  KEY `FK_OstatniKomentarz_WypowiedzKomentarz_ID` (`OstatniKomentarz_WypowiedzKomentarz_ID`),
  KEY `FK_OstatniKomentarz_AutorUzytkownik_ID` (`OstatniKomentarz_AutorUzytkownik_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `WypowiedzStatystykiPytanie` (
  `Wypowiedz_ID` int(11) NOT NULL,
  `Uczestnicy_Ilosc` int(11) NOT NULL DEFAULT '0',
  `Uczestnicy_Wartosc` int(11) NOT NULL DEFAULT '0',
  `OstatniaOcenaPytania` timestamp NULL DEFAULT NULL,
  `OstatniaNowaOdpowiedz` timestamp NULL DEFAULT NULL,
  `OstatniaWypowiedzTresc` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Wypowiedz_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `WypowiedzTresc` (
  `WypowiedzTresc_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Wypowiedz_ID` int(11) NOT NULL,
  `AutorUzytkownik_ID` int(11) NOT NULL,
  `DataUtworzenia` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `KomentarzWersji` nvarchar(50) DEFAULT NULL,
  `Tytul` nvarchar(200) NULL,
  `Tresc` text NOT NULL,
  PRIMARY KEY (`WypowiedzTresc_ID`),
  UNIQUE KEY `UQ_AutorTrescWypowiedz` (`AutorUzytkownik_ID`, `DataUtworzenia`, `Wypowiedz_ID`),
  KEY `FK_Wypowiedz_ID` (`Wypowiedz_ID`),
  KEY `FK_AutorUzytkownik_ID` (`AutorUzytkownik_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `Wypowiedz_Tag` (
  `Wypowiedz_ID` int(11) NOT NULL,
  `Tag_ID` int(11) NOT NULL,
  PRIMARY KEY (`Wypowiedz_ID`,`Tag_ID`),
  KEY `FK_Wypowiedz_ID` (`Wypowiedz_ID`),
  KEY `FK_Tag_ID` (`Tag_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `SearchWord` (
  `SearchWord_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Word` nvarchar(250) NOT NULL,
  PRIMARY KEY (`SearchWord_ID`),
  UNIQUE KEY `UQ_Word` (`Word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE `SearchMatch_Wypowiedz` (
  `SearchWord_ID` int(11) NOT NULL,
  `Wypowiedz_ID` int(11) NOT NULL,
  `PytanieWypowiedz_ID` int(11),
  `IsTitle` tinyint(1) NOT NULL,
  `Count` int(11) NOT NULL,
  PRIMARY KEY (`SearchWord_ID`, `Wypowiedz_ID`, `IsTitle`),
  KEY `FK_SearchWord_ID` (`SearchWord_ID`),
  KEY `FK_Wypowiedz_ID` (`Wypowiedz_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `SearchQueryWord` (
  `Identity` nvarchar(25) NOT NULL,
  `Word` nvarchar(250) NOT NULL,
  `QueryTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
