<?php

require_once '../common/DataBase.php';
require_once '../common/Helpers.php';
require_once '../common/Roles.php';
require_once '../model/UzytkownikModel.php';
require_once '../model/PytanieModel.php';
require_once '../model/OdpowiedzModel.php';
require_once '../model/KomentarzModel.php';
require_once '../model/TagModel.php';

class Sampler {

    private $fake_time;
    private $db_connection;
    private $users;
    private $tags;

    public function seed() {
        $this->init();

        $this->createSampleUsers();
        $this->createSampleTags();
        $this->connectUsersWithTags();

        $this->createSampleQuestionsWithoutResponses();
        $this->createSampleQuestionWithDoubleResponses();
        $this->createSampleQuestionWithDoubleResponsesButFirstRemoved();
        $this->createSampleQuestionWithTripleResponsesAndAccepted();
        $this->createSampleRemovedQuestionWithResponsesAndRaitngs();
        $this->createSampleComplexQuestionWithEverything();
    }

    private function init() {
        $this->initFakeTime();
        $this->db_connection = DataBase::GetConnection();

        $this->users = array(
            "admin" => -1,
            "moder" => -1,
            "user" => -1,
            "ada" => -1
        );

        $this->tags = array(
            "normalny-tag" => -1,
            "ulubiony-tag" => -1,
            "ignorowany-tag" => -1,
            "inny-taki-tag" => -1,
            "totalnie-nie-używany" => -1,
            "z-podrzędnymi" => array(
                "pierwszy-podrzędny" => -1,
                "drugi-podrzędny" => -1
            )
        );
    }

    private function initFakeTime() {
        $fake_date = mktime(8, 16, 59, date("m"), date("d"), date("Y"));
        $fake_date -= (24 * 60 * 60); // yesterday
        $this->fake_time = $fake_date;
    }

    private function getFakeTime() {
        $_10minutes = (10 * 60);
        $_20minutes = (20 * 60);
        $span = 4 + rand($_10minutes, $_20minutes);

        $this->fake_time += $span;
        return $this->fake_time;
    }

    private function createSampleUsers() {
        $users_roles = array(
            "admin" => Roles::ADMIN,
            "moder" => Roles::MOD,
            "user" => Roles::USER,
            "ada" => Roles::USER
        );

        foreach ($this->users as $login => $oldID) {
            $pass = "qwe123";
            $email = $login . "@example.com";
            $rola = $users_roles[$login];

            $userID = UzytkownikModel::create($login, $pass, $email, $rola);
            $this->users[$login] = $userID;
        }
    }

    private function createSampleTags() {
        $sql_st = $this->db_connection->prepare(
                "SELECT Tag_Create(:tag)");

        foreach ($this->tags as $tag => $sub) {
            $sql_st->bindParam(":tag", $tag, PDO::PARAM_STR, 20);
            $sql_st->execute();

            $creation_result = $sql_st->fetch(PDO::FETCH_NUM);
            $result = intval($creation_result[0]);

            if ($sub === -1) {
                $this->tags[$tag] = $result;
            } else {
                $subTags = array();
                $sub_statement = $this->db_connection->prepare(
                        "SELECT Tag_CreateAsSubtag(:tag, :nadrzednyTagID)");

                foreach ($sub as $subtag => $ignored) {
                    $sub_statement->bindParam(":tag", $subtag, PDO::PARAM_STR, 20);
                    $sub_statement->bindParam(":nadrzednyTagID", $result, PDO::PARAM_INT);
                    $sub_statement->execute();

                    $creation_result = $sub_statement->fetch(PDO::FETCH_NUM);
                    $subTags[$subtag] = intval($creation_result[0]);
                }

                $this->tags[$tag] = $result;
                $this->tags = array_merge($this->tags, $subTags);
            }
        }
    }

    private function connectUsersWithTags() {
        TagModel::setUserTagsUsage($this->users["user"], array("ulubiony-tag"), array("ignorowany-tag"));
        TagModel::setUserTagsUsage($this->users["moder"], array("ulubiony-tag"), array("z-podrzędnymi"));
        TagModel::setUserTagsUsage($this->users["ada"], array("inny-taki-tag"), array("normalny-tag"));
        TagModel::setUserTagsUsage($this->users["ada"], array(), array());
    }

    private function createSampleQuestionsWithoutResponses() {
        $question_1ID = $this->createQuestion("Pierwsze pytanie", "W sumie totalnie bez sensu...", "admin");

        $question_2ID = $this->createQuestion("Drugie normalne pytanie", "Ogólnie Ziema krąży w koło Słońca. Czy mam racaję?", "admin");
        TagModel::setForPytanie($question_2ID, array("normalny-tag"));

        $question_3ID = $this->createQuestion("Powiedzmy", "Czy czas jest ziarnisty?", "admin");
        TagModel::setForPytanie($question_3ID, array("normalny-tag", "inny-taki-tag", "z-podrzędnymi"));
    }

    private function createSampleQuestionWithDoubleResponses() {
        $question_1ID = $this->createQuestion("Podwojnie odpowiedziane", "To pytanie jest bez znaczenia... Ale jakiego koloru jest cisza?", "ada");
        $this->createResponse($question_1ID, "Cóż można powiedzieć, tak samo jak brzmi światło...", "user");
        $this->createResponse($question_1ID, "Grube wypowiedzi tu widzę ;)", "moder");
        TagModel::setForPytanie($question_1ID, array("pierwszy-podrzędny", "inny-taki-tag"));
    }

    private function createSampleQuestionWithDoubleResponsesButFirstRemoved() {
        $question_2ID = $this->createQuestion("Inne dualne", "Ying Yang, czy raczej tylko mieszanie farb?", "user");
        $this->createResponse($question_2ID, "Ja tam wole mieszać dzwięki", "admin");
        $response_to_remove = $this->createResponse($question_2ID, "Dualność wynika z natury tkaniny wszechświata", "ada");
        TagModel::setForPytanie($question_2ID, array("ulubiony-tag", "z-podrzędnymi", "pierwszy-podrzędny"));
        $this->removeStatement($response_to_remove);
    }

    private function createSampleQuestionWithTripleResponsesAndAccepted() {
        $question = $this->createQuestion("Z drugą zaakceptowaną", "Zgadnijcie którą odpowiedź uznam za poprawną?", "user");
        $this->createResponse($question, "Pewnie nie moją...", "ada");
        $accepted_response = $this->createResponse($question, "W sumie szczęscia nie mam.", "moder");
        $this->createResponse($question, "I tak bez znaczenia, bana zaraz dostaniesz.", "admin");
        TagModel::setForPytanie($question, array("ulubiony-tag", "ignorowany-tag", "drugi-podrzędny"));
        OdpowiedzModel::accept($accepted_response);
    }

    private function createSampleRemovedQuestionWithResponsesAndRaitngs() {
        $question = $this->createQuestion("Będzie usuniety, albo już jest", "Tak jak podatki, usunięcie też jest nieuniknione...", "moder");
        TagModel::setForPytanie($question, array("ulubiony-tag", "z-podrzędnymi"));
        $this->rateStatementUp($question, "admin");
        $this->rateStatementDown($question, "user");

        $accepted_response = $this->createResponse($question, "Cóż poradzić, czas jak rzeka.", "ada");
        $this->rateStatementDown($accepted_response, "user");
        $this->rateStatementUp($question, "moder");
        $this->rateStatementUp($question, "admin");
        OdpowiedzModel::accept($accepted_response);

        $rated_down_response = $this->createResponse($question, "Jak lawina, wszysto się sypie.", "user");
        $this->rateStatementDown($rated_down_response, "admin");

        $this->removeStatement($question);
    }

    private function createSampleComplexQuestionWithEverything() {
        $question = $this->createQuestion("Pytanie co będzie spoko", "Można by się postarać w takim ważnym pytaniu. W sumie pewnie miło było by gdyby to było jakieś życiowe pytanie, na które można by sesownie odpowiedzieć, a później w sumie przekazać wiedzę potomnym. Co zjeść na kolację?", "ada");
        $this->createVersionOfQuestion($question, "Powiedzmy, że to będzie takie SPOKO", "Można by się postarać w takim ważnym pytaniu. W sumie pewnie miło było by gdyby to było jakieś życiowe pytanie, na które można by sesownie odpowiedzieć, a później w sumie przekazać wiedzę potomnym. No ale w sumie pytanie brzmi: \"Ultimate Question of Life, the Universe, and Everything\"?", "ada", "Trochę sensowniejsze pytanie...");
        $this->createCommentUnder($question, "Ale o co Ci chodzi?", "admin");
        $this->createCommentUnder($question, "Poprawione :)", "ada");
        $this->createCommentUnder($question, "Aaaaa OK, teraz to ma sens.", "admin");
        $this->createCommentUnder($question, "Baaa... Gadacie...", "user");
        TagModel::setForPytanie($question, array("z-podrzędnymi", "pierwszy-podrzędny", "normalny-tag", "ignorowany-tag", "ulubiony-tag"));
        $this->rateStatementDown($question, "user");
        $this->rateStatementUp($question, "admin");
        $this->rateStatementUp($question, "moder");

        $response_first = $this->createResponse($question, "A no to ja powiem tak...", "user");
        $this->createCommentUnder($response_first, "Dajesz :)", "ada");
        $this->createVersionOfResponse($response_first, "No mniejwiecej tak: czyyyyyyy...", "user", "Usilna pierwsza wersja, no ale nie poszło.");
        $this->createCommentUnder($response_first, "No coś Ci nie idzie...", "admin");
        $this->createVersionOfResponse($response_first, "Było by to 40?", "user", "Pierwsza połowiczna myśl ;)");
        $this->createCommentUnder($response_first, "Byłeś blisko :)", "moder");
        $this->createCommentUnder($response_first, "Miało byc o dwa wiecej... ale coś się mi klawiatura zepsuła... Poprawi kotś?", "user");
        $this->createVersionOfResponse($response_first, "Odpowiedzią jest: 42.", "moder", "oprawiona wypowiedź");
        $this->rateStatementDown($response_first, "moder");
        $this->rateStatementDown($response_first, "admin");
        $this->rateStatementDown($response_first, "user");

        $response_to_remove = $this->createResponse($question, "W sumie sama moge sobie odpowiedzieć.", "ada");
        $this->createVersionOfResponse($response_to_remove, "No ale się rozmyśliłam...", "ada", "W sumie tak tylko");
        $this->createCommentUnder($response_to_remove, "Nikt nie protestuje?", "ada");
        $this->removeStatement($response_to_remove);

        $response_chocho = $this->createResponse($question, "Dla mnie najważniejsza jest czekolada.", "moder");
        $this->createCommentUnder($response_chocho, "Brzmi słodko ;)", "ada");
        $this->rateStatementUp($response_chocho, "admin");

        $response_accepted = $this->createResponse($question, "Niektórzy powiadają ze najważniejsze jest by Twoje potomstwo miało lepiej niż Ty.", "admin");
        $this->createVersionOfResponse($response_accepted, "Niektórzy powiadają, że najważniejsze jest by Twoje potomstwo miało lepiej niż Ty.", "admin", "Drobna poprawka");
        $this->rateStatementUp($response_accepted, "ada");
        OdpowiedzModel::accept($response_accepted);
        $this->createCommentUnder($response_accepted, "Zgadzam się.", "ada");
    }

    private function createQuestion($title, $message, $user) {
        $questionID = PytanieModel::create(
                        $this->users[$user], $title, $message);

        $fake_time = $this->getFakeTime();
        $this->setFakeTimeForStatement($questionID, $fake_time);
        $this->setFakeTimeForStatementTextLastVersion($questionID, $fake_time);

        return $questionID;
    }

    private function createVersionOfQuestion($questionID, $title, $message, $user, $version_comment) {
        PytanieModel::makeNewVersion(
                $questionID, $this->users[$user], $title, $message, $version_comment);

        $this->setFakeTimeForStatementTextLastVersion($questionID);
    }

    private function createResponse($questionID, $message, $user) {
        $responseID = OdpowiedzModel::create(
                        $this->users[$user], $questionID, $message);

        $fake_time = $this->getFakeTime();
        $this->setFakeTimeForStatement($responseID, $fake_time);
        $this->setFakeTimeForStatementTextLastVersion($responseID, $fake_time);

        return $responseID;
    }

    private function createVersionOfResponse($responseID, $message, $user, $version_comment) {
        OdpowiedzModel::makeNewVersion(
                $responseID, $this->users[$user], $message, $version_comment);

        $this->setFakeTimeForStatementTextLastVersion($responseID);
    }

    private function createCommentUnder($statementID, $comment, $user) {
        $commentID = KomentarzModel::create(
                        $this->users[$user], $statementID, $comment);

        $this->setFakeTimeForComment($commentID);

        return $commentID;
    }

    private function rateStatementUp($statementID, $user) {
        OdpowiedzModel::rateUp($statementID, $this->users[$user]);
    }

    private function rateStatementDown($statementID, $user) {
        OdpowiedzModel::rateDown($statementID, $this->users[$user]);
    }

    private function removeStatement($statementID) {
        $sql_st = $this->db_connection->prepare(
                "CALL Wypowiedz_Delete(:wypowiedzID)");

        $sql_st->bindParam(":wypowiedzID", $statementID, PDO::PARAM_INT);
        $sql_st->execute();
    }

    private function setFakeTimeForStatement($statementID, $fake_time = null) {
        if (!isset($fake_time))
            $fake_time = $this->getFakeTime();

        $sql_st = $this->db_connection->prepare(
                "UPDATE `Wypowiedz`
                    SET `DataUtworzenia` = FROM_UNIXTIME(:fakeTime)
                    WHERE `Wypowiedz_ID` = :wypowiedzID");

        $sql_st->bindParam(":fakeTime", $fake_time, PDO::PARAM_INT);
        $sql_st->bindParam(":wypowiedzID", $statementID, PDO::PARAM_INT);
        $sql_st->execute();
    }

    private function setFakeTimeForStatementTextLastVersion($statementID, $fake_time = null) {
        $sql_st = $this->db_connection->prepare(
                "SELECT `WypowiedzTresc_ID`
                    FROM `WypowiedzTresc`
                    WHERE `Wypowiedz_ID` = :wypowiedzID
                    ORDER BY `WypowiedzTresc_ID` DESC
                    LIMIT 1");

        $sql_st->bindParam(":wypowiedzID", $statementID, PDO::PARAM_INT);
        $sql_st->execute();

        $select_result = $sql_st->fetch(PDO::FETCH_NUM);
        $text_last_versionID = intval($select_result[0]);

        if (!isset($fake_time))
            $fake_time = $this->getFakeTime();

        $sql_st = $this->db_connection->prepare(
                "UPDATE `WypowiedzTresc`
                    SET `DataUtworzenia` = FROM_UNIXTIME(:fakeTime)
                    WHERE `WypowiedzTresc_ID` = :wypowiedzTrescID");

        $sql_st->bindParam(":fakeTime", $fake_time, PDO::PARAM_INT);
        $sql_st->bindParam(":wypowiedzTrescID", $text_last_versionID, PDO::PARAM_INT);
        $sql_st->execute();
    }

    private function setFakeTimeForComment($commentID) {
        $fake_time = $this->getFakeTime();

        $sql_st = $this->db_connection->prepare(
                "UPDATE `WypowiedzKomentarz`
                    SET `DataUtworzenia` = FROM_UNIXTIME(:fakeTime)
                    WHERE `WypowiedzKomentarz_ID` = :komentarzID");

        $sql_st->bindParam(":fakeTime", $fake_time, PDO::PARAM_INT);
        $sql_st->bindParam(":komentarzID", $commentID, PDO::PARAM_INT);
        $sql_st->execute();
    }

}

?>
