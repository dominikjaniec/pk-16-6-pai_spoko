DELIMITER $$


DROP PROCEDURE IF EXISTS Search_DropIndex $$
CREATE PROCEDURE Search_DropIndex (
    IN wypowiedzID int(11)
)
BEGIN
    DELETE FROM `SearchMatch_Wypowiedz`
        WHERE `Wypowiedz_ID` = wypowiedzID;
END $$


DROP PROCEDURE IF EXISTS _Search_GetSearchWordID $$
CREATE PROCEDURE _Search_GetSearchWordID (
    IN searchWord nvarchar(250),
    OUT searchWordID int(11)
)
BEGIN
    IF NOT EXISTS (
        SELECT sw.`SearchWord_ID`
            FROM `SearchWord` AS sw
            WHERE sw.`Word` = searchWord
    ) THEN
        INSERT INTO `SearchWord`
            (`Word`)
            VALUES (searchWord);
    END IF;

    SELECT sw.`SearchWord_ID` INTO searchWordID
        FROM `SearchWord` AS sw
        WHERE sw.`Word` = searchWord;
END $$

DROP PROCEDURE IF EXISTS Search_SetIndex $$
CREATE PROCEDURE Search_SetIndex (
    IN wypowiedzID int(11),
    IN searchWord nvarchar(250),
    IN isTitle tinyint(1),
    IN count int(11)
)
BEGIN
    DECLARE _searchWordID int(11);
    DECLARE _pytanieWypowiedzID int(11);

    CALL _Search_GetSearchWordID(searchWord, _searchWordID);

    SELECT w.`PytanieWypowiedz_ID` INTO _pytanieWypowiedzID
        FROM `WypowiedzPartitionData` as w
        WHERE w.`Wypowiedz_ID` = wypowiedzID;

    INSERT INTO `SearchMatch_Wypowiedz`
        (`SearchWord_ID`, `Wypowiedz_ID`, `PytanieWypowiedz_ID`, `IsTitle`, `Count`)
        VALUES (_searchWordID, wypowiedzID, _pytanieWypowiedzID, isTitle, count);
END $$


DROP PROCEDURE IF EXISTS Search_ByWords $$
CREATE PROCEDURE Search_ByWords (
    IN queryID nvarchar(25),
    IN titleBonus int(11)
)
BEGIN
    DECLARE _goingQueryTime timestamp
        DEFAULT NOW() - INTERVAL 15 MINUTE;

    DELETE FROM `SearchQueryWord`
        WHERE _goingQueryTime > `QueryTime`;

    SELECT
        pytanie.`Wypowiedz_ID`,
        pytanie.`DataUtworzenia`,
        pytanie.`Tytul`,
        pytanie.`Oceny_Ilosc`,
        pytanie.`Oceny_SumaWartosc`,
        pytanie.`AutorUzytkownik_ID`,
        search_match.`TotalScore`
    FROM `Pytania_ListView` AS pytanie
        NATURAL JOIN (
            SELECT
                r.`Wypowiedz_ID`,
                sum( r.`PartialScore` ) AS `TotalScore`
            FROM (
                SELECT
                    IFNULL( m.`PytanieWypowiedz_ID`, m.`Wypowiedz_ID` ) AS `Wypowiedz_ID`,
                    sum(( m.`IsTitle` * titleBonus ) + m.`Count` ) AS `PartialScore`
                FROM `SearchQueryWord` AS filter
                    NATURAL JOIN `SearchWord` AS sw
                    NATURAL JOIN `SearchMatch_Wypowiedz` AS m
                WHERE filter.`Identity` = queryID
                GROUP BY m.`Wypowiedz_ID`
            ) AS r
            GROUP BY r.`Wypowiedz_ID`
        ) AS search_match
    ORDER BY search_match.`TotalScore` DESC;
END $$


-- ------------------------------------------------------------------- --
-- ------------------------------------------------------------------- --
-- Core levenshtein function adapted from function by Jason Rust (http://sushiduy.plesk3.freepgs.com/levenshtein.sql)
-- Originally from http://codejanitor.com/wp/2007/02/10/levenshtein-distance-as-a-mysql-stored-function/
-- Rewritten by Arjen Lentz for utf8, code/logic cleanup and removing HEX()/UNHEX() in favour of ORD()/CHAR()
-- Levenshtein reference: http://en.wikipedia.org/wiki/Levenshtein_distance

-- Arjen note: because the levenshtein value is encoded in a byte array, distance cannot exceed 255;
-- thus the maximum string length this implementation can handle is also limited to 255 characters.

-- Code was gathered from: https://openquery.com.au/blog/levenshtein-mysql-stored-function
-- ------------------------------------------------------------------- --

DROP FUNCTION IF EXISTS Search_Levenshtein $$
CREATE FUNCTION Search_Levenshtein (
    s1 VARCHAR(255) CHARACTER SET utf8,
    s2 VARCHAR(255) CHARACTER SET utf8
)
RETURNS INT
DETERMINISTIC
BEGIN
    DECLARE s1_len, s2_len, i, j, c, c_temp, cost INT;
    DECLARE s1_char CHAR CHARACTER SET utf8;
    -- max strlen=255 for this function
    DECLARE cv0, cv1 VARBINARY(256);

    SET s1_len = CHAR_LENGTH(s1),
        s2_len = CHAR_LENGTH(s2),
        cv1 = 0x00,
        j = 1,
        i = 1,
        c = 0;

    IF (s1 = s2) THEN
        RETURN (0);
    ELSEIF (s1_len = 0) THEN
        RETURN (s2_len);
    ELSEIF (s2_len = 0) THEN
        RETURN (s1_len);
    END IF;

    WHILE (j <= s2_len) DO
        SET cv1 = CONCAT(cv1, CHAR(j)),
            j = j + 1;
    END WHILE;

    WHILE (i <= s1_len) DO
        SET s1_char = SUBSTRING(s1, i, 1),
            c = i,
            cv0 = CHAR(i),
            j = 1;

        WHILE (j <= s2_len) DO
            SET c = c + 1,
                cost = IF(s1_char = SUBSTRING(s2, j, 1), 0, 1);

            SET c_temp = ORD(SUBSTRING(cv1, j, 1)) + cost;
            IF (c > c_temp) THEN
                SET c = c_temp;
            END IF;

            SET c_temp = ORD(SUBSTRING(cv1, j+1, 1)) + 1;
                IF (c > c_temp) THEN
            SET c = c_temp;
            END IF;

            SET cv0 = CONCAT(cv0, CHAR(c)),
                j = j + 1;
        END WHILE;

        SET cv1 = cv0,
        i = i + 1;
    END WHILE;

    RETURN (c);
END $$


DELIMITER ;
