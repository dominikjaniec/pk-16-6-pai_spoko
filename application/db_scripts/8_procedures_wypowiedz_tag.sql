DELIMITER $$


DROP PROCEDURE IF EXISTS Wypowiedz_Tag_Set $$
CREATE PROCEDURE Wypowiedz_Tag_Set (
    IN tagID int(11),
    IN wypowiedzID int(11)
)
BEGIN
    START TRANSACTION;

    INSERT INTO `Wypowiedz_Tag`
        (`Wypowiedz_ID`, `Tag_ID`)
        VALUES (wypowiedzID, tagID);

    CALL _TagStatystyki_Update_Wypowiedz(tagID);

    COMMIT;
END $$


DROP PROCEDURE IF EXISTS Wypowiedz_Tag_Unset $$
CREATE PROCEDURE Wypowiedz_Tag_Unset (
    IN tagID int(11),
    IN wypowiedzID int(11)
)
BEGIN
    START TRANSACTION;

    DELETE FROM `Wypowiedz_Tag`
        WHERE `Tag_ID` = tagID
            AND `Wypowiedz_ID` = wypowiedzID;

    CALL _TagStatystyki_Update_Wypowiedz(tagID);

    COMMIT;
END $$


DELIMITER ;
