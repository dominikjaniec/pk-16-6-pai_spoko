<?php

require_once '../common/DataBase.php';
require_once '../common/Helpers.php';
require_once '../common/Roles.php';
require_once '../model/UzytkownikModel.php';
require_once 'Sampler.php';

class Installer {

    const ADMINISTRATOR_LOGIN = "__spoko__admin__";
    const ADMINISTRATOR_PASSWORD = "qwe123!@#QWE";

    private $wipeout_schema;
    private $seed_samples;
    private $db_connection;
    private $last_action;
    private $last_error;
    private $installacion_time;

    public function __construct($wipeout_schema, $seed_samples) {
        $this->wipeout_schema = $wipeout_schema;
        $this->seed_samples = $seed_samples;
    }

    public function installSpoko() {
        $start_time = microtime(true);
        $installed = false;

        try {
            $this->prepareDataBase();
            $this->runCreationScripts();
            $this->prepareUserRoles();
            $this->createAdministrator();

            if ($this->seed_samples)
                $this->seedWithSamples();

            $installed = true;
        } catch (Exception $ex) {
            $this->spoko_installed = false;
            $this->last_error = $ex;
        }

        $end_time = microtime(true);
        $this->installacion_time = $end_time - $start_time;

        return $installed;
    }

    public function getError() {
        return array(
            "last_action" => $this->last_action,
            "last_error" => $this->last_error,
        );
    }

    public function getInstalationTime() {
        return $this->installacion_time;
    }

    public function wipeoutSchema() {
        return $this->wipeout_schema;
    }

    public function seedSamples() {
        return $this->seed_samples;
    }

    private function prepareDataBase() {
        $this->last_action = "Connecting...";
        $this->db_connection = DataBase::GetConnectionWithoutSchema();
        $this->db_connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        if ($this->wipeout_schema) {
            $this->last_action = "Schema wipeout";
            $this->db_connection->exec(
                    "DROP SCHEMA IF EXISTS `" . Config::dbSchema() . "`");
            $this->db_connection->exec(
                    "CREATE SCHEMA `" . Config::dbSchema() . "` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci");
        }

        $this->last_action = "Choose schema";
        $this->db_connection->exec(
                "USE `" . Config::dbSchema() . "`");
    }

    private function runCreationScripts() {
        $this->last_action = "Loading scripts...";

        $scripts_files = glob(dirname(__FILE__) . "/*.sql");
        $scripts_files = array_diff($scripts_files, array('..', '.'));
        sort($scripts_files);

        foreach ($scripts_files as $file_path) {
            $this->last_action = "Loading file: '" . $file_path . "'...";
            $file_content = file_get_contents($file_path);

            foreach ($this->exctractScriptsFrom($file_content) as $script) {
                $this->last_action = "Executing script: " . substr($script, 0, 50) . "...";
                $this->db_connection->exec($script);
            }
        }
    }

    private function exctractScriptsFrom($file_content) {
        $changing_delimiter = stripos($file_content, "DELIMITER");
        if ($changing_delimiter === false) {
            return array($file_content);
        }

        // DELIMITER lookup:
        $delimiter_start_index = $changing_delimiter + strlen("DELIMITER");
        while (ctype_space($file_content[$delimiter_start_index]) == true) {
            $delimiter_start_index++;
        }

        $delimiter_end_index = $delimiter_start_index;
        while (ctype_space($file_content[$delimiter_end_index]) == false) {
            $delimiter_end_index++;
        }

        $delimiter_lenght = $delimiter_end_index - $delimiter_start_index;
        $delimiter = substr($file_content, $delimiter_start_index, $delimiter_lenght);

        // Scripts splited by DELIMITER:
        $result_scripts = array();
        $script_end_index = $delimiter_start_index;

        do {
            $script_start_index = $delimiter_lenght + $script_end_index;
            $script_end_index = stripos($file_content, $delimiter, $script_start_index);
            if ($script_end_index === false) {
                $script_end_index = stripos($file_content, "DELIMITER", $script_start_index);
                if ($script_end_index === false)
                    break;
            }

            $script_lenght = $script_end_index - $script_start_index;
            $script = substr($file_content, $script_start_index, $script_lenght);
            $script = trim($script);

            if ($script != "")
                array_push($result_scripts, $script);
        } while (true);

        return $result_scripts;
    }

    private function prepareUserRoles() {
        $this->last_action = "Preparation of User Roles...";

        $roles = array(
            Roles::ADMIN => Roles::getName(Roles::ADMIN),
            Roles::MOD => Roles::getName(Roles::MOD),
            Roles::USER => Roles::getName(Roles::USER),
        );

        $statement = $this->db_connection->prepare(
                "INSERT INTO `Rola` (`Nazwa`, `Klucz`) VALUES (:roleName, :roleKey)");

        $statement->bindParam(':roleKey', $role_key);
        $statement->bindParam(':roleName', $role_name);

        foreach ($roles as $role_key => $role_name) {
            $statement->execute();
        }
    }

    private function createAdministrator() {
        UzytkownikModel::create(
            self::ADMINISTRATOR_LOGIN,
            self::ADMINISTRATOR_PASSWORD,
            self::ADMINISTRATOR_LOGIN . "@example.com",
            Roles::ADMIN);
    }

    private function seedWithSamples() {
        $this->last_action = "Sample seeding...";
        $sampler = new Sampler();
        $sampler->seed();
    }

}

?>
