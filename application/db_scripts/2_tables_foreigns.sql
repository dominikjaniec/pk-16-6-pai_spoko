ALTER TABLE `Tag`
  ADD CONSTRAINT `Tag__NadrzednyTag`
    FOREIGN KEY (`NadrzednyTag_ID`)
    REFERENCES `Tag` (`Tag_ID`);

ALTER TABLE `TagStatystyki`
  ADD CONSTRAINT `Statystyki__Tag`
    FOREIGN KEY (`Tag_ID`)
    REFERENCES `Tag` (`Tag_ID`)
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Uzytkownik`
  ADD CONSTRAINT `Uzytkownik__Rola`
    FOREIGN KEY (`Rola_ID`)
    REFERENCES `Rola` (`Rola_ID`);

ALTER TABLE `UzytkownikLogowania`
  ADD CONSTRAINT `Logowania__Uzytkownik`
    FOREIGN KEY (`Uzytkownik_ID`)
    REFERENCES `Uzytkownik` (`Uzytkownik_ID`);

ALTER TABLE `UzytkownikSesja`
  ADD CONSTRAINT `Sesja__Uzytkownik`
    FOREIGN KEY (`Uzytkownik_ID`)
    REFERENCES `Uzytkownik` (`Uzytkownik_ID`);

ALTER TABLE `UzytkownikStatystyki`
  ADD CONSTRAINT `Statystyki__Uzytkownik`
    FOREIGN KEY (`Uzytkownik_ID`)
    REFERENCES `Uzytkownik` (`Uzytkownik_ID`);

ALTER TABLE `Uzytkownik_Tag`
  ADD CONSTRAINT `_Tag__Uzytkownik_`
    FOREIGN KEY (`Tag_ID`)
    REFERENCES `Tag` (`Tag_ID`)
    ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `_Uzytkownik__Tag_`
    FOREIGN KEY (`Uzytkownik_ID`)
    REFERENCES `Uzytkownik` (`Uzytkownik_ID`);

ALTER TABLE `Wypowiedz`
  ADD CONSTRAINT `Wypowiedz__Pytanie`
    FOREIGN KEY (`PytanieWypowiedz_ID`)
    REFERENCES `Wypowiedz` (`Wypowiedz_ID`),
  ADD CONSTRAINT `Wypowiedz__Uzytkownik`
    FOREIGN KEY (`AutorUzytkownik_ID`)
    REFERENCES `Uzytkownik` (`Uzytkownik_ID`);

ALTER TABLE `WypowiedzPartitionData`
  ADD CONSTRAINT `Wypowiedz__Pytanie`
    FOREIGN KEY (`PytanieWypowiedz_ID`)
    REFERENCES `Wypowiedz` (`Wypowiedz_ID`);

ALTER TABLE `WypowiedzKomentarz`
  ADD CONSTRAINT `Komentarz__Wypowiedz`
    FOREIGN KEY (`Wypowiedz_ID`)
    REFERENCES `Wypowiedz` (`Wypowiedz_ID`),
  ADD CONSTRAINT `Komentarz__Uzytkownik`
    FOREIGN KEY (`AutorUzytkownik_ID`)
    REFERENCES `Uzytkownik` (`Uzytkownik_ID`);

ALTER TABLE `WypowiedzOcena`
  ADD CONSTRAINT `Ocena__Wypowiedz`
    FOREIGN KEY (`Wypowiedz_ID`)
    REFERENCES `Wypowiedz` (`Wypowiedz_ID`),
  ADD CONSTRAINT `Ocena__Uzytkownik`
    FOREIGN KEY (`Uzytkownik_ID`)
    REFERENCES `Uzytkownik` (`Uzytkownik_ID`);

ALTER TABLE `WypowiedzOdslona`
  ADD CONSTRAINT `Odslona__Wypowiedz`
    FOREIGN KEY (`Wypowiedz_ID`)
    REFERENCES `Wypowiedz` (`Wypowiedz_ID`),
  ADD CONSTRAINT `Odslona__Uzytkownik`
    FOREIGN KEY (`Uzytkownik_ID`)
    REFERENCES `Uzytkownik` (`Uzytkownik_ID`);

ALTER TABLE `WypowiedzStatystyki`
  ADD CONSTRAINT `StatystykiWypowiedz__Wypowiedz`
    FOREIGN KEY (`Wypowiedz_ID`)
    REFERENCES `Wypowiedz` (`Wypowiedz_ID`),
  ADD CONSTRAINT `StatystykiWypowiedz__Tresc`
    FOREIGN KEY (`OstatniaWersja_WypowiedzTresc_ID`)
    REFERENCES `WypowiedzTresc` (`WypowiedzTresc_ID`),
  ADD CONSTRAINT `StatystykiWypowiedz__Uzytkownik`
    FOREIGN KEY (`OstatniaWersja_AutorUzytkownik_ID`)
    REFERENCES `Uzytkownik` (`Uzytkownik_ID`);

ALTER TABLE `WypowiedzStatystykiPytanie`
  ADD CONSTRAINT `StatystykiPytanie__Wypowiedz`
    FOREIGN KEY (`Wypowiedz_ID`)
    REFERENCES `Wypowiedz` (`Wypowiedz_ID`);

ALTER TABLE `WypowiedzTresc`
  ADD CONSTRAINT `Tresc__Wypowiedz`
    FOREIGN KEY (`Wypowiedz_ID`)
    REFERENCES `Wypowiedz` (`Wypowiedz_ID`),
  ADD CONSTRAINT `Tresc__Uzytkownik`
    FOREIGN KEY (`AutorUzytkownik_ID`)
    REFERENCES `Uzytkownik` (`Uzytkownik_ID`);

ALTER TABLE `Wypowiedz_Tag`
  ADD CONSTRAINT `_Wypowiedz__Tag_`
    FOREIGN KEY (`Tag_ID`)
    REFERENCES `Tag` (`Tag_ID`)
    ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `_Tag__Wypowiedz_`
    FOREIGN KEY (`Wypowiedz_ID`)
    REFERENCES `Wypowiedz` (`Wypowiedz_ID`);

ALTER TABLE `SearchMatch_Wypowiedz`
  ADD CONSTRAINT `_SearchWord__Wypowiedz_`
    FOREIGN KEY (`SearchWord_ID`)
    REFERENCES `SearchWord` (`SearchWord_ID`),
  ADD CONSTRAINT `_Wypowiedz__SearchWord_`
    FOREIGN KEY (`Wypowiedz_ID`)
    REFERENCES `Wypowiedz` (`Wypowiedz_ID`)
    ON DELETE CASCADE ON UPDATE CASCADE;
