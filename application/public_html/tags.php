<?php

require_once '../model/TagModel.php';

$term = null;
if (isset($_GET["term"]))
    $term = $_GET["term"];

$tagsToSkip = null;
if (isset($_GET["selected"]))
    $tagsToSkip = explode(TagModel::LIST_SEPARATOR, $_GET["selected"]);

$useUserPreferences = false;
if(isset($_GET["useUserPreferences"]))
    $useUserPreferences = $_GET["useUserPreferences"] == "true";

$matches = TagModel::findMatches($term, $tagsToSkip, $useUserPreferences);
echo json_encode($matches);
?>
