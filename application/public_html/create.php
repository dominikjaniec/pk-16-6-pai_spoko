<?php

require_once '../common/Helpers.php';
require_once '../common/CurrentUser.php';
require_once '../common/Templater.php';
require_once '../viewmodel/WypowiedzCreateVM.php';

if (!CurrentUser::isLogged()) {
    show404();
}

if (isset($_GET[WypowiedzCreateVM::CREATE_ACTION])) {
    $resultUrl = WypowiedzCreateVM::handleCreatePost();
    redirect($resultUrl);
}

function template_render() {
    WypowiedzCreateVM::renderPytanie();
}

Templater::apply(__FILE__);
?>
