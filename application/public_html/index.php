<?php

require_once '../common/Templater.php';
require_once '../viewmodel/PytanieListVM.php';

$searchTerms = null;
if (isset($_GET[PytanieListVM::FIELD_SEARCH]))
    $searchTerms = $_GET[PytanieListVM::FIELD_SEARCH];

function template_render() {
    global $searchTerms;
    PytanieListVM::render($searchTerms);
}

Templater::apply(__FILE__);
?>
