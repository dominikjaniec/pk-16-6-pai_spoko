(function ($) {
    "use strict";

    $(function () {
        var $header = $(".spoko-logo");
        var $headerContent = $header
            .find(".logo-content");
        var $headerHoverContent = $header
            .find(".logo-hover-content");

        $header.hover(function () {
            $headerContent.hide();
            $headerHoverContent.show();
        }, function () {
            $headerContent.show();
            $headerHoverContent.hide();
        });

        $(".popover-user-marker")
            .popover({
                html: true,
                trigger: "hover",
                title: function () {
                    return $(this)
                        .find(".popover-user-title")
                        .html();
                },
                content: function () {
                    return $(this)
                        .find(".popover-user-content")
                        .html();
                },
            });

        $(".popover-tag-marker")
            .popover({
                html: true,
                trigger: "hover",
                title: function () {
                    return $(this)
                        .find(".popover-tag-title")
                        .html();
                },
                content: function () {
                    return $(this)
                        .find(".popover-tag-content")
                        .html();
                },
            });

        $.extend( $.validator.messages, {
	        required: "To pole jest wymagane.",
	        remote: "Proszę o wypełnienie tego pola.",
	        email: "Proszę o podanie prawidłowego adresu email.",
	        url: "Proszę o podanie prawidłowego URL.",
	        date: "Proszę o podanie prawidłowej daty.",
	        dateISO: "Proszę o podanie prawidłowej daty (ISO).",
	        number: "Proszę o podanie prawidłowej liczby.",
	        digits: "Proszę o podanie samych cyfr.",
	        creditcard: "Proszę o podanie prawidłowej karty kredytowej.",
	        equalTo: "Proszę o podanie tej samej wartości ponownie.",
	        extension: "Proszę o podanie wartości z prawidłowym rozszerzeniem.",
	        maxlength: $.validator.format( "Proszę o podanie nie więcej niż {0} znaków." ),
	        minlength: $.validator.format( "Proszę o podanie przynajmniej {0} znaków." ),
	        rangelength: $.validator.format( "Proszę o podanie wartości o długości od {0} do {1} znaków." ),
	        range: $.validator.format( "Proszę o podanie wartości z przedziału od {0} do {1}." ),
	        max: $.validator.format( "Proszę o podanie wartości mniejszej bądź równej {0}." ),
	        min: $.validator.format( "Proszę o podanie wartości większej bądź równej {0}." ),
	        pattern: $.validator.format( "Pole zawiera niedozwolone znaki." )
        });

        $('form').each(function () {
            $(this).validate({
                onkeyup: false,
                highlight: function (elementDOM) {
                    var element = $(elementDOM);
                    var formGroup = element.closest('.form-group');
                    formGroup.addClass('has-error');

                    if(formGroup.hasClass('has-feedback')) {
                        formGroup.removeClass('has-success');
                        formGroup.find('.glyphicon-ok').hide();
                        formGroup.find('.glyphicon-remove').show();
                    }
                },
                unhighlight: function (elementDOM) {
                    var element = $(elementDOM);
                    var formGroup = element.closest('.form-group');
                    formGroup.removeClass('has-error');

                    if(formGroup.hasClass('has-feedback')) {
                        formGroup.addClass('has-success');
                        formGroup.find('.glyphicon-ok').show();
                        formGroup.find('.glyphicon-remove').hide();
                    }
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });

        if(window.spoko_register_form !== undefined) {
            var registerFormData = window.spoko_register_form;

            $(registerFormData.login_id)
                .rules( "add", {
                    remote: registerFormData.login_check_url,
                    messages: { remote: "Ten login jest już zajęty przez innego użytkownika." }
                });

            $(registerFormData.password_reentred_id)
                .rules( "add", {
                    equalTo: registerFormData.password_id
                });
        }

        $('.form-group.has-feedback')
            .each(function () {
                var withFeedback = $(this);

                withFeedback.append($('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>'));
                withFeedback.append($('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>'));

                withFeedback
                    .find(".glyphicon.form-control-feedback")
                    .hide();
            });

        $('.spoko-tags-edit')
            .each(function () {
                var tagsBlock = $(this);
                var inputName = tagsBlock.attr('data-fieldname');
                var tagSplitter = tagsBlock.attr('data-splitter');
                var placeholder = tagsBlock.attr('data-placeholder');
                var usePreferencesValue = tagsBlock.attr('data-use-user-preferences');

                var tags = tagsBlock.find('ul');
                tags.addClass('form-control');
                tags.tagit({
                    fieldName: inputName,
                    autocomplete: {
                        source: function (request, response) {
                            var selectedTags = tagsBlock
                                .find("[name='" + inputName + "']");

                            var tagsRequestData = {
                                useUserPreferences: usePreferencesValue === 'true',
                                selected: selectedTags.val(),
                                term: request.term
                            };

                            $.get("tags.php", tagsRequestData)
                                .done(function (result) {
                                    response(JSON.parse(result));
                                });
                        },
                        search: function (event) { tagsBlock.addClass('spoko-working-32'); },
                        response: function (event) { tagsBlock.removeClass('spoko-working-32'); },
                        minLength: 2,
                        delay: 250
                    },
                    removeConfirmation: true,
                    caseSensitive: false,
                    singleField: true,
                    singleFieldDelimiter: tagSplitter,
                    placeholderText: placeholder,
                    afterTagAdded: function (event) { tagsBlock.removeClass('spoko-working-32'); },
                });
            });
    });
})(jQuery);

