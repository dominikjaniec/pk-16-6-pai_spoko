<?php

require_once '../common/Helpers.php';
require_once '../common/Navigations.php';
require_once '../common/Templater.php';
require_once '../viewmodel/ProfilVM.php';

$signInResult = ProfilVM::loginPostDefaultResult();

if (isset($_GET[ProfilVM::SIGNIN_ACTION])) {
    $signInResult = ProfilVM::handleLoginPost();

    if ($signInResult[ProfilVM::RESULT_STATUS]) {
        redirect(Navigations::LISTA_PYTAN);
    }
}

function template_render() {
    global $signInResult;

    ProfilVM::renderSignIn($signInResult);
}

Templater::apply(__FILE__);
?>
