<?php

require_once '../common/Helpers.php';
require_once '../common/CurrentUser.php';
require_once '../common/Templater.php';
require_once '../model/UzytkownikModel.php';
require_once '../viewmodel/UrlCreator.php';
require_once '../viewmodel/ProfilVM.php';


if (!isset($_GET[ProfilVM::SHOW_ACTION])) {
    if (CurrentUser::isLogged()) {
        redirect(UrlCreator::forUzytkownik(CurrentUser::id()));
    } else {
        show404();
    }
}

$userID = intval(resolveGetValue(ProfilVM::SHOW_ACTION));
if (!UzytkownikModel::isValidUserID($userID)) {
    show404();
}

if (isset($_POST[UzytkownikVM::TAGS_USAGE_ACTION])) {
    $resultUrl = UzytkownikVM::handleTagsUsagePost();
    redirect($resultUrl);
}

function template_render() {
    global $userID;
    ProfilVM::renderProfile($userID);
}

Templater::apply(__FILE__);
?>
