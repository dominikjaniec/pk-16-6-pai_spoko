<?php

require_once '../common/Navigations.php';
require_once '../common/Templater.php';
require_once '../viewmodel/PytanieVM.php';

if (!isset($_GET[PytanieVM::SHOW_ACTION])) {
    show404();
}

$pytanieID = intval($_GET[PytanieVM::SHOW_ACTION]);

try {
    $pytanieTytul = PytanieVM::getTytul($pytanieID);
} catch (Exception $ex) {
    show404();
}

function template_render() {
    global $pytanieID;
    PytanieVM::render($pytanieID);
}

Templater::applyWithTitle(__FILE__, "Pytanie: " . $pytanieTytul);
?>
