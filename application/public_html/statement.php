<?php

require_once '../common/Helpers.php';
require_once '../common/CurrentUser.php';
require_once '../common/Navigations.php';
require_once '../viewmodel/PytanieVM.php';
require_once '../viewmodel/UrlCreator.php';

if (!CurrentUser::isLogged()) {
    show404();
}

if (isset($_GET[PytanieVM::SHOW_ACCEPT_ACTION])) {
    $sourceOdpowiedzId = PytanieVM::handleAcceptPost();
    redirect(UrlCreator::forWypowiedz($sourceOdpowiedzId));
}

if (isset($_GET[PytanieVM::SHOW_RATE_ACTION])) {
    $sourceWypowiedzId = PytanieVM::handleRatePost();
    redirect(UrlCreator::forWypowiedz($sourceWypowiedzId));
}

show404();
?>
