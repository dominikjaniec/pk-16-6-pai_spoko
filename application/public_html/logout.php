<?php

require_once '../common/Helpers.php';
require_once '../common/Navigations.php';
require_once '../model/UserSessionModel.php';

UserSessionModel::remove();

redirect(Navigations::LISTA_PYTAN);
?>
