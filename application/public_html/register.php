<?php

require_once '../common/Helpers.php';
require_once '../common/Navigations.php';
require_once '../common/Templater.php';
require_once '../viewmodel/ProfilVM.php';

$registerResult = ProfilVM::registerPostDefaultResult();

if (isset($_GET[ProfilVM::REGISTER_ACTION])) {
    $registerResult = ProfilVM::handleRegisterPost();

    if ($registerResult[ProfilVM::RESULT_STATUS]) {
        redirect(Navigations::LISTA_PYTAN);
    }
}

function template_render() {
    global $registerResult;

    ProfilVM::renderRegister($registerResult);
}

Templater::apply(__FILE__);
?>
