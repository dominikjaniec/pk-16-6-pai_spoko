<?php

require_once '../common/Helpers.php';
require_once '../model/UzytkownikModel.php';
require_once '../viewmodel/ProfilVM.php';

$login = resolveGetValue(ProfilVM::FIELD_LOGIN);

if (uzytkownikModel::isLoginAvailable($login)) {
    echo "true";
} else {
    echo "false";
}
?>
