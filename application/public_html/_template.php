<?php
require_once '../common/CurrentUser.php';
require_once '../common/DataBase.php';
require_once '../common/DebugSpoko.php';
require_once '../common/Navigations.php';
require_once '../viewmodel/UzytkownikVM.php';
require_once '../viewmodel/PytanieListVM.php';

if (!isset($template_from_spoko_install))
    DataBase::EnsureSchemaExists();

if (!isset($template_show_navigation))
    $template_show_navigation = true;

if (!isset($template_current_file))
    $template_current_file = "";

if (!isset($template_title))
    $template_title = "spoko";
else
    $template_title = "spoko - " . $template_title;

?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?= $template_title ?></title>
        <link href="static/css/jquery-ui.css" rel="stylesheet" />
        <link href="static/css/jquery.tagit.css" rel="stylesheet" />
        <link href="static/css/bootstrap.css" rel="stylesheet" />
        <link href="static/css/spoko.css" rel="stylesheet" />
    </head>

    <body>
        <div class="container">
            <div class="header clearfix">

                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <div class="navbar-brand spoko-logo">
                                <a href="<?= Navigations::LISTA_PYTAN ?>"
                                   class="spoko-logo-link">
                                    <span class="logo-content">
                                        <strong>S.P.O.K.O.</strong>
                                    </span>
                                    <span class="logo-hover-content"
                                          style="display: none">
                                        <strong>S</strong>ystem
                                        <strong>P</strong>ytanie
                                        <strong>O</strong>dpowiedź
                                        <strong>K</strong>omentarz
                                        <strong>O</strong>cena
                                    </span>
                                </a>
                            </div>
                        </div>

                        <?php if (!$template_from_spoko_install && $template_show_navigation) { ?>
                            <div class="collapse navbar-collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <?php
                                    foreach (Navigations::getOptions() as $navigation) {
                                        $page_file = $navigation["page_file"];
                                        $name = $navigation["navigation_name"];
                                        $link = sprintf('<a href="%s">%s</a>', $page_file, $name);

                                        $class = "";
                                        if (Navigations::isCurrent($page_file))
                                            $class = ' class="active"';

                                        echo sprintf('<li%s>%s</li>', $class, $link);
                                    }
                                    ?>
                                </ul>

                                <form class="navbar-form navbar-right" role="search"
                                      action="<?= Navigations::LISTA_PYTAN ?>" method="get">
                                    <div class="input-group">
                                        <?php
                                        $searchField = "";
                                        if (isset($_GET[PytanieListVM::FIELD_SEARCH]))
                                            $searchField = $_GET[PytanieListVM::FIELD_SEARCH];
                                        ?>
                                        <input type="text" class="form-control"
                                               name="<?= PytanieListVM::FIELD_SEARCH ?>"
                                               value="<?= htmlspecialchars($searchField) ?>"
                                               placeholder="Szukaj...">

                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit">
                                                &#8203;
                                                <span class="glyphicon glyphicon-circle-arrow-right"></span>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        <?php } ?>
                    </div>
                </nav>
            </div>

            <div class="row">
                <div class="col-lg-2">
                    <?php if (!$template_from_spoko_install && CurrentUser::isLogged()) { ?>
                        <div class="text-center">
                            Zalogowany:
                            <strong class="spoko-nowrap-space">
                                <?= CurrentUser::displayName() ?>
                            </strong>
                        </div>
                        <?php UzytkownikVM::renderGravatar(CurrentUser::id(), UzytkownikVM::GRAVATAR_KIND_SIDE);
                    } ?>
                </div>
                <div class="col-lg-8">
                    <div class="jumbotron">
                        <?php
                        if (isset($template_override_render)) {
                            call_user_func($template_override_render);
                        } else {
                            template_render();
                        }
                        ?>
                    </div>
                </div>
                <div class="col-lg-2">
                    <?php
                    if (DebugSpoko::shouldShow()) {
                        foreach (DebugSpoko::getAllStatistics() as $key => $value) {
                            ?>
                            <div>
                                <strong><?= $key ?>:</strong>
                                <br />
                                <?= $value ?>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>

        <script src="static/js/jquery.js" type="text/javascript"></script>
        <script src="static/js/jquery-ui.js" type="text/javascript"></script>
        <script src="static/js/jquery.validate.js" type="text/javascript"></script>
        <script src="static/js/tag-it.js" type="text/javascript"></script>
        <script src="static/js/bootstrap.js" type="text/javascript"></script>
        <script src="static/js/spoko.js" type="text/javascript"></script>
    </body>
</html>
