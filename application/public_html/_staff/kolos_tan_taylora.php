<?php
const INPUT_NAME = "x";
const ITER_NUM = 100;

$x = 0;
if (isset($_POST[INPUT_NAME]))
    $x = doubleval($_POST[INPUT_NAME]);

function silnia($x) {
    if ($x < 1)
        return 1;

    $wynik = $x;
    for ($i = $x - 1; $i > 0; $i--) {
        $wynik *= $i;
    }

    return $wynik;
}

function calcSin($x) {
    $wynik = 0;

    for ($n = 0; $n < ITER_NUM; $n++) {
        $znak = doubleval(pow(-1, $n));
        $x_part = doubleval(pow($x, 2 * $n + 1));
        $n_part = doubleval(silnia(2 * $n + 1));

        $wynik_part = $znak * $x_part;
        $wynik_part /= $n_part;

        $wynik += $wynik_part;
    }

    return $wynik;
}

function calcCos($x) {
    $wynik = 0;

    for ($n = 0; $n < ITER_NUM; $n++) {
        $znak = doubleval(pow(-1, $n));
        $x_part = doubleval(pow($x, 2 * $n));
        $n_part = doubleval(silnia(2 * $n));

        $wynik_part = $znak * $x_part;
        $wynik_part /= $n_part;

        $wynik += $wynik_part;
    }

    return $wynik;
}

function isDefineTan($x) {
    return abs($x) < (pi() / 2);
}

$wynik = calcSin($x) / calcCos($x);
?>

<html>
    <body>
        <h1>PHP:</h1>
        <form action="<?= basename(__FILE__) ?>" method="post">
            <label for="x">x: </label>
            <input type="text" name="x" value="<?= $x ?>" />
            |
            tan(x)
            <input type="submit" value=" = " />
            <?= $wynik ?>
        </form>

        <hr />
        <h1>JavaScript:</h1>
        <div>
            <label for="x">x: </label>
            <input type="text" id="x_JS" value="<?= $x ?>" />
            |
            tan(x)
            <input type="button" value=" = " onclick="licz()" />
            <span id="js_wynik"></span>
        </div>
        <script>
ITER_NUM = <?= ITER_NUM ?>;

function silnia($x) {
    if ($x < 1)
        return 1;

    var $wynik = $x;
    for (var $i = $x - 1; $i > 0; $i--) {
        $wynik *= $i;
    }

    return $wynik;
}

function calcSin($x) {
    var $wynik = 0;

    for (var $n = 0; $n < ITER_NUM; $n++) {
        var $znak = Math.pow(-1, $n);
        var $x_part = Math.pow($x, 2 * $n + 1);
        var $n_part = silnia(2 * $n + 1);

        var $wynik_part = $znak * $x_part;
        $wynik_part /= $n_part;

        $wynik += $wynik_part;
    }

    return $wynik;
}

function calcCos($x) {
    var $wynik = 0;

    for ($n = 0; $n < ITER_NUM; $n++) {
        var $znak = Math.pow(-1, $n);
        var $x_part = Math.pow($x, 2 * $n);
        var $n_part = silnia(2 * $n);

        var $wynik_part = $znak * $x_part;
        $wynik_part /= $n_part;

        $wynik += $wynik_part;
    }

    return $wynik;
}

function isDefineTan($x) {
    return Math.abs($x) < (Math.PI / 2);
}

function licz() {
    var x = document.getElementById("x_JS").value;
    $wynik = calcSin(x) / calcCos(x);
    document.getElementById("js_wynik").innerText = $wynik;
}

licz();
        </script>
    </body>

</html>
