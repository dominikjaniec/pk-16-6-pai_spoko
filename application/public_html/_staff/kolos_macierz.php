<?php

if(isset($_POST["set"])){
    $A_rows = intval($_POST["A_ROWS"]);
    $A_cols = intval($_POST["A_COLS"]);
    $B_rows = intval($_POST["B_ROWS"]);
    $B_cols = intval($_POST["B_COLS"]);

    $A_setting = "A=".$A_rows ."x".$A_cols;
    $B_setting = "B=".$B_rows ."x".$B_cols;
    $url = "/spoko/_staff/". basename(__FILE__) . "?" . $A_setting . "&" . $B_setting;

    header("Location: $url", true, 303);
    exit();
}

function createMatrix($wiersze, $kolumny) {
    $matrix = array();
    for($wiersz = 0; $wiersz < $wiersze; ++$wiersz){
        $m_wiersz = array();
        for($kolumna = 0; $kolumna < $kolumny; ++$kolumna)
            $m_wiersz[$kolumna] = 0;

        $matrix[$wiersz] = $m_wiersz;
    }

    return $matrix;
}

function loadMatrix($namePrefixm, $rows, $cols){
    $matrix = array();
    for($r = 0; $r < $rows; ++$r){
        $m_r = array();

        for($c = 0; $c < $cols; ++$c){
            $postId = $namePrefixm . "r" . $r;
            $data = $_POST[$postId];

            $m_r[$c] = floatval($data[$c]);
        }

        $matrix[$r] = $m_r;
    }

    return $matrix;
}

function multipe($A, $B){
    $A_rows = count($A);
    $A_cols = count($A[0]);

    $B_rows = count($B);
    $B_cols = count($B[0]);

    $m = $A_cols;
    if($A_cols != $B_rows)
        die("Tego sie nie da policzyc... A l.kolumn != B l.wierszy ($A_cols != $B_rows)");

    $R_rows = $A_rows;
    $R_cols = $B_cols;

    $R = array();
    for($r = 0; $r < $R_rows; ++$r){
        $R_row = array();

        for($c = 0; $c < $R_cols; ++$c){
            $s = 0;

            for ($i = 0; $i < $m; ++$i){
                $a_r = $A[$r];
                $a = $a_r[$i];
                $a = floatval($a);

                $b_r = $B[$i];
                $b = $b_r[$c];
                $b = floatval($b);

                $s += $a * $b;
            }

            $R_row[$c] = $s;
        }

        $R[$r] = $R_row;
    }

    return $R;
}

$A_wiersze = 2;
$A_kolumny = 4;
if(isset($_GET["A"])){
    $A_def = explode("x", $_GET["A"]);
    if(count($A_def) == 2) {
        $A_wiersze = intval($A_def[0]);
        $A_kolumny = intval($A_def[1]);
    }
}
$A = null;
if(isset($_POST["do"]))
    $A = loadMatrix("A", $A_wiersze, $A_kolumny);
else
    $A = createMatrix($A_wiersze, $A_kolumny);

$B_wiersze = 4;
$B_kolumny = 3;
if(isset($_GET["B"])){
    $B_def = explode("x", $_GET["B"]);
    if(count($B_def) == 2) {
        $B_wiersze = intval($B_def[0]);
        $B_kolumny = intval($B_def[1]);
    }
}
$B = null;
if(isset($_POST["do"]))
    $B = loadMatrix("B", $B_wiersze, $B_kolumny);
else
    $B = createMatrix($B_wiersze, $B_kolumny);

$R = multipe($A, $B);

?>

<html>
<head>
<style>
input {
	width: 50px;
}
</style>
</head>
<body>
	<form method="post">
		<input type="hidden" name="set">
		<table>
			<tr>
				<td></td>
				<td>
				Macierz B:<br />
				Liczba wierszy: <input type="text" name="B_ROWS" value="<?= $B_wiersze ?>"><br />
				Liczba kolumn: <input type="text" name="B_COLS" value="<?= $B_kolumny ?>"><br />
				</td>
			</tr>
			<tr>
				<td>
				Macierz A:<br />
				Liczba wierszy: <input type="text" name="A_ROWS" value="<?= $A_wiersze ?>"><br />
				Liczba kolumn: <input type="text" name="A_COLS" value="<?= $A_kolumny ?>"><br /></td>
				<td><strong>Macierz<br />Wynikowa</strong></td>
			</tr>
		</table>
		<input type="submit" value="Ustaw">
	</form>
	<hr />
	<form method="post">
		<table>
			<?php

			$dump_wiersze = $A_wiersze + $B_wiersze;
			$dump_kolumny = $A_kolumny + $B_kolumny;

			for($wiersz = 0; $wiersz < $dump_wiersze; ++$wiersz) {
		    echo "\n<tr>";

		    for($kolumna = 0; $kolumna < $dump_kolumny; ++$kolumna) {

		        if($wiersz < $B_wiersze) {
		            if($kolumna < $A_kolumny) {
		                echo "<td> </td>";
		            }
		            else {
                        $B_wiersz = $B[$wiersz];
                        $wartosc = $B_wiersz[$kolumna - $A_kolumny];
                        $inputName = "Br".$wiersz."[]";

                        echo "<td><input type='text' value='$wartosc' name='$inputName' /></td>";
                    }
		        }
		        else {

                    if($kolumna < $A_kolumny) {
                        $A_wiersz = $A[$wiersz - $B_wiersze];
                        $wartosc = $A_wiersz[$kolumna];
                        $inputName = "Ar".($wiersz - $B_wiersze)."[]";

                        echo "<td><input type='text' value='$wartosc' name='$inputName' /></td>";
                    }
                    else {
                        $R_wiersz = $R[$wiersz - $B_wiersze];
                        $R_wartosc = $R_wiersz[$kolumna - $A_kolumny];
                        echo "<td>$R_wartosc</td>";
                    }
                }
		    }

		    echo "</tr>";
		}
		?>
		</table>
		<hr />
		<input type="hidden" name="do" />
		<input type="submit" value="Policz" />
	</form>
</body>
</html>
