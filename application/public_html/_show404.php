<?php
require_once '../common/Templater.php';

function not_found_render() {
    ?>
    <h1 class="text-center">
        <strong>404 Not Found</strong>
    </h1>
    <?php
}

Templater::applyWithRender(__FILE__, "not_found_render");
?>
