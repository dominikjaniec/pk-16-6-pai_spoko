<?php
require_once '../model/TagModel.php';

class TagVM {

    public static function renderTags($pytanieID) {
        foreach (TagModel::loadTags($pytanieID) as $tag_data) {
            ?>
            <a href="#" class="popover-tag-marker">
                [<?= $tag_data["Nazwa"] ?>]
                <div class="hidden popover-tag-title">
                    <h4 class="spoko-nowrap-space">
                        <small>Tag:</small>
                        <strong><?= $tag_data["Nazwa"] ?></strong>
                    </h4>
                </div>
                <div class="hidden popover-tag-content">
                    <table class="table table-condensed small spoko-nowrap-space">
                        <tr>
                            <td>Suma ocen pytań:</td>
                            <td><?= $tag_data["Wypowiedz_SumaWartosci"] ?></td>
                        </tr>
                        <tr>
                            <td>Liczba pytań:</td>
                            <td><?= $tag_data["Wypowiedz_Ilosc"] ?></td>
                        </tr>
                        <tr>
                            <td>Ulubiony przez:</td>
                            <td><?= $tag_data["Uzytkownicy_Ulubiony"] ?></td>
                        </tr>
                    </table>
                </div>
            </a>
            <?php
        }
    }

}
?>
