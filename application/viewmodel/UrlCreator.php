<?php

require_once '../model/OdpowiedzModel.php';
require_once '../model/KomentarzModel.php';
require_once 'PytanieVM.php';
require_once 'UzytkownikVM.php';

class UrlCreator {

    public static function forWypowiedz($wypowiedzID) {
        // TODO : probably to remove after beter links with #hash
        $pytanieID = OdpowiedzModel::getPytanieID($wypowiedzID);
        $pytanieID = $pytanieID !== 0 ? $pytanieID : $wypowiedzID;

        return self::forPytanie($pytanieID);
    }

    public static function forPytanie($pytanieID) {
        return self::prepare(Navigations::PYTANIE, PytanieVM::SHOW_ACTION, $pytanieID);
    }

    public static function forOdpowiedz($odpowiedzID) {
        // TODO : add show Odpowiedz by #odpowiedz_hash
        return self::forPytanie(OdpowiedzModel::getPytanieID($odpowiedzID));
    }

    public static function forKomentarz($komentarzID) {
        // TODO : add show Odpowiedz by #komentarz_hash
        return self::forPytanie(KomentarzModel::getPytanieID($komentarzID));
    }

    public static function forUzytkownik($uzytkownikID) {
        return self::prepare(Navigations::PROFIL, UzytkownikVM::SHOW_ACTION, $uzytkownikID);
    }

    private static function prepare($page, $action, $value) {
        return sprintf("%s?%s=%s", $page, $action, intval($value));
    }

}

?>
