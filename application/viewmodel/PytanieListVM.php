<?php
require_once '../common/DataBase.php';
require_once '../common/Navigations.php';
require_once '../model/SearchModel.php';
require_once 'UzytkownikVM.php';
require_once 'PytanieVM.php';
require_once 'TagVM.php';
require_once 'UrlCreator.php';

class PytanieListVM {
    const FIELD_SEARCH = "search";

    public static function render($searchTerms) {
        $pytaniaList = SearchModel::getPytaniaList($searchTerms);

        if (!empty($pytaniaList))
            self::renderList($pytaniaList);
        else
            self::renderEmpty();
    }

    private static function renderEmpty() {
        ?>
        <h2><i>:(</i> cóż... Nic tu nie ma</h2>
        <hr />
        <p class="text-center">Spróbuj poszukać inaczej, albo po prostu zadaj pytanie.</p>
        <p class="text-right">Na tym tu, to wszystko polega :)</p>
        <?php
    }

    private static function renderList($pytaniaList) {
        ?>
        <ul class="list-group">
            <?php foreach ($pytaniaList as $pytanie) { ?>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-md-1 text-right">
                            <small>
                                <span class="badge">
                                    <?= sprintf("%d/%d", $pytanie["Oceny_SumaWartosc"], $pytanie["Oceny_Ilosc"]) ?>
                                </span>
                            </small>
                        </div>
                        <div class="col-md-7">
                            <?= self::renderPytanieLink($pytanie["Wypowiedz_ID"], $pytanie["Tytul"]) ?>
                        </div>
                        <div class="col-md-4 text-right small">
                            <?= UzytkownikVM::renderLink($pytanie["AutorUzytkownik_ID"]) ?>
                            <small><?= $pytanie["DataUtworzenia"] ?></small>
                        </div>
                    </div>
                    <div class="row small">
                        <div class="col-md-12">
                            <small><?= TagVM::renderTags($pytanie["Wypowiedz_ID"]) ?></small>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
        <?php
    }

    private static function renderPytanieLink($pytanieID, $title) {
        return sprintf('<a href="%s">%s</a>', UrlCreator::forPytanie($pytanieID), $title);
    }

}
?>
