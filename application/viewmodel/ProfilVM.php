<?php
require_once '../common/Helpers.php';
require_once '../common/Navigations.php';
require_once '../common/Roles.php';
require_once '../common/DebugSpoko.php';
require_once '../model/UzytkownikModel.php';
require_once '../model/UserSessionModel.php';
require_once 'UzytkownikVM.php';

class ProfilVM {
    const SHOW_ACTION = "id";
    const SIGNIN_ACTION = "signin";
    const REGISTER_ACTION = "register";

    const FIELD_EMAIL = "email";
    const FIELD_LOGIN = "login";
    const FIELD_PASSWORD = "password";
    const FIELD_PASSWORD_REENTRED = "password_reentred";
    const FIELD_REMEMBERMY = "remember_me";

    const RESULT_STATUS = "status";
    const RESULT_MESSAGES = "errorMessages";

    public static function handleRegisterPost() {
        $login = resolvePostField(self::FIELD_LOGIN);
        $email = resolvePostField(self::FIELD_EMAIL);
        $password = resolvePostField(self::FIELD_PASSWORD);
        $passwordReentred = resolvePostField(self::FIELD_PASSWORD_REENTRED);

        try {
            if (strcmp($password, $passwordReentred) !== 0)
                throw new Exception("Podane hasła nie odpowiadają sobie.");

            UzytkownikModel::create($login, $password, $email, Roles::USER);
            UserSessionModel::authenticate($login, $password);

            return array(
                self::RESULT_STATUS => true
            );
        } catch (Exception $ex) {
            return array(
                self::FIELD_LOGIN => $login,
                self::FIELD_EMAIL => $email,
                self::RESULT_STATUS => false,
                self::RESULT_MESSAGES => array($ex->getMessage())
            );
        }
    }

    public static function registerPostDefaultResult() {
        return array(
            self::FIELD_LOGIN => "",
            self::FIELD_EMAIL => "",
            self::RESULT_STATUS => null,
            self::RESULT_MESSAGES => array()
        );
    }

    public static function handleLoginPost() {
        $login = resolvePostField(self::FIELD_LOGIN);
        $password = resolvePostField(self::FIELD_PASSWORD);
        $rememberme = resolvePostCheckbox(self::FIELD_REMEMBERMY);

        try {
            UserSessionModel::authenticate($login, $password);

            return array(
                self::RESULT_STATUS => true
            );
        } catch (Exception $ex) {
            $messages = array("Proszę podać poprawne dane logowania.");
            if (DebugSpoko::shouldShow())
                array_push($messages, $ex->getMessage());

            return array(
                self::FIELD_LOGIN => $login,
                self::RESULT_STATUS => false,
                self::RESULT_MESSAGES => $messages
            );
        }
    }

    public static function loginPostDefaultResult() {
        return array(
            self::FIELD_LOGIN => "",
            self::RESULT_STATUS => null,
            self::RESULT_MESSAGES => array()
        );
    }

    public static function renderProfile($userID) {
        ?>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <?php UzytkownikVM::renderBasic($userID); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 spoko-profile">
                        <?php
                        if (!self::hasCurrentUserAcceess($userID)) {
                            UzytkownikVM::renderStatistics($userID);
                        } else {
                            ?>
                            <ul class="nav nav-tabs nav-justified" role="tablist">
                                <li role="presentation" class="active"><a href="#user-statistics" role="tab" data-toggle="tab">Statystyki</a></li>
                                <li role="presentation"><a href="#user-profile" role="tab" data-toggle="tab">Profile</a></li>
                                <li role="presentation"><a href="#user-tags" role="tab" data-toggle="tab">Tagi</a></li>
                                <?php if (CurrentUser::isAdmin()) { ?>
                                    <li role="presentation"><a href="#user-manage" role="tab" data-toggle="tab">Zarządzaj</a></li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="user-statistics">
                                    <?php UzytkownikVM::renderStatistics($userID); ?>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="user-profile">
                                    <?php UzytkownikVM::renderEdit($userID); ?>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="user-tags">
                                    <?php UzytkownikVM::renderTags($userID); ?>
                                </div>
                                <?php if (CurrentUser::isAdmin()) { ?>
                                    <div role="tabpanel" class="tab-pane" id="user-manage">
                                        <?php UzytkownikVM::renderManage($userID); ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <?php UzytkownikVM::renderGravatar($userID, UzytkownikVM::GRAVATAR_KIND_PROFILE); ?>
            </div>
        </div>
        <?php
    }

    public static function renderSignIn($signInStatus) {
        ?>
        <form action="<?= self::getSignAction() ?>" method="post" class="spoko-sign">
            <fieldset>
                <h2>Witamy</h2>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" name="<?= self::FIELD_LOGIN ?>" class="form-control"
                                   placeholder="login" required autofocus
                                   maxlength="<?= UzytkownikModel::LENGHT_LOGIN ?>"
                                   value="<?= $signInStatus[self::FIELD_LOGIN] ?>" />
                            <input type="password" name="<?= self::FIELD_PASSWORD ?>" class="form-control"
                                   placeholder="hasło" required
                                   maxlength="<?= UzytkownikModel::LENGHT_MAX_PASSWORD ?>" />
                        </div>
                    </div>
                </div>
                <?php
                if (!empty($signInStatus[self::RESULT_MESSAGES])) {
                    foreach ($signInStatus[self::RESULT_MESSAGES] as $errorMessage) {
                        ?>
                        <div class="row">
                            <div class="col-md-12 form-group text-danger">
                                <?= $errorMessage ?>
                            </div>
                        </div>
                    <?php }
                } ?>
                <div class="row">
                    <div class="col-md-5">
                        <div class="checkbox spoko-small-padding-left">
                            <label>
                                <input type="checkbox" name="<?= self::FIELD_REMEMBERMY ?>" />
                                Zapamiętaj mnie
                            </label>
                        </div>
                    </div>
                    <div class="col-md-7 text-right">
                        <div class="spoko-small-padding-right">
                            <button type="submit" class="btn btn-lg btn-primary btn-block">
                                Zaloguj
                            </button>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
        <?php
    }

    public static function renderRegister($registerStatus) {
        ?>
        <form action="<?= self::getRegisterAction() ?>" method="post"
              id="spoko-register-form" class="spoko-sign">
            <fieldset>
                <h2>Zarejestruj się</h2>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group has-feedback">
                            <input type="text" id="<?= self::FIELD_LOGIN ?>" name="<?= self::FIELD_LOGIN ?>"
                                   class="form-control" placeholder="login" required autofocus
                                   maxlength="<?= UzytkownikModel::LENGHT_LOGIN ?>"
                                   value="<?= $registerStatus[self::FIELD_LOGIN] ?>" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="email" name="<?= self::FIELD_EMAIL ?>"
                                   class="form-control" placeholder="email" required email
                                   maxlength="<?= UzytkownikModel::LENGHT_EMAIL ?>"
                                   value="<?= $registerStatus[self::FIELD_EMAIL] ?>" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="password" id="<?= self::FIELD_PASSWORD ?>" name="<?= self::FIELD_PASSWORD ?>"
                                   class="form-control" placeholder="hasło" required
                                   minlength="<?= UzytkownikModel::LENGHT_MIN_PASSWORD ?>"
                                   maxlength="<?= UzytkownikModel::LENGHT_MAX_PASSWORD ?>" />
                            <input type="password" id="<?= self::FIELD_PASSWORD_REENTRED ?>" name="<?= self::FIELD_PASSWORD_REENTRED ?>"
                                   class="form-control" placeholder="potwierdz hasło" required />
                        </div>
                    </div>
                </div>
                <?php
                if (!empty($registerStatus[self::RESULT_MESSAGES])) {
                    foreach ($registerStatus[self::RESULT_MESSAGES] as $errorMessage) {
                        ?>
                        <div class="row">
                            <div class="col-md-12 form-group text-danger">
                                <?= $errorMessage ?>
                            </div>
                        </div>
                    <?php }
                } ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="spoko-small-padding">
                            <button type="submit" class="btn btn-lg btn-primary btn-block">
                                Zapisz
                            </button>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
        <script type="text/javascript">
            spoko_register_form = {
            login_id: "#<?= self::FIELD_LOGIN ?>",
            login_check_url: "availability.php",
            password_id: "#<?= self::FIELD_PASSWORD ?>",
            password_reentred_id: "#<?= self::FIELD_PASSWORD_REENTRED ?>"
            };
        </script>
        <?php
    }

    public static function hasCurrentUserAcceess($userID) {
        if (CurrentUser::isLogged()) {
            return CurrentUser::isAdmin()
                || CurrentUser::id() == $userID;
        }

        return false;
    }

    private static function getSignAction() {
        return Navigations::ZALOGUJ . "?" . self::SIGNIN_ACTION;
    }

    private static function getRegisterAction() {
        return Navigations::REJESTRACJA . "?" . self::REGISTER_ACTION;
    }

}
?>
