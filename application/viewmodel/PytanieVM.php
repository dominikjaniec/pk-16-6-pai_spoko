<?php
require_once '../common/Helpers.php';
require_once '../common/CurrentUser.php';
require_once '../common/Navigations.php';
require_once '../model/PytanieModel.php';
require_once 'UzytkownikVM.php';
require_once 'TagVM.php';
require_once 'WypowiedzCreateVM.php';

class PytanieVM {
    const SHOW_ACTION = "id";

    const STATEMENT_PAGE = "statement.php";
    const SHOW_RATE_ACTION = "rate";
    const SHOW_ACCEPT_ACTION = "accept";

    const FIELD_WYPOWIEDZ_ID = "wypowiedz_id";
    const FIELD_RATE_DIRECTION = "rate_direction";

    const RATE_DIRECTION_UP = "up";
    const RATE_DIRECTION_DOWN = "down";

    public static function handleAcceptPost() {
        $wypowiedzID = resolvePostField(self::FIELD_WYPOWIEDZ_ID);

        if (OdpowiedzModel::canBeAccepted($wypowiedzID, CurrentUser::id())) {
            OdpowiedzModel::accept($wypowiedzID);
            return $wypowiedzID;
        };

        throw new Exception("Odpowiedz nie może zostać zaakceptowana...");
    }

    public static function handleRatePost() {
        $wypowiedzID = resolvePostField(self::FIELD_WYPOWIEDZ_ID);
        $rateDirection = resolvePostField(self::FIELD_RATE_DIRECTION);

        if (OdpowiedzModel::canBeRated($wypowiedzID, CurrentUser::id())) {
            switch ($rateDirection) {
                case self::RATE_DIRECTION_UP:
                    OdpowiedzModel::rateUp($wypowiedzID, CurrentUser::id());
                    return $wypowiedzID;

                case self::RATE_DIRECTION_DOWN:
                    OdpowiedzModel::rateDown($wypowiedzID, CurrentUser::id());
                    return $wypowiedzID;
            }
        };

        throw new Exception("Wypowiedz nie może zostać oceniona...");
    }

    public static function render($pytanieID) {
        $dane = PytanieModel::load($pytanieID);
        $pytanie = $dane[PytanieModel::RESULT_PYTANIE];
        $odpowiedzi = $dane[PytanieModel::RESULT_ODPOWIEDZI];

        self::renderPytanie($pytanieID, $pytanie);

        if (isset($odpowiedzi) && !empty($odpowiedzi)) {
            echo "<hr />";
            foreach ($odpowiedzi as $odpowiedz)
                self::renderOdpowiedz($odpowiedz, $pytanie);
        }

        if (CurrentUser::isLogged()) {
            echo "<hr />";
            WypowiedzCreateVM::renderOdpowiedz($pytanieID);
        }
    }

    public static function getTytul($pytanieID) {
        return PytanieModel::loadTitle($pytanieID);
    }

    private static function renderPytanie($pytanieID, $pytanie) {
        ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3><?= $pytanie["Tytul"] ?></h3>
                <small><?php TagVM::renderTags($pytanieID) ?></small>
            </div>
            <?php self::renderWypowiedzContent($pytanie, $pytanie) ?>
        </div>
        <?php
    }

    private static function renderOdpowiedz($wypowiedz, $forPytanie) {
        ?>
        <div class="panel panel-default">
            <?php self::renderWypowiedzContent($wypowiedz, $forPytanie) ?>
        </div>
        <?php
    }

    private static function renderWypowiedzContent($wypowiedz, $forPytanie) {
        ?>
        <div class="panel panel-body">
            <div class="row">
                <div class="col-md-1 text-center spoko-nopadding-right">
                    <?php self::renderOcene($wypowiedz, $forPytanie) ?>
                </div>
                <div class="col-md-11">
                    <div class="well">
                        <p><?= $wypowiedz["Tresc"]; ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?php
                    if (CurrentUser::isLogged()) {
                        $wypowiedzID = $wypowiedz["Wypowiedz_ID"];
                        $collapse_id = "collapse-forKomentarz_" . $wypowiedzID;
                        ?>
                        <a href="#<?= $collapse_id ?>"
                           role="button" data-toggle="collapse">
                            <small>Napisz komentarz...</small>
                        </a>
                    <?php } ?>
                </div>
                <div class="col-md-8 text-right">
                    Autor:
                    <?php UzytkownikVM::renderLink($wypowiedz["AutorUzytkownik_ID"]) ?>
                    <small><?= $wypowiedz["DataUtworzenia"] ?></small>
                </div>
            </div>
            <?php if (CurrentUser::isLogged()) { ?>
                <div class="row small">
                    <div class="col-md-12">
                        <?php WypowiedzCreateVM::renderKomentarz($wypowiedzID, $collapse_id); ?>
                    </div>
                </div>
            <?php } ?>
        </div>
        <table class="table table-striped table-condensed small">
            <?php foreach ($wypowiedz[PytanieModel::RESULT_KOMENTARZE] as $komentarz) { ?>
                <tr>
                    <td>
                        <div class="spoko-small-padding-left">
                            <?= $komentarz["Tresc"] ?>
                        </div>
                    </td>
                    <td class="text-right spoko-nowrap-space">
                        ~
                        <?php UzytkownikVM::renderLink($komentarz["AutorUzytkownik_ID"]) ?>
                        <small><?= $komentarz["DataUtworzenia"] ?></small>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <?php
    }

    private static function renderOcene($wypowiedz, $forPytanie) {
        $canBeAccepted = false;
        $rateButtonAttr = 'disabled="disabled"';

        if (CurrentUser::isLogged()) {
            $pytanieAuthorID = $forPytanie["AutorUzytkownik_ID"];
            $canBeAccepted = CurrentUser::id() == $pytanieAuthorID;

            $wypowiedzAuthorID = $wypowiedz["AutorUzytkownik_ID"];
            if (CurrentUser::id() != $wypowiedzAuthorID)
                $rateButtonAttr = 'type="submit"';
        }

        $isAccepted = false;
        if ($wypowiedz != $forPytanie) {
            $isAccepted = (bool) intval($wypowiedz["CzyUznana"]);
        } else {
            $canBeAccepted = false;
        }

        $ocenaSummaWartosc = $wypowiedz["Oceny_SumaWartosc"];
        if ($ocenaSummaWartosc > 0)
            $ocenaSummaWartosc = "+" . $ocenaSummaWartosc;
        ?>
        <div class="spoko-ocena-up">
            <form action="<?= self::getRateActionUrl() ?>" method="post">
                <input type="hidden"
                       name="<?= self::FIELD_WYPOWIEDZ_ID ?>"
                       value="<?= $wypowiedz["Wypowiedz_ID"] ?>" />
                <input type="hidden"
                       name="<?= self::FIELD_RATE_DIRECTION ?>"
                       value="<?= self::RATE_DIRECTION_UP ?>" />
                <button class="btn btn-link" <?= $rateButtonAttr ?>>
                    <span class="h3 text-success">
                        <span class="glyphicon glyphicon-triangle-top"></span>
                    </span>
                </button>
            </form>
        </div>
        <div>
            <strong><?= $ocenaSummaWartosc ?></strong>
        </div>
        <div class="spoko-ocena-down">
            <form action="<?= self::getRateActionUrl() ?>" method="post">
                <input type="hidden"
                       name="<?= self::FIELD_WYPOWIEDZ_ID ?>"
                       value="<?= $wypowiedz["Wypowiedz_ID"] ?>" />
                <input type="hidden"
                       name="<?= self::FIELD_RATE_DIRECTION ?>"
                       value="<?= self::RATE_DIRECTION_DOWN ?>" />
                <button class="btn btn-link" <?= $rateButtonAttr ?>>
                    <span class="h3 text-danger">
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </span>
                </button>
            </form>
        </div>
        <?php if ($isAccepted) { ?>
            <div class="h4 text-danger spoko-nomargin">
                <span class="text-success">
                    <span class="glyphicon glyphicon-ok"></span>
                </span>
            </div>
        <?php } else if ($canBeAccepted) { ?>
            <div>
                <form action="<?= self::getAcceptActionUrl() ?>" method="post">
                    <input type="hidden"
                           name="<?= self::FIELD_WYPOWIEDZ_ID ?>"
                           value="<?= $wypowiedz["Wypowiedz_ID"] ?>" />
                    <button class="btn btn-xs btn-success" type="submit">
                        <span class="glyphicon glyphicon-ok"></span>
                    </button>
                </form>
            </div>
        <?php } ?>
        <?php
    }

    private static function getAcceptActionUrl() {
        return self::STATEMENT_PAGE . "?" . self::SHOW_ACCEPT_ACTION;
    }

    private static function getRateActionUrl() {
        return self::STATEMENT_PAGE . "?" . self::SHOW_RATE_ACTION;
    }

}
?>
