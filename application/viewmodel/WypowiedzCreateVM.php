<?php
require_once '../common/Helpers.php';
require_once '../common/CurrentUser.php';
require_once '../common/Navigations.php';
require_once '../model/KomentarzModel.php';
require_once '../model/PytanieModel.php';
require_once '../model/OdpowiedzModel.php';
require_once '../model/TagModel.php';
require_once 'UrlCreator.php';

class WypowiedzCreateVM {
    const CREATE_ACTION = "create";

    const ACTION_KIND = "action_kind";
    const ACTION_KIND_PYTANIE = "pytanie";
    const ACTION_KIND_ODPOWIEDZ = "odpowiedz";
    const ACTION_KIND_KOMENTARZ = "komentarz";

    const FIELD_TITLE = "tytul";
    const FIELD_TAGS = "tagi";
    const FIELD_CONTENT = "tresc";
    const FIELD_ODPOWIEDZ_PYTANIE_ID = "pytanieID";
    const FIELD_KOMENTARZ_WYPOWIEDZ_ID = "wypowiedzID";

    public static function handleCreatePost() {
        $actionKind = resolvePostField(self::ACTION_KIND);

        switch ($actionKind) {
            case self::ACTION_KIND_PYTANIE :
                $pytanieID = self::handleCreatePytanie();
                return UrlCreator::forPytanie($pytanieID);

            case self::ACTION_KIND_ODPOWIEDZ :
                $odpowiedzID = self::handleCreateOdpowiedz();
                return UrlCreator::forOdpowiedz($odpowiedzID);

            case self::ACTION_KIND_KOMENTARZ :
                $komentarzID = self::handleCreateKomentarz();
                return UrlCreator::forKomentarz($komentarzID);
        }

        throw new Exception("Unknown action kind: '" . $actionKind . "'");
    }

    private static function handleCreatePytanie() {
        $title = resolvePostField(self::FIELD_TITLE);
        $content = resolvePostField(self::FIELD_CONTENT);

        $pytanieID = PytanieModel::create(CurrentUser::id(), $title, $content);
        self::handleConnectTags($pytanieID);

        return $pytanieID;
    }

    private static function handleConnectTags($pytanieID) {
        $tagsList = resolvePostField(self::FIELD_TAGS);
        if (empty($tagsList))
            return;

        $tags = explode(TagModel::LIST_SEPARATOR, $tagsList);
        TagModel::setForPytanie($pytanieID, $tags);
    }

    private static function handleCreateOdpowiedz() {
        $pytanieID = intval(resolvePostField(self::FIELD_ODPOWIEDZ_PYTANIE_ID));
        $content = resolvePostField(self::FIELD_CONTENT);

        return OdpowiedzModel::create(CurrentUser::id(), $pytanieID, $content);
    }

    private static function handleCreateKomentarz() {
        $wypowiedzID = intval(resolvePostField(self::FIELD_KOMENTARZ_WYPOWIEDZ_ID));
        $content = resolvePostField(self::FIELD_CONTENT);

        return KomentarzModel::create(CurrentUser::id(), $wypowiedzID, $content);
    }

    public static function renderPytanie() {
        ?>
        <form action="<?= self::getActionUrl() ?>" method="post">
            <fieldset>
                <input type="hidden" name="<?= self::ACTION_KIND ?>"
                       value="<?= self::ACTION_KIND_PYTANIE ?>" />
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="form-group">
                            <input type="text" name="<?= self::FIELD_TITLE ?>"
                                   placeholder="Tytuł" class="form-control input-lg" autofocus
                                   required maxlength="<?= PytanieModel::LENGHT_TITLE ?>" />
                                   <?php self::renderTagsInput(); ?>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php self::renderWypowiedzContent("Zapytaj") ?>
                    </div>
                </div>
            </fieldset>
        </form>
        <?php
    }

    public static function renderOdpowiedz($pytanieID) {
        ?>
        <form action="<?= self::getActionUrl() ?>" method="post">
            <fieldset>
                <input type="hidden" name="<?= self::ACTION_KIND ?>"
                       value="<?= self::ACTION_KIND_ODPOWIEDZ ?>" />
                <input type="hidden" name="<?= self::FIELD_ODPOWIEDZ_PYTANIE_ID ?>"
                       value="<?= intval($pytanieID) ?>" />
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?php self::renderWypowiedzContent("Odpowiedz") ?>
                    </div>
                </div>
            </fieldset>
        </form>
        <?php
    }

    public static function renderKomentarz($wypowiedzID, $collapse_id) {
        ?>
        <div class="row">
            <div class="collapse" id="<?= $collapse_id ?>">
                <form action="<?= self::getActionUrl() ?>" method="post"
                      class="spoko-nomargin">
                    <fieldset>
                        <input type="hidden" name="<?= self::ACTION_KIND ?>"
                               value="<?= self::ACTION_KIND_KOMENTARZ ?>" />
                        <input type="hidden" name="<?= self::FIELD_KOMENTARZ_WYPOWIEDZ_ID ?>"
                               value="<?= intval($wypowiedzID) ?>" />
                        <div class="col-md-10">
                            <div class="form-group spoko-nomargin">
                                <textarea name="<?= self::FIELD_CONTENT ?>" rows="1"
                                          required placeholder="Komentarz"
                                          class="form-control input-sm spoko-komentarz-textarea"></textarea>
                            </div>
                        </div>
                        <div class="col-md-2 text-right">
                            <div class="spoko-stretch-horizontal">
                                <button type="submit" class="btn btn-sm btn-primary">Wyślij</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <?php
    }

    private static function renderWypowiedzContent($promptContent) {
        ?>
        <div class="row">
            <div class="col-md-9">
                <div class="form-group spoko-nomargin">
                    <textarea name="<?= self::FIELD_CONTENT ?>" rows="4"
                              required placeholder="<?= $promptContent ?>"
                              class="form-control spoko-wypowiedz-textarea"></textarea>
                </div>
            </div>
            <div class="col-md-3">
                <div role="group" class="btn-group-vertical spoko-stretch-horizontal">
                    <button type="submit" class="btn btn-default" disabled>
                        Podgląd
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <?= $promptContent ?>
                    </button>
                </div>
            </div>
        </div>
        <?php
    }

    private static function renderTagsInput() {
        ?>
        <div class="spoko-tags-edit spoko-pytanie-tags-edit"
             data-fieldname="<?= self::FIELD_TAGS ?>"
             data-splitter="<?= TagModel::LIST_SEPARATOR ?>"
             data-use-user-preferences="true"
             data-placeholder="Tagi">
            <ul></ul>
        </div>
        <?php
    }

    private static function getActionUrl() {
        return Navigations::NAPISZ . "?" . self::CREATE_ACTION;
    }

}
?>
