<?php
require_once '../common/Helpers.php';
require_once '../common/DataBase.php';
require_once '../common/Navigations.php';
require_once '../model/UzytkownikModel.php';
require_once '../model/TagModel.php';
require_once 'UrlCreator.php';

class UzytkownikVM {
    const SHOW_ACTION = "id";
    const TAGS_USAGE_ACTION = "tags_usage";

    const FIELD_TAGS_FAVOURITE = "tags_favourite";
    const FIELD_TAGS_IGNORE = "tags_ignore";

    const GRAVATAR_KIND_SIDE = 1;
    const GRAVATAR_KIND_PROFILE = 2;
    const GRAVATAR_KIND_POPOVER = 3;

    public static function renderGravatar($userID, $imgKind) {
        $user_data = UzytkownikModel::loadLinkViewData($userID);

        $gravatarHash = trim($user_data["Email"]);
        $gravatarHash = strtolower($gravatarHash);
        $gravatarHash = md5($gravatarHash);

        $url = "https://secure.gravatar.com/avatar/" . $gravatarHash;
        $url = $url . "?d=identicon&s=" . self::getAvatarSize($imgKind);
        ?>
        <img src="<?= $url ?>" class="center-block" alt="<?= $user_data["Login"] ?> Gravatar" />
        <?php
    }

    private static function getAvatarSize($imgKind) {
        switch ($imgKind) {
            case self::GRAVATAR_KIND_SIDE:
                return 150;
            case self::GRAVATAR_KIND_PROFILE:
                return 300;
            case self::GRAVATAR_KIND_POPOVER:
                return 55;
            default:
                return 80;
        }
    }

    public static function renderBasic($userID) {
        $userData = UzytkownikModel::loadLinkViewData($userID);
        ?>
        <div class="spoko-inlineblock">
            <h4 class="spoko-nowrap-space">
                <strong>
                    <?= isset($userData["PelnaNazwa"]) ? $userData["PelnaNazwa"] : $userData["Login"] ?>
                    <span class="glyphicon glyphicon-star-empty"></span>
                    <?= $userData["WypowiedzOcena_SumaWartosci"] ?>
                </strong>
            </h4>
            <small>
                Od: <?= $userData["DataUtworzenia"] ?>
            </small>
        </div>
        <?php
    }

    public static function renderStatistics($userID) {
        $userData = UzytkownikModel::loadLinkViewData($userID);
        ?>
        <table class="table table-condensed spoko-nowrap-space">
            <tr>
                <td>Liczba wypowiedzi:</td>
                <td><?= $userData["Wypowiedz_Ilosc"] ?></td>
            </tr>
            <tr>
                <td>Ostatnia wypowiedz:</td>
                <td><?= $userData["Wypowiedz_Ostatnia"] ?></td>
            </tr>
            <tr>
                <td>Liczba pytań:</td>
                <td><?= $userData["WypowiedzPytanie_Ilosc"] ?></td>
            </tr>
            <tr>
                <td>Ostatnie pytanie:</td>
                <td><?= $userData["WypowiedzPytanie_Ostatnie"] ?></td>
            </tr>
        </table>
        <?php
    }

    public static function renderLink($userID) {
        $userData = UzytkownikModel::loadLinkViewData($userID);
        ?>
        <a href="<?= UrlCreator::forUzytkownik($userID) ?>" class="popover-user-marker">
            <?= $userData["Login"] ?>
            <div class="hidden popover-user-title">
                <?php self::renderBasic($userID); ?>
                <div class="pull-right">
                    <?php self::renderGravatar($userID, self::GRAVATAR_KIND_POPOVER) ?>
                </div>
            </div>
            <div class="hidden popover-user-content">
                <div class="small">
                    <?php self::renderStatistics($userID); ?>
                </div>
            </div>
        </a>
        <?php
    }

    public static function renderEdit($userID) {
        ?>
        <div class="spoko-profile-tab">
            TODO : Możliwość zmiany hasła, czy pełnej nazwy użytkownika
        </div>
        <?php
    }

    public static function handleTagsUsagePost() {
        $userID = intval(resolveGetValue(self::SHOW_ACTION));
        if(!ProfilVM::hasCurrentUserAcceess($userID))
            throw new Exception("Brak uprawnień do zmiany Tagów Użytkownika...");

        $tagsFavourite = resolvePostField(self::FIELD_TAGS_FAVOURITE);
        $tagsFavourite = explode(TagModel::LIST_SEPARATOR, $tagsFavourite);

        $tagsIgnored = resolvePostField(self::FIELD_TAGS_IGNORE);
        $tagsIgnored = explode(TagModel::LIST_SEPARATOR, $tagsIgnored);

        TagModel::setUserTagsUsage($userID, $tagsFavourite, $tagsIgnored);
        return UrlCreator::forUzytkownik($userID);
    }

    public static function renderTags($userID) {
        $usersTags = TagModel::loadUsersTags($userID);
        ?>
        <form class="spoko-profile-tab" method="post">
            <input type="hidden" name="<?= self::TAGS_USAGE_ACTION ?>" />
            <div class="form-group">
                <label for="exampleInputEmail1">Ulubione:</label>
                <div class="spoko-tags-edit"
                     data-fieldname="<?= self::FIELD_TAGS_FAVOURITE ?>"
                     data-splitter="<?= TagModel::LIST_SEPARATOR ?>"
                     data-use-user-preferences="false"
                     data-placeholder="Ulubione tagi">
                    <ul>
                    <?php
                        foreach ($usersTags[TagModel::USAGE_KIND_FAVOURITE] as $tag)
                            echo "<li>". $tag . "</li>";
                    ?>
                    </ul>
                </div>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Ignorowane:</label>
                <div class="spoko-tags-edit"
                     data-fieldname="<?= self::FIELD_TAGS_IGNORE ?>"
                     data-splitter="<?= TagModel::LIST_SEPARATOR ?>"
                     data-use-user-preferences="false"
                     data-placeholder="Ignorowane tagi">
                    <ul>
                    <?php
                        foreach ($usersTags[TagModel::USAGE_KIND_IGNORED] as $tag)
                            echo "<li>". $tag . "</li>";
                    ?>
                    </ul>
                </div>
            </div>
            <div class="spoko-small-padding">
                <button type="submit" class="btn btn-primary btn-block">
                    Zapisz ustawienia tagów
                </button>
            </div>
        </form>
        <?php
    }

    public static function renderManage($userID) {
        ?>
        <div class="spoko-profile-tab">
            TODO : Możliwość nadania / odebrania uprawnień do MOD / ADMIN
        </div>
        <?php
    }

}
?>
