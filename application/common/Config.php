<?php

class Config {
    const CONFIG_FILE = "../spoko_config.ini";
    const CONFIG_SECTION = "database";
    const SERVER = "db_server";
    const SCHEMA = "db_schema";
    const USER = "db_user";
    const PASS = "db_pass";

    public static function configurationFile() {
        return realpath(self::CONFIG_FILE);
    }

    public static function dbServer() {
        self::ensureLoaded();
        return self::$entries[self::SERVER];
    }

    public static function dbSchema() {
        self::ensureLoaded();
        return self::$entries[self::SCHEMA];
    }

    public static function dbUser() {
        self::ensureLoaded();
        return self::$entries[self::USER];
    }

    public static function dbPass() {
        self::ensureLoaded();
        return self::$entries[self::PASS];
    }

    private static $loaded = false;
    private static $entries;

    private static function ensureLoaded() {
        if (!self::$loaded) {
            self::$entries = parse_ini_file(self::CONFIG_FILE, true);
            self::$entries = self::$entries[self::CONFIG_SECTION];

            self::$loaded = true;
        }
    }

}

?>