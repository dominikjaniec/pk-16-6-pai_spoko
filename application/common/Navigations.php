<?php

require_once 'CurrentUser.php';

class Navigations {

    private static $current_page;

    const LISTA_PYTAN = "index.php";
    const NAPISZ = "create.php";
    const PYTANIE = "question.php";
    const PROFIL = "profile.php";
    const WYLOGUJ = "logout.php";
    const ZALOGUJ = "login.php";
    const REJESTRACJA = "register.php";

    public static function getOptions() {
        $navigations = array();

        if (CurrentUser::isLogged()) {
            array_push($navigations, self::create(self::NAPISZ));
        }

        array_push($navigations, self::create(self::LISTA_PYTAN));

        if (CurrentUser::isLogged()) {
            array_push($navigations, self::create(self::PROFIL));
            array_push($navigations, self::create(self::WYLOGUJ));
        } else {
            array_push($navigations, self::create(self::ZALOGUJ));
            array_push($navigations, self::create(self::REJESTRACJA));
        }

        return $navigations;
    }

    public static function setCurrent($page) {
        self::$current_page = $page;
    }

    public static function isCurrent($page) {
        return self::$current_page == $page;
    }

    public static function getName($page) {
        switch ($page) {
            case self::LISTA_PYTAN:
                return "Lista pytań";

            case self::PYTANIE:
                return "Pytanie";

            case self::NAPISZ:
                return "Zadaj pytanie";

            case self::PROFIL:
                return "Profil";

            case self::WYLOGUJ:
                return "Wyloguj";

            case self::ZALOGUJ:
                return "Zaloguj się";

            case self::REJESTRACJA:
                return "Zarejestruj się";

            default:
                return "";
        }
    }

    private static function create($page) {
        $name = self::getName($page);
        return array(
            "page_file" => $page,
            "navigation_name" => $name
        );
    }

}

?>
