<?php

require_once 'DataBase.php';

class Roles {
    const ADMIN = "ADMIN";
    const MOD = "MOD";
    const USER = "USER";

    public static function getName($roleKey) {
        switch ($roleKey) {
            case self::ADMIN:
                return "Administrator";

            case self::MOD:
                return "Moderator";

            case self::USER:
                return "Użytkownik";

            default:
                throw new Exception("Invalid Role code");
        }
    }

    public static function getId($roleKey) {
        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT r.`Rola_ID`
                    FROM `Rola` AS r
                    WHERE r.`Klucz` = :rolaKlucz");

        $sql_st->bindValue(":rolaKlucz", $roleKey, PDO::PARAM_STR);
        $sql_st->execute();

        $result = $sql_st->fetch(PDO::FETCH_NUM);
        return intval($result[0]);
    }

    public static function getById($rolaID) {
        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT r.`Klucz`
                    FROM `Rola` AS r
                    WHERE r.`Rola_ID` = :rolaID");

        $sql_st->bindValue(":rolaID", $rolaID, PDO::PARAM_STR);
        $sql_st->execute();

        $result = $sql_st->fetch(PDO::FETCH_NUM);
        return $result[0];
    }

}

?>
