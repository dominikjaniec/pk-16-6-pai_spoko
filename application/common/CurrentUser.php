<?php

require_once '../model/UserSessionModel.php';
require_once 'Roles.php';

class CurrentUser {

    private static $curentUser;

    public static function isLogged() {
        self::ensureLoaded();
        return isset(self::$curentUser);
    }

    public static function isAdmin() {
        return Roles::ADMIN == self::rola();
    }

    public static function isMod() {
        return self::isAdmin()
                || Roles::MOD == self::rola();
    }

    public static function id() {
        self::throwIfNotLogged();
        return self::$curentUser[UserSessionModel::LOAD_USER_ID];
    }

    public static function login() {
        self::throwIfNotLogged();
        return self::$curentUser[UserSessionModel::LOAD_LOGIN];
    }

    public static function displayName() {
        self::throwIfNotLogged();

        $fullname = self::$curentUser[UserSessionModel::LOAD_FULLNAME];
        if (isset($fullname) && !empty($fullname))
            return $fullname;

        return self::$curentUser[UserSessionModel::LOAD_LOGIN];
    }

    private static function throwIfNotLogged() {
        if (!self::isLogged())
            throw new Exception("There is no logged user here");
    }

    private static function ensureLoaded() {
        if (isset(self::$curentUser))
            return;

        self::$curentUser = UserSessionModel::loadCurrentUserData();
    }

    private static function rola() {
        self::throwIfNotLogged();
        return self::$curentUser[UserSessionModel::LOAD_ROLA];
    }

}

?>
