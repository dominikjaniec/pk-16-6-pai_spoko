<?php

require_once 'Helpers.php';
require_once 'Config.php';

class DataBase {

    public static function GetConnection() {
        $dns = sprintf("mysql:host=%s;dbname=%s;charset=utf8", Config::dbServer(), Config::dbSchema());
        return self::GetConnectionFor($dns);
    }

    public static function GetConnectionWithoutSchema() {
        $dns = sprintf("mysql:host=%s;charset=utf8", Config::dbServer());
        return self::GetConnectionFor($dns);
    }

    public static function GetPrivateConnection() {
        // TODO : create and store private connection...
        return self::GetConnection();
    }

    public static function ReleasePrivateConnection($connection) {
        // TODO : release connection...
    }

    public static function EnsureSchemaExists() {
        // Should throw, or return true.
        return self::GetConnection() != null;
    }

    private static function GetConnectionFor($dns) {
        $pdo = self::createPDO($dns);

        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);

        return $pdo;
    }

    private static function createPDO($dns) {
        try {
            return new PDO($dns, Config::dbUser(), Config::dbPass());
        } catch (PDOException $pdoEx) {
            if (!__tryHandlePDOCreationException($pdoEx))
                throw $pdoEx;
        }
    }
}

?>
