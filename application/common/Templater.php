<?php

require_once 'Navigations.php';

class Templater {

    public static function apply($current_file) {
        $file = basename($current_file);
        $title = Navigations::getName($file);
        self::applyTemplate($file, $title, true, null, null);
    }

    public static function applyWithTitle($current_file, $page_title) {
        $file = basename($current_file);
        self::applyTemplate($file, $page_title, true, null, null);
    }

    public static function applyForInstaller($current_file) {
        $file = basename($current_file);
        self::applyTemplate($file, "Spoko Installer", false, true, null);
    }

    public static function applyWithRender($current_file, $render) {
        $file = basename($current_file);
        self::applyTemplate($file, null, true, null, $render);
    }

    private static function applyTemplate($current_file, $page_title, $show_navigation, $from_installer, $render) {
        Navigations::setCurrent($current_file);
        $template_current_file = $current_file;
        $template_show_navigation = $show_navigation;
        $template_title = $page_title;
        $template_from_spoko_install = $from_installer;
        $template_override_render = $render;

        require '../public_html/_template.php';
    }

}

?>
