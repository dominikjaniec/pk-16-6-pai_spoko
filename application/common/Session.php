<?php

require_once 'DataBase.php';
require_once 'Helpers.php';

class Session {
    const KEY_CURRENT_USER = "_current_user_data_";

    // Session will always expired in 14 days:
    const SESSION_EXPIRATION_TIME = 1209600;
    const SESSION_COOKIE_LIFETIME = 0;
    const SESSION_COOKIE_HTTP_ONLY = true;

    public static function exists($key) {
        self::ensureIsPrepared();
        return isset($_SESSION[$key]);
    }

    public static function get($key) {
        self::ensureIsPrepared();
        return $_SESSION[$key];
    }

    public static function set($key, $value) {
        self::ensureIsPrepared();
        $_SESSION[$key] = $value;
    }

    public static function isActive() {
        return session_status() === PHP_SESSION_ACTIVE;
    }

    public static function changeId() {
        self::ensureIsPrepared();
        session_regenerate_id(true);
    }

    public static function restart() {
        self::$restartSession = true;
        self::ensureIsPrepared();
    }

    private static $sessionHandler;
    private static $restartSession = false;

    private static function ensureIsPrepared() {
        if (!self::IsActive())
            self::preparePhpSession();

        if (self::$restartSession || self::$sessionHandler->isExpired()) {
            $_SESSION = array();
            session_destroy();

            self::startSession();
            session_regenerate_id(true);

            self::$restartSession = false;
        }
    }

    private static function preparePhpSession() {
        self::$sessionHandler = new _spokoPhpSessionHandler();
        session_set_save_handler(self::$sessionHandler, true);

        self::startSession();
    }

    private static function startSession() {
        $currentCookieParams = session_get_cookie_params();
        session_start(array(
            "session.cookie_lifetime" => self::SESSION_COOKIE_LIFETIME,
            "session.cookie_path" => $currentCookieParams["path"],
            "session.cookie_domain" => $currentCookieParams["domain"],
            "session.cookie_secure" => $currentCookieParams["secure"],
            "session.cookie_httponly" => self::SESSION_COOKIE_HTTP_ONLY
        ));
    }

}

class _spokoPhpSessionHandler implements SessionHandlerInterface {

    private $expired = false;
    private $dbConnection;
    private $reqestTime;

    public function isExpired() {
        return $this->expired;
    }

    public function open($savePath, $sessionName) {
        $this->dbConnection = DataBase::GetPrivateConnection();
        $this->reqestTime = intval($_SERVER["REQUEST_TIME"]);

        return true;
    }

    public function close() {
        DataBase::ReleasePrivateConnection($this->dbConnection);
        $this->dbConnection = null;

        return true;
    }

    public function read($id) {
        $sessionData = $this->loadValue($id);
        if ($sessionData === false) {
            $this->createNew($id, "");
            $sessionData = "";
        }

        return $sessionData;
    }

    public function write($id, $data) {
        try {
            $this->updateValue($id, $data);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function destroy($id) {
        $this->dropSession($id);
        return true;
    }

    public function gc($maxlifetime) {
        $this->dropOldSessions();
        return true;
    }

    private function loadValue($id) {
        $sql_st = $this->dbConnection->prepare(
                "SELECT us.`Session_VALUE`, UNIX_TIMESTAMP(us.`ExpirationTime`)
                    FROM `UzytkownikSesja` AS us
                    WHERE us.`Session_KEY` = :sessionKey");

        $sql_st->bindValue(":sessionKey", $id);
        $sql_st->execute();

        $sessionData = $sql_st->fetch(PDO::FETCH_NUM);
        if (empty($sessionData))
            return false;

        $this->expired = $this->reqestTime >= $sessionData[1];
        return (string) $sessionData[0];
    }

    private function createNew($id, $data) {
        if (strlen($id) > 50)
            throw new Exception("Session ID is too big... ID: '" . $id . "'");

        $sql_st = $this->dbConnection->prepare(
                "INSERT INTO `UzytkownikSesja`
                    (`Session_KEY`, `Session_VALUE`, `ExpirationTime`)
                    VALUES (:key, :value, FROM_UNIXTIME(:expires))");

        $sql_st->bindValue(":key", $id, PDO::PARAM_STR);
        $sql_st->bindValue(":value", $data, PDO::PARAM_STR);
        $sql_st->bindValue(":expires", $this->getNextExpiresTime(), PDO::PARAM_INT);
        $sql_st->execute();
    }

    private function updateValue($id, $data) {
        $sql_st = $this->dbConnection->prepare(
                "UPDATE `UzytkownikSesja`
                    SET `Session_VALUE` = :value
                    WHERE `Session_KEY` = :key");

        $sql_st->bindValue(":key", $id, PDO::PARAM_STR);
        $sql_st->bindValue(":value", $data, PDO::PARAM_STR);
        $sql_st->execute();

        if ($sql_st->rowCount() < 1)
            $this->createNew($id, $data);
    }

    private function dropSession($id) {
        $sql_st = $this->dbConnection->prepare(
                "DELETE FROM `UzytkownikSesja`
                    WHERE `Session_KEY` = :key");

        $sql_st->bindValue(":key", $id, PDO::PARAM_STR);
        $sql_st->execute();
    }

    private function dropOldSessions() {
        $sql_st = $this->dbConnection->prepare(
                "DELETE `UzytkownikSesja`
                    WHERE FROM_UNIXTIME(:requestTime) >= `ExpirationTime`");

        $sql_st->bindValue(":requestTime", $this->reqestTime, PDO::PARAM_INT);
        $sql_st->execute();
    }

    private function getNextExpiresTime() {
        return $this->reqestTime + Session::SESSION_EXPIRATION_TIME;
    }

}

?>
