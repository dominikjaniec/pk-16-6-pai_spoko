<?php

function show404() {
    header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found", true, 404);
    include '../public_html/_show404.php';
    exit();
}

function redirect($destinationUrl) {
    header("Location: $destinationUrl", true, 303);
    exit();
}

function resolveGetValue($valueName) {
    if (!isset($_GET[$valueName]))
        throw new Exception("Missing value: '" . $valueName . "' in GET.");

    return $_GET[$valueName];
}

function resolvePostField($fieldName) {
    if (!isset($_POST[$fieldName]))
        throw new Exception("Missing field: '" . $fieldName . "' in POST.");

    return $_POST[$fieldName];
}

function resolvePostCheckbox($fieldName) {
    return isset($_POST[$fieldName]);
}

function __tryHandlePDOCreationException($pdoEx) {
    $isKnownSeriousProblem = false;
    $knownDieCodes = array(
            2005 /* There is no MySQL server at given name. */,
            1045 /* Cannot log-in with given credentials. */);

    if (in_array($pdoEx->getCode(), $knownDieCodes))
        $isKnownSeriousProblem = true;

    if ($pdoEx->getCode() == 1049) {
        // There is no Schema on server, so maybe should be installed?
        $installFileName = "spoko_install.php";
        if (basename($_SERVER["SCRIPT_FILENAME"]) != $installFileName) {
            // OK, we are not in Installator, but is there any?
            $installFilePath = $_SERVER["DOCUMENT_ROOT"] . "/" . $installFileName;

            if (file_exists($installFilePath))
                redirect($installFileName);
            else
                $isKnownSeriousProblem = true;
        }
    }

    if ($isKnownSeriousProblem) {
        http_response_code(503);
        die("SPOKO, but there is very serious problem with our database: " . $pdoEx->getMessage());
    }

    return false;
}

function DumpIt($variable, $name = null) {
    echo "<pre>";

    if (!$name == null)
        echo "============= Dump of: '" . $name . "':\n";
    else
        echo "=======================================\n";

    var_dump($variable);
    echo "</pre>";
}

?>
