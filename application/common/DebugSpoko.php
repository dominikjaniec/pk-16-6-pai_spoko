<?php

class DebugSpoko {

    public static function shouldShow() {
        return ini_get("display_errors") === "1";
    }

    public static function getAllStatistics() {
        return array(
            "PHP version" => phpversion()
        );
    }

}

?>
