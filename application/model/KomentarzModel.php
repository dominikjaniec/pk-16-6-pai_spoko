<?php

require_once '../common/DataBase.php';

class KomentarzModel {

    public static function loadFor($wypowiedzID) {
        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT *
                    FROM `WypowiedzKomentarz`
                        WHERE `Wypowiedz_ID` = :wypowiedzID
                    ORDER BY `DataUtworzenia` ASC");

        $sql_st->bindValue(":wypowiedzID", $wypowiedzID);
        $sql_st->execute();

        return $sql_st->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function create($uzytkownikID, $wypowiedzID, $content) {
        $codedContent = self::validateAndEncodeContent($content);

        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT WypowiedzKomentarz_Create(:tresc, :autorID, :wypowiedzID)");

        $sql_st->bindParam(":tresc", $content, PDO::PARAM_STR);
        $sql_st->bindParam(":autorID", intval($uzytkownikID), PDO::PARAM_INT);
        $sql_st->bindParam(":wypowiedzID", intval($wypowiedzID), PDO::PARAM_INT);
        $sql_st->execute();

        $creation_result = $sql_st->fetch(PDO::FETCH_NUM);
        return intval($creation_result[0]);
    }

    public static function getPytanieID($komentarzID) {
        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT coalesce(w.`PytanieWypowiedz_ID`, w.`Wypowiedz_ID`)
                    FROM `WypowiedzPartitionData` AS w
                        NATURAL JOIN `WypowiedzKomentarz` AS k
                    WHERE k.`WypowiedzKomentarz_ID` = :komentarzID");

        $sql_st->bindValue(":komentarzID", intval($komentarzID));
        $sql_st->execute();

        $pytanieID = $sql_st->fetch(PDO::FETCH_NUM);
        return intval($pytanieID[0]);
    }

    private static function validateAndEncodeContent($content) {
        if (!isset($content) || empty($content))
            throw new Exception("Content is required");

        // TODO - ensure is no malicious...
        return htmlspecialchars($content);
    }

}

?>
