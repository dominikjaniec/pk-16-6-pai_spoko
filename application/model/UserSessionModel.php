<?php

require_once '../common/DataBase.php';
require_once '../common/Session.php';
require_once 'UzytkownikModel.php';

class UserSessionModel {
    const LOAD_USER_ID = "ID";
    const LOAD_LOGIN = "Login";
    const LOAD_FULLNAME = "FullName";
    const LOAD_EMAIL = "Email";
    const LOAD_ROLA = "Rola";

    public static function authenticate($login, $password) {
        self::authenticateCredentials($login, $password, true);
    }

    public static function authenticateKeepSession($login, $password) {
        self::authenticateCredentials($login, $password, false);
    }

    public static function loadCurrentUserData() {
        if (!Session::exists(Session::KEY_CURRENT_USER))
            return null;

        return Session::get(Session::KEY_CURRENT_USER);
    }

    public static function remove() {
        Session::restart();
    }

    private static function authenticateCredentials($login, $password, $dismissSession = true) {
        $login = UzytkownikModel::validateAndCorrectLogin($login);

        $userID = self::authenticateByDatabase($login, $password);
        $currentUserData = self::loadUserData($userID);
        self::saveUserLoginEvent($userID);

        if ($dismissSession) {
            Session::restart();
        } else {
            Session::changeId();
        }

        Session::set(Session::KEY_CURRENT_USER, $currentUserData);
    }

    private static function authenticateByDatabase($login, $password) {
        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT Uzytkownik_Authenticate(:login, :pass)");

        $sql_st->bindValue(":login", $login, PDO::PARAM_STR);
        $sql_st->bindValue(":pass", $password, PDO::PARAM_STR);
        $sql_st->execute();

        $result = $sql_st->fetch(PDO::FETCH_NUM);
        return intval($result[0]);
    }

    private static function loadUserData($userID) {
        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT u.`Login`, u.`PelnaNazwa`, u.`Email`, r.`Klucz`
                    FROM `Uzytkownik` AS u
                        NATURAL JOIN `Rola` AS r
                    WHERE u.`Uzytkownik_ID` = :userID");

        $sql_st->bindValue(":userID", $userID);
        $sql_st->execute();

        $user = $sql_st->fetch(PDO::FETCH_ASSOC);

        return array(
            self::LOAD_USER_ID => $userID,
            self::LOAD_LOGIN => $user["Login"],
            self::LOAD_FULLNAME => $user["PelnaNazwa"],
            self::LOAD_EMAIL => $user["Email"],
            self::LOAD_ROLA => $user["Klucz"]
        );
    }

    private static function saveUserLoginEvent($userID) {
        $currentDateTime = date("Y-m-d H:i:s");

        $userAgent = $_SERVER["HTTP_USER_AGENT"];
        $userHost = $_SERVER["HTTP_HOST"];

        $userAgent = substr($userAgent, 0, 400);
        $userHost = substr($userHost, 0, 200);

        $sql_st = DataBase::GetConnection()->prepare(
                "INSERT INTO `UzytkownikLogowania`
                    (`Uzytkownik_ID`, `DataLogowania`, `SourceHost`, `UserAgent`)
                    VALUES (:userID, :now, :userHost, :userAgent)");

        $sql_st->bindValue(":userID", $userID, PDO::PARAM_INT);
        $sql_st->bindValue(":now", $currentDateTime, PDO::PARAM_STR);
        $sql_st->bindValue(":userHost", $userHost, PDO::PARAM_STR);
        $sql_st->bindValue(":userAgent", $userAgent, PDO::PARAM_STR);
        $sql_st->execute();
    }

}

?>
