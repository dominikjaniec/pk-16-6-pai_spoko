<?php

require_once '../common/DataBase.php';

class WypowiedzModel {
    const RESULT_PYTANIE = "Pytanie";
    const RESULT_ODPOWIEDZI = "Odpowiedzi";
    const RESULT_KOMENTARZE = "Komentarze";

    const LENGHT_TITLE = 200;
    const LENGHT_VERSION_COMMENT = 50;

    public static function canBeRated($wypowiedzID, $uzytkownikID) {
        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT w.`AutorUzytkownik_ID`
                    FROM `Wypowiedz` AS w
                    WHERE w.`Wypowiedz_ID` = :wypowiedzID");

        $sql_st->bindValue(":wypowiedzID", intval($wypowiedzID));
        $sql_st->execute();

        $authorID = $sql_st->fetch(PDO::FETCH_NUM);
        return intval($authorID[0]) != intval($uzytkownikID);
    }

    public static function rateUp($wypowiedzID, $uzytkownikID) {
        $sql_st = DataBase::GetConnection()->prepare(
                "CALL Wypowiedz_OcenNaPlus(:wypowiedzID, :uzytkownikID)");

        $sql_st->bindParam(":wypowiedzID", intval($wypowiedzID), PDO::PARAM_INT);
        $sql_st->bindParam(":uzytkownikID", intval($uzytkownikID), PDO::PARAM_INT);
        $sql_st->execute();
    }

    public static function rateDown($wypowiedzID, $uzytkownikID) {
        $sql_st = DataBase::GetConnection()->prepare(
                "CALL Wypowiedz_OcenNaMinus(:wypowiedzID, :uzytkownikID)");

        $sql_st->bindParam(":wypowiedzID", intval($wypowiedzID), PDO::PARAM_INT);
        $sql_st->bindParam(":uzytkownikID", intval($uzytkownikID), PDO::PARAM_INT);
        $sql_st->execute();
    }

    protected static function validateAndEncodeContent($content) {
        if (!isset($content) || empty($content))
            throw new Exception("Content is required");

        // TODO - ensure is no malicious...
        return htmlspecialchars($content);
    }

    protected static function validateAndEncodeTitle($title) {
        if (!isset($title) || empty($title))
            throw new Exception("Title is required");

        if (strlen($title) > self::LENGHT_TITLE)
            throw new Exception("Title's length cannot be greather then " . self::LENGHT_TITLE);

        // TODO - ensure is no malicious...
        return htmlspecialchars($title);
    }

    protected static function validateAndEncodeVersionComment($version_comment) {
        if (!isset($version_comment) || empty($version_comment))
            throw new Exception("Version comment is required");

        if (strlen($version_comment) > self::LENGHT_VERSION_COMMENT)
            throw new Exception("Version comment's length cannot be greather then " . self::LENGHT_VERSION_COMMENT);

        // TODO - ensure is no malicious...
        return htmlspecialchars($version_comment);
    }

}

?>
