<?php

require_once '../common/DataBase.php';
require_once 'WypowiedzModel.php';
require_once 'OdpowiedzModel.php';
require_once 'KomentarzModel.php';
require_once 'SearchModel.php';

class PytanieModel extends WypowiedzModel {

    public static function load($pytanieID) {
        $pytanieID = intval($pytanieID);

        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT *
                    FROM `Pytanie_DataView` AS p
                    WHERE p.`Wypowiedz_ID` = :wypowiedzID");

        $sql_st->bindValue(":wypowiedzID", $pytanieID);
        $sql_st->execute();

        $pytanie = $sql_st->fetch(PDO::FETCH_ASSOC);
        if (empty($pytanie))
            self::throwThereIsNo($pytanieID);

        $pytanie[self::RESULT_KOMENTARZE] = KomentarzModel::loadFor($pytanieID);
        $odpowiedzi_pytania = OdpowiedzModel::loadFor($pytanieID);

        return array(
            self::RESULT_PYTANIE => $pytanie,
            self::RESULT_ODPOWIEDZI => $odpowiedzi_pytania);
    }

    public static function loadTitle($pytanieID) {
        $pytanieID = intval($pytanieID);

        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT p.`Tytul`
                    FROM `Pytanie_DataView` AS p
                    WHERE p.`Wypowiedz_ID` = :wypowiedzID");

        $sql_st->bindValue(":wypowiedzID", $pytanieID);
        $sql_st->execute();

        $result = $sql_st->fetch(PDO::FETCH_NUM);
        if (empty($result))
            self::throwThereIsNo($pytanieID);

        return $result[0];
    }

    private static function throwThereIsNo($pytanieID) {
        throw new Exception("There is no Pytanie with ID: '" . $pytanieID . "'");
    }

    public static function create($uzytkownikID, $title, $content) {
        $title = self::validateAndEncodeTitle($title);
        $content = self::validateAndEncodeContent($content);

        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT Wypowiedz_CreatePytanie(:tytul, :tresc, :autorID)");

        $sql_st->bindParam(":tytul", $title, PDO::PARAM_STR, 200);
        $sql_st->bindParam(":tresc", $content, PDO::PARAM_STR);
        $sql_st->bindParam(":autorID", intval($uzytkownikID), PDO::PARAM_INT);
        $sql_st->execute();

        $creation_result = $sql_st->fetch(PDO::FETCH_NUM);
        $pytanieID = intval($creation_result[0]);

        SearchModel::buildIndexFor($pytanieID, $title, $content);
        return $pytanieID;
    }

    public static function makeNewVersion($pytanieID, $uzytkownikID, $title, $content, $version_comment) {
        $version_comment = self::validateAndEncodeVersionComment($version_comment);
        $content = self::validateAndEncodeContent($content);

        $sql_st = DataBase::GetConnection()->prepare(
                "CALL WypowiedzTresc_CreateVersionForPytanie(:tytul, :tresc, :autorID, :komentarz, :wypowiedzID)");

        $sql_st->bindParam(":tytul", $title, PDO::PARAM_STR, 200);
        $sql_st->bindParam(":tresc", $content, PDO::PARAM_STR);
        $sql_st->bindParam(":autorID", intval($uzytkownikID), PDO::PARAM_INT);
        $sql_st->bindParam(":komentarz", $version_comment, PDO::PARAM_STR, 50);
        $sql_st->bindParam(":wypowiedzID", intval($pytanieID), PDO::PARAM_INT);
        $sql_st->execute();

        SearchModel::buildIndexFor($pytanieID, $title, $content);
    }

}

?>
