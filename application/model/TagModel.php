<?php

require_once '../common/DataBase.php';
require_once '../common/CurrentUser.php';

class TagModel {
    const USAGE_KIND_FAVOURITE = "ulubiony";
    const USAGE_KIND_IGNORED = "ignorowany";

    const TAG_FAVOURITE_AWARD = 5;
    const TAG_IGNORED_PENALTY = 10;

    const LIST_SEPARATOR = "|";
    const MAX_MATCHED_TAGS = 5;

    public static function findMatches($term, $tagsToSkip, $useUserPreferences) {
        $queryLimit = intval(self::MAX_MATCHED_TAGS * 2.5);

        $matchedTags = CurrentUser::isLogged() && $useUserPreferences
                ? self::getMatchedTagsWithUserPreferences($term, $queryLimit)
                : self::getMatchedTags($term, $queryLimit);

        $tagsCount = 0;
        $resultTags = array();
        foreach ($matchedTags as $tag) {
            if (!in_array($tag, $tagsToSkip)) {
                array_push($resultTags, $tag);
                $tagsCount += 1;
            }

            if ($tagsCount >= self::MAX_MATCHED_TAGS)
                break;
        }

        return $resultTags;
    }

    private static function getMatchedTags($term, $queryLimit) {
        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT
                    t.`Nazwa`,
                    Search_Levenshtein(t.`Nazwa`, :termTag)
                        AS `EditDistance`
                FROM `Tag` AS t
                ORDER BY
                    `EditDistance` ASC,
                    t.`Nazwa` ASC
                LIMIT " . $queryLimit);

        $sql_st->bindValue(":termTag", $term);
        $sql_st->execute();

        return $sql_st
                ->fetchAll(PDO::FETCH_COLUMN, 0);
    }

    private static function getMatchedTagsWithUserPreferences($term, $queryLimit) {
        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT
                    tm.`Nazwa`,
                    IF(ut.`TypUzycia` = N'" . self::USAGE_KIND_FAVOURITE . "',
                        tm.`EditDistance` - " . self::TAG_FAVOURITE_AWARD . ",
                        IF(ut.`TypUzycia` = N'" . self::USAGE_KIND_IGNORED . "',
                            tm.`EditDistance` + ". self::TAG_IGNORED_PENALTY. ",
                            tm.`EditDistance`)) AS `EditDistance`
                FROM
                    (SELECT
                        t.`Tag_ID`,
                        t.`Nazwa`,
                        Search_Levenshtein(t.`Nazwa`, :termTag)
                            AS `EditDistance`
                        FROM `Tag` AS t) AS tm
                    LEFT OUTER JOIN `Uzytkownik_Tag` AS ut
                        ON ut.`Uzytkownik_ID` = :userID
                            AND ut.`Tag_ID` = tm.`Tag_ID`
                ORDER BY
                    `EditDistance` ASC,
                    `Nazwa` ASC
                LIMIT " . $queryLimit);

        $sql_st->bindValue(":termTag", $term);
        $sql_st->bindValue(":userID", CurrentUser::id());
        $sql_st->execute();

        return $sql_st
                ->fetchAll(PDO::FETCH_COLUMN, 0);
    }

    public static function loadTags($pytanieID) {
        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT
                    wt.`Tag_ID`,
                    wt.`Nazwa`,
                    wt.`Wypowiedz_Ilosc`,
                    wt.`Wypowiedz_SumaWartosci`,
                    wt.`Uzytkownicy_Ulubiony`
                FROM `Wypowiedz_Tag_View` AS wt
                    WHERE wt.`Wypowiedz_ID` = :wypowiedzID
                    ORDER BY wt.`Nazwa`");

        $sql_st->bindValue(":wypowiedzID", $pytanieID);
        $sql_st->execute();

        return $sql_st->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function loadUsersTags($userID) {
        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT
                    t.`Nazwa`,
                    ut.`TypUzycia`
                FROM `Uzytkownik_Tag` AS ut
                    NATURAL JOIN `Tag` AS t
                WHERE
                    ut.`Uzytkownik_ID` = :userID
                ORDER BY
                    t.`Nazwa`");

        $sql_st->bindValue(":userID", $userID);
        $sql_st->execute();

        $allTags = $sql_st
                ->fetchAll(PDO::FETCH_ASSOC);

        $tags = array(
                self::USAGE_KIND_FAVOURITE => array(),
                self::USAGE_KIND_IGNORED => array());

        foreach ($allTags as $tag) {
            $usage = $tag["TypUzycia"];
            $name = $tag["Nazwa"];

            array_push($tags[$usage], $name);
        }

        return $tags;
    }

    public static function setForPytanie($pytanieID, $tags) {
        $currentTagsIds = self::getCurrentTagsIds($pytanieID);
        $tagsToSetIds = self::getTagsIdsByNames($tags);

        $toRemoveIds = array_diff($currentTagsIds, $tagsToSetIds);
        foreach ($toRemoveIds as $tagId) {
            $sql_st = DataBase::GetConnection()->prepare(
                    "CALL Wypowiedz_Tag_Unset(:tagID, :wypowiedzID)");

            $sql_st->bindValue(":tagID", $tagId);
            $sql_st->bindValue(":wypowiedzID", $pytanieID);
            $sql_st->execute();
        }

        $toSetIds = array_diff($tagsToSetIds, $currentTagsIds);
        foreach ($toSetIds as $tagId) {
            $sql_st = DataBase::GetConnection()->prepare(
                    "CALL Wypowiedz_Tag_Set(:tagID, :wypowiedzID)");

            $sql_st->bindValue(":tagID", $tagId);
            $sql_st->bindValue(":wypowiedzID", $pytanieID);
            $sql_st->execute();
        }
    }

    public static function setUserTagsUsage($userID, $tagsFavourite, $tagsIgnored) {
        $currentTagsUsageIds = self::getCurrentTagsUsageIds($userID);
        $favouriteTagsIds = self::getTagsIdsByNames($tagsFavourite);
        $ignoredTagsIds = self::getTagsIdsByNames($tagsIgnored);

        $toUnsetIds = array_diff($currentTagsUsageIds, $ignoredTagsIds);
        $toUnsetIds = array_diff($toUnsetIds, $favouriteTagsIds);
        foreach ($toUnsetIds as $tagID) {
            self::execUserTagsUsage("Uzytkownik_Tag_Unset", $userID, $tagID);
        }

        foreach ($ignoredTagsIds as $tagID) {
            self::execUserTagsUsage("Uzytkownik_Tag_SetAsIgnorowany", $userID, $tagID);
        }

        foreach ($favouriteTagsIds as $tagID) {
            self::execUserTagsUsage("Uzytkownik_Tag_SetAsUlubiony", $userID, $tagID);
        }
    }

    private function execUserTagsUsage($procedure_name, $userID, $tagID) {
        $sql_st = DataBase::GetConnection()->prepare(
                "CALL $procedure_name(:tagID, :userID)");

        $sql_st->bindValue(":tagID", $tagID);
        $sql_st->bindValue(":userID", $userID);
        $sql_st->execute();
    }

    private static function getCurrentTagsIds($pytanieID) {
        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT wt.`Tag_ID`
                    FROM `Wypowiedz_Tag` as wt
                    WHERE wt.`Wypowiedz_ID` = :wypowiedzID");

        $sql_st->bindValue(":wypowiedzID", $pytanieID);
        $sql_st->execute();

        return $sql_st->fetchAll(PDO::FETCH_COLUMN, 0);
    }

    private static function getCurrentTagsUsageIds($userID) {
        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT ut.`Tag_ID`
                    FROM `Uzytkownik_Tag` AS ut
                    WHERE ut.`Uzytkownik_ID` = :userID");

        $sql_st->bindValue(":userID", $userID);
        $sql_st->execute();

        return $sql_st->fetchAll(PDO::FETCH_COLUMN, 0);
    }

    private static function getTagsIdsByNames($tags) {
        $tagsIds = array();
        foreach ($tags as $tag) {
            if (empty($tag)) {
                continue;
            }

            $sql_st = DataBase::GetConnection()->prepare(
                    "SELECT t.`Tag_ID`
                        FROM `Tag` AS t
                        WHERE t.`Nazwa` = :tag");

            $sql_st->bindValue(":tag", $tag);
            $sql_st->execute();

            $tagIdData = $sql_st->fetch(PDO::FETCH_NUM);
            if (!empty($tagIdData)) {
                array_push($tagsIds, intval($tagIdData[0]));
            } else {
                throw new Exception("New tags creation is NOT implemented...");
            }
        }

        return $tagsIds;
    }

}

?>
