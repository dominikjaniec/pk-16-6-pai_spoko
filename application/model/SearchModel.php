<?php

require_once '../common/DataBase.php';

class SearchModel {
    const SEARCHWORD_MAX_LENGHT = 250;
    const SEARCHWORD_MIN_LENGHT = 3;

    const TITLE_OCCURRENCE_BONUS = 7;

    public static function buildIndexFor($wypowiedzID, $title, $content) {
        $titleStatistics = self::calculateStatistics($title);
        $contentStatistics = self::calculateStatistics($content);

        self::dropSearchIndexFor($wypowiedzID);
        self::setSearchIndexFor($wypowiedzID, $titleStatistics, true);
        self::setSearchIndexFor($wypowiedzID, $contentStatistics, false);
    }

    public static function getPytaniaList($searchTerms) {
        if (!isset($searchTerms) || empty($searchTerms))
            return self::loadDefaults();

        return self::loadBySearch($searchTerms);
    }

    private static function getSearchableWords($sourceText) {
        $words = preg_split("/\s+/u", $sourceText);
        $words = array_map(array("SearchModel", "cleanup"), $words);
        return array_filter($words, array("SearchModel", "isSearchable"));
    }

    private static function isSearchable($word) {
        // TODO : Skips "non-word" and "non-searchable" words.

        $length = mb_strlen($word, 'UTF-8');
        return $length >= self::SEARCHWORD_MIN_LENGHT
                && $length <= self::SEARCHWORD_MAX_LENGHT;
    }

    private static function cleanup($word) {
        $word = strtolower($word);
        $word = preg_replace("/\W+/u", "", $word);

        // TODO : Removes text format directives.
        return $word;
    }

    private static function calculateStatistics($sourceText) {
        $statistics = array();
        foreach (self::getSearchableWords($sourceText) as $word) {
            if (!isset($statistics[$word]))
                $statistics[$word] = 1;
            else
                $statistics[$word] += 1;
        }

        return $statistics;
    }

    private static function dropSearchIndexFor($wypowiedzID) {
        $sql_st = DataBase::GetConnection()->prepare(
                "CALL Search_DropIndex(:wypowiedzID)");

        $sql_st->bindParam(":wypowiedzID", $wypowiedzID, PDO::PARAM_INT);
        $sql_st->execute();
    }

    private static function setSearchIndexFor($wypowiedzID, $statistics, $isTitle) {
        $sql_st = DataBase::GetConnection()->prepare(
                "CALL Search_SetIndex(:wypowiedzID, :word, :isTitle, :count)");

        foreach ($statistics as $word => $count) {
            $sql_st->bindParam(":wypowiedzID", $wypowiedzID, PDO::PARAM_INT);
            $sql_st->bindParam(":word", $word, PDO::PARAM_STR, self::SEARCHWORD_MAX_LENGHT);
            $sql_st->bindParam(":isTitle", $isTitle, PDO::PARAM_BOOL);
            $sql_st->bindParam(":count", $count, PDO::PARAM_INT);
            $sql_st->execute();
        }
    }

    private static function loadDefaults() {
        // TODO : Adds using of CurrentUser tags preferences...

        $sql_st = DataBase::GetConnection()->query(
                "SELECT *
                    FROM `Pytania_ListView` AS p
                    ORDER BY
                        p.`DataUtworzenia` DESC,
                        p.`Oceny_SumaWartosc` DESC");

        return $sql_st->fetchAll(PDO::FETCH_ASSOC);
    }

    private static function loadBySearch($searchTerms) {
        $terms = self::getSearchableWords($searchTerms);
        if (empty($terms))
            return self::loadDefaults();

        $queryId = uniqid();
        self::prepareTableSearchQueryWord($queryId, $terms);
        return self::executeSearchQuery($queryId);
    }

    private static function prepareTableSearchQueryWord($queryId, $terms) {
        $n = 0;
        $wordsInsertQuery = array();
        $wordsInsertData = array("identity" => $queryId);

        foreach ($terms as $term) {
            array_push($wordsInsertQuery, "(:identity, :word_" . $n . ")");
            $wordsInsertData["word_" . $n] = $term;
            $n++;
        }

        $sqlQuery = "INSERT INTO `SearchQueryWord` (`Identity`, `Word`)
                        VALUES " . implode(', ', $wordsInsertQuery);

        $sql_st = DataBase::GetConnection()->prepare($sqlQuery);
        $sql_st->execute($wordsInsertData);
    }

    private static function executeSearchQuery($queryId) {
        $sql_st = DataBase::GetConnection()->prepare(
                "CALL Search_ByWords(:queryID, :titleBonus)");

        $sql_st->bindValue(":queryID", $queryId);
        $sql_st->bindValue(":titleBonus", self::TITLE_OCCURRENCE_BONUS);
        $sql_st->execute();

        return $sql_st->fetchAll(PDO::FETCH_ASSOC);
    }

}

?>
