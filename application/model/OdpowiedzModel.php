<?php

require_once '../common/DataBase.php';
require_once 'WypowiedzModel.php';
require_once 'KomentarzModel.php';

class OdpowiedzModel extends WypowiedzModel {

    public static function loadFor($pytanieID) {
        $pytanieID = intval($pytanieID);

        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT *
                    FROM `Odpowiedz_DataView`
                        WHERE `PytanieWypowiedz_ID` = :pytanieID
                    ORDER BY
                        `Oceny_SumaWartosc` DESC,
                        `DataUtworzenia` DESC");

        $sql_st->bindValue(":pytanieID", $pytanieID);
        $sql_st->execute();

        $odpowiedzi = array();
        foreach ($sql_st->fetchAll(PDO::FETCH_ASSOC) as $odpowiedz) {
            $odpowiedzID = intval($odpowiedz["Wypowiedz_ID"]);
            $odpowiedz[self::RESULT_KOMENTARZE] = KomentarzModel::loadFor($odpowiedzID);

            array_push($odpowiedzi, $odpowiedz);
        }

        return $odpowiedzi;
    }

    public static function create($uzytkownikID, $pytanieID, $content) {
        $content = self::validateAndEncodeContent($content);

        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT Wypowiedz_CreateOdpowiedz(:tresc, :autorID, :pytanieID)");

        $sql_st->bindParam(":tresc", $content, PDO::PARAM_STR);
        $sql_st->bindParam(":autorID", intval($uzytkownikID), PDO::PARAM_INT);
        $sql_st->bindParam(":pytanieID", intval($pytanieID), PDO::PARAM_INT);
        $sql_st->execute();

        $creation_result = $sql_st->fetch(PDO::FETCH_NUM);
        $odpowiedzID = intval($creation_result[0]);

        SearchModel::buildIndexFor($odpowiedzID, "", $content);
        return $odpowiedzID;
    }

    public static function makeNewVersion($odpowiedzID, $uzytkownikID, $content, $version_comment) {
        $version_comment = self::validateAndEncodeVersionComment($version_comment);
        $content = self::validateAndEncodeContent($content);

        $sql_st = DataBase::GetConnection()->prepare(
                "CALL WypowiedzTresc_CreateVersionForOdpowiedz(:tresc, :autorID, :komentarz, :wypowiedzID)");

        $sql_st->bindParam(":tresc", $content, PDO::PARAM_STR);
        $sql_st->bindParam(":autorID", intval($uzytkownikID), PDO::PARAM_INT);
        $sql_st->bindParam(":komentarz", $version_comment, PDO::PARAM_STR, 50);
        $sql_st->bindParam(":wypowiedzID", intval($odpowiedzID), PDO::PARAM_INT);
        $sql_st->execute();
    }

    public static function canBeAccepted($odpowiedzID, $uzytkownikID) {
        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT w_pyt.`AutorUzytkownik_ID`
                    FROM `WypowiedzPartitionData` AS w_odp
                    JOIN `Wypowiedz` AS w_pyt
                        ON  w_pyt.`Wypowiedz_ID` = w_odp.`PytanieWypowiedz_ID`
                    WHERE w_odp.`PytanieWypowiedz_ID` IS NOT NULL
                        AND w_odp.`Wypowiedz_ID` = :odpowiedzID");

        $sql_st->bindValue(":odpowiedzID", intval($odpowiedzID));
        $sql_st->execute();

        $authorID = $sql_st->fetch(PDO::FETCH_NUM);
        return intval($authorID[0]) == intval($uzytkownikID);
    }

    public static function accept($odpowiedzID) {
        $sql_st = DataBase::GetConnection()->prepare(
                "CALL Wypowiedz_SetAs_Uznana(:odpowiedzID)");

        $sql_st->bindParam(":odpowiedzID", intval($odpowiedzID), PDO::PARAM_INT);
        $sql_st->execute();
    }

    public static function getPytanieID($odpowiedzID) {
        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT o.`PytanieWypowiedz_ID`
                    FROM `Odpowiedz_DataView` AS o
                    WHERE o.`Wypowiedz_ID` = :odpowiedzID");

        $sql_st->bindValue(":odpowiedzID", intval($odpowiedzID));
        $sql_st->execute();

        $pytanieID = $sql_st->fetch(PDO::FETCH_NUM);
        return intval($pytanieID[0]);
    }

}

?>
