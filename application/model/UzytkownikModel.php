<?php

require_once '../common/DataBase.php';

class UzytkownikModel {
    const LENGHT_LOGIN = 50;
    const LENGHT_EMAIL = 50;
    const LENGHT_MIN_PASSWORD = 6;
    const LENGHT_MAX_PASSWORD = 255;

    public static function isLoginAvailable($login) {
        try {
            $sql_st = DataBase::GetConnection()->prepare(
                    "SELECT u.`Login`
                        FROM `Uzytkownik` AS u
                        WHERE u.`Login` = :login");

            $sql_st->bindValue(":login", self::validateAndCorrectLogin($login));
            $sql_st->execute();

            $result = $sql_st->fetch(PDO::FETCH_NUM);
            return empty($result);
        } catch (Exception $ex) {
            return false;
        }
    }

    public static function isValidUserID($userID){
        try {
            $sql_st = DataBase::GetConnection()->prepare(
                    "SELECT u.`Uzytkownik_ID`
                        FROM `Uzytkownik` AS u
                        WHERE u.`Uzytkownik_ID` = :userID");

            $sql_st->bindValue(":userID", intval($userID));
            $sql_st->execute();

            $result = $sql_st->fetch(PDO::FETCH_NUM);
            return !empty($result);
        } catch (Exception $ex) {
            return false;
        }
    }

    public static function create($login, $password, $email, $rolaKey) {
        $login = self::validateAndCorrectLogin($login);
        self::validatePassword($password);
        self::validateEmail($email);

        $sql_st = DataBase::GetConnection()->prepare(
                "SELECT Uzytkownik_Create(:login, :email, :pass, :rola)");

        $sql_st->bindValue(":login", $login, PDO::PARAM_STR);
        $sql_st->bindValue(":email", $email, PDO::PARAM_STR);
        $sql_st->bindValue(":pass", $password, PDO::PARAM_STR);
        $sql_st->bindValue(":rola", $rolaKey, PDO::PARAM_STR);
        $sql_st->execute();

        $result = $sql_st->fetch(PDO::FETCH_NUM);
        return intval($result[0]);
    }

    public static function validateAndCorrectLogin($login) {
        if (!isset($login) || empty($login))
            throw new Exception("Login jest wymagany.");

        if (strlen($login) > self::LENGHT_LOGIN)
            throw new Exception("Login nie może być dłuższy niż: " . self::LENGHT_LOGIN . " znaków.");

        return strtolower($login);
    }

    public static function validatePassword($password) {
        if (!isset($password) || empty($password))
            throw new Exception("Hasło jest wymagane.");

        if (strlen($password) > self::LENGHT_MAX_PASSWORD)
            throw new Exception("Hasło nie może być dłuższe niż: " . self::LENGHT_MAX_PASSWORD . " znaków.");

        if (strlen($password) < self::LENGHT_MIN_PASSWORD)
            throw new Exception("Hasło musi być dłuższe niż: " . self::LENGHT_MIN_PASSWORD . " znaków.");
    }

    public static function validateEmail($email) {
        if (!isset($email) || empty($email))
            throw new Exception("Email jest wymagany.");

        if (strlen($email) > self::LENGHT_EMAIL)
            throw new Exception("Email nie może być dłuższy niż: " . self::LENGHT_EMAIL . " znaków.");
    }

    public static function loadLinkViewData($userID) {
        $db_connection = DataBase::GetConnection();
        $data_query = $db_connection->prepare(
                "SELECT *
                    FROM `Uzytkownik_LinkView`
                    WHERE `Uzytkownik_ID` = :userID");

        $data_query->bindValue(":userID", $userID);
        $data_query->execute();

        return $data_query->fetch(PDO::FETCH_ASSOC);
    }

}

?>
